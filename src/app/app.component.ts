import { Component, Inject, OnDestroy, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';

import { Observable, Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as Waves from 'node-waves';

import { CoreMenuService } from '@core/components/core-menu/core-menu.service';
import { CoreSidebarService } from '@core/components/core-sidebar/core-sidebar.service';
import { CoreConfigService } from '@core/services/config.service';
import { CoreLoadingScreenService } from '@core/services/loading-screen.service';
import { CoreTranslationService } from '@core/services/translation.service';
import { environment } from 'environments/environment';

import { menu } from 'app/menu/menu';
import { locale as menuFinnish } from 'app/menu/i18n/fi';
import { locale as menuEnglish } from 'app/menu/i18n/en';
import { locale as menuFrench } from 'app/menu/i18n/fr';
import { locale as menuGerman } from 'app/menu/i18n/de';
import { locale as menuPortuguese } from 'app/menu/i18n/pt';
import { User } from './auth/models';
import { NavigationEnd, Router } from '@angular/router';
import { ActionPerformed, PushNotification, PushNotificationActionPerformed, PushNotifications, PushNotificationSchema, PermissionStatus } from '@capacitor/push-notifications';
import { Plugins } from '@capacitor/core';
import { App } from '@capacitor/app';
import { Device } from '@capacitor/device';
import { Capacitor } from '@capacitor/core';
import { Browser } from '@capacitor/browser';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { AppTrackingTransparency } from "capacitor-ios-app-tracking";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  coreConfig: any;
  menu: any;
  defaultLanguage: 'fi'; // This language will be used as a fallback when a translation isn't found in the current language
  appLanguage: 'fi'; // Set application default language i.e fr
  public cUser = null;
  curruser: any;
  public isLogged = false;
  devicetoken: any;
  devicedata: any;
  web = false;
  device;
  platform;
  app_version;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {DOCUMENT} document
   * @param {Title} _title
   * @param {Renderer2} _renderer
   * @param {ElementRef} _elementRef
   * @param {CoreConfigService} _coreConfigService
   * @param {CoreSidebarService} _coreSidebarService
   * @param {CoreLoadingScreenService} _coreLoadingScreenService
   * @param {CoreMenuService} _coreMenuService
   * @param {CoreTranslationService} _coreTranslationService
   * @param {TranslateService} _translateService
   */
  constructor(
    @Inject(DOCUMENT) private document: any,
    private _title: Title,
    private _http: HttpClient,
    private _renderer: Renderer2,
    private _elementRef: ElementRef,
    public _coreConfigService: CoreConfigService,
    private _coreSidebarService: CoreSidebarService,
    private _coreLoadingScreenService: CoreLoadingScreenService,
    private _coreMenuService: CoreMenuService,
    private _coreTranslationService: CoreTranslationService,
    private _translateService: TranslateService,
    private router: Router,
    private gtmService: GoogleTagManagerService
  ) {

    this.getUser();
    // Get the application main menu
    this.menu = menu;

    // Register the menu to the menu service
    this._coreMenuService.register('main', this.menu);

    // Set the main menu as our current menu
    this._coreMenuService.setCurrentMenu('main');

    // Add languages to the translation service
    this._translateService.addLangs(['fi', 'en', 'fr', 'de', 'pt']);

    // This language will be used as a fallback when a translation isn't found in the current language
    this._translateService.setDefaultLang('fi');

    // Set the translations for the menu
    this._coreTranslationService.translate(menuFinnish, menuEnglish, menuFrench, menuGerman, menuPortuguese);

    // Set application default language.
    // Change application language? Read the ngxTranslate Fix
    this._translateService.use('fi');
    // ? OR
    // ? User the current browser lang if available, if undefined use 'en'
    // const browserLang = this._translateService.getBrowserLang();
    // this._translateService.use(browserLang.match(/en|fr|de|pt/) ? browserLang : 'fi');

    /**
     * ! Fix : ngxTranslate
     * ----------------------------------------------------------------------------------------------------
     */

    /**
     *
     * Using different language than the default ('en') one i.e French?
     * In this case, you may find the issue where application is not properly translated when your app is initialized.
     *
     * It's due to ngxTranslate module and below is a fix for that.
     * Eventually we will move to the multi language implementation over to the Angular's core language service.
     *
     **/

    // Set the default language to 'en' and then back to 'fr'.

    setTimeout(() => {
      // this._translateService.setDefaultLang('en');
      this._translateService.setDefaultLang('fi');
    });

    /**
     * !Fix: ngxTranslate
     * ----------------------------------------------------------------------------------------------------
     */

    // Set the private defaults
    this._unsubscribeAll = new Subject();


    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        // Function you want to call here
        // console.log(this.curruser);
        if (this.curruser) {
          this.isLogged = true;
        }
        const gtmTag = {
          event: 'page',
          pageName: e.url
        };
        console.log('pushtagi kutsuttu');
        this.gtmService.pushTag(gtmTag);
      }
    });
    this.router.events.forEach(item => {
      if (item instanceof NavigationEnd) {
        const gtmTag = {
          event: 'page',
          pageName: item.url
        };
        this.gtmService.pushTag(gtmTag);
      }
    });

    // Device.getInfo().then(dinfo => {
    //   this.device = dinfo.name;
    //   this.platform = dinfo.platform;
    //   console.log('deviceinfo haettu..');
    // });
    // App.getInfo().then(ainfo => {
    //   this.app_version = ainfo.version;
    //   console.log('appversio haettu..');
    // });
    
  }

  // Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------
  getUser() {
    this.curruser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.curruser) {
      this.isLogged = true;
    }
    // console.log(this.curruser);
  }
  /**
   * On init
   */
  ngOnInit(): void {
    const isPushNotificationsAvailable = Capacitor.isPluginAvailable('PushNotifications');
    const isAppTrackingAvailable = Capacitor.isPluginAvailable('AppTrackingTransparency');

    // Ilmoitusten käyttöönotto
    if (isPushNotificationsAvailable) {
      this.initPushNotifications();
    }
    if (isAppTrackingAvailable) {
      this.initAppTracking();
    }

    // Init wave effect (Ripple effect)
    Waves.init();    

    // Subscribe to config changes
    this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
      this.coreConfig = config;

      // Layout
      //--------

      // Remove default classes first
      this._elementRef.nativeElement.classList.remove(
        'vertical-layout',
        'vertical-menu-modern',
        'horizontal-layout',
        'horizontal-menu'
      );
      // Add class based on config options
      if (this.coreConfig.layout.type === 'vertical') {
        this._elementRef.nativeElement.classList.add('vertical-layout', 'vertical-menu-modern');
      } else if (this.coreConfig.layout.type === 'horizontal') {
        this._elementRef.nativeElement.classList.add('horizontal-layout', 'horizontal-menu');
      }

      // Navbar
      //--------

      // Remove default classes first
      this._elementRef.nativeElement.classList.remove(
        'navbar-floating',
        'navbar-static',
        'navbar-sticky',
        'navbar-hidden'
      );

      // Add class based on config options
      if (this.coreConfig.layout.navbar.type === 'navbar-static-top') {
        this._elementRef.nativeElement.classList.add('navbar-static');
      } else if (this.coreConfig.layout.navbar.type === 'fixed-top') {
        this._elementRef.nativeElement.classList.add('navbar-sticky');
      } else if (this.coreConfig.layout.navbar.type === 'floating-nav') {
        this._elementRef.nativeElement.classList.add('navbar-floating');
      } else {
        this._elementRef.nativeElement.classList.add('navbar-hidden');
      }

      // Footer
      //--------

      // Remove default classes first
      this._elementRef.nativeElement.classList.remove('footer-fixed', 'footer-static', 'footer-hidden');

      // Add class based on config options
      if (this.coreConfig.layout.footer.type === 'footer-sticky') {
        this._elementRef.nativeElement.classList.add('footer-fixed');
      } else if (this.coreConfig.layout.footer.type === 'footer-static') {
        this._elementRef.nativeElement.classList.add('footer-static');
      } else {
        this._elementRef.nativeElement.classList.add('footer-hidden');
      }

      // Blank layout
      if (
        this.coreConfig.layout.menu.hidden &&
        this.coreConfig.layout.navbar.hidden &&
        this.coreConfig.layout.footer.hidden
      ) {
        this._elementRef.nativeElement.classList.add('blank-page');
        // ! Fix: Transition issue while coming from blank page
        this._renderer.setAttribute(
          this._elementRef.nativeElement.getElementsByClassName('app-content')[0],
          'style',
          'transition:none'
        );
      } else {
        this._elementRef.nativeElement.classList.remove('blank-page');
        // ! Fix: Transition issue while coming from blank page
        setTimeout(() => {
          this._renderer.setAttribute(
            this._elementRef.nativeElement.getElementsByClassName('app-content')[0],
            'style',
            'transition:300ms ease all'
          );
        }, 0);
        // If navbar hidden
        if (this.coreConfig.layout.navbar.hidden) {
          this._elementRef.nativeElement.classList.add('navbar-hidden');
        }
        // Menu (Vertical menu hidden)
        if (this.coreConfig.layout.menu.hidden) {
          this._renderer.setAttribute(this._elementRef.nativeElement, 'data-col', '1-column');
        } else {
          this._renderer.removeAttribute(this._elementRef.nativeElement, 'data-col');
        }
        // Footer
        if (this.coreConfig.layout.footer.hidden) {
          this._elementRef.nativeElement.classList.add('footer-hidden');
        }
      }

      // Skin Class (Adding to body as it requires highest priority)
      if (this.coreConfig.layout.skin !== '' && this.coreConfig.layout.skin !== undefined) {
        this.document.body.classList.remove('default-layout', 'bordered-layout', 'dark-layout', 'semi-dark-layout');
        this.document.body.classList.add(this.coreConfig.layout.skin + '-layout');
      }
    });

    // Set the application page title
    this._title.setTitle(this.coreConfig.app.appTitle);

    App.addListener('appUrlOpen', data => {
      // console.log('Incoming data from deeplink', data);

      // this.events.publish('deeplink', this.parseDeeplink(data.url));
      Browser.close().then(() => { }).catch(err => console.error(err));
    });

    // Avataan sidebarin valikot
    setTimeout(() => {
      const asdd = document.getElementsByClassName('nav-item has-sub');
      // console.log(asdd.length);
      if (asdd.length > 0) {
        for (let index = 0; index < asdd.length; index++) {
          const element = asdd[index] as HTMLElement;
          element.classList.add("open");
        }
      }
      // const asdasdd = asdd[1];
      // const asdasd = asdd[0] as HTMLElement;
      // const asdasddd = asdd[2] as HTMLElement;
      // asdasd.classList.add("open");
      // asdasdd.classList.add("open");
      // asdasddd.classList.add("open");

    }, 500);
  }

  initAppTracking() {
    AppTrackingTransparency.requestPermission().then((status) => {
      // console.log(status);
    });
    // AppTrackingTransparency.getTrackingStatus().then((status) => {
    //   console.log(status);
    // });
  }
  initPushNotifications() {
    // console.log('initPushNotifications kutsuttu @' + new Date());
    // const fcm = this.appData.getVariable('fcm');
    this.getUser();

    PushNotifications.addListener('registration', token => {
      // console.log('APN token', token);
      this.devicetoken = token;
      // fcm.getToken().then(data => {
      //   console.log('FCM token', data.token);
      // }).catch(e => console.error(e));
      if (this.isLogged) {
        this.getDeviceData(this.devicetoken.value);
      }
    });

    PushNotifications.addListener('registrationError', (error: any) => {
      // console.log('Error on registration', error);
    });

    PushNotifications.addListener('pushNotificationReceived', (notification: PushNotificationSchema) => {
      // console.log('Push received: ', notification);
    });

    PushNotifications.addListener('pushNotificationActionPerformed', (notification: ActionPerformed) => {
      // console.log('Push action performed', notification);
    });

    PushNotifications.requestPermissions().then(premission => {
      if (premission.receive == 'granted') {
        PushNotifications.register().then(() => {
          // console.log('Push notification register OK');
        }).catch(e => console.error(e));
      }
    });
  }

  getDeviceData(push_token: string) {
    return new Promise(async (resolve, reject) => {
      
      const devicedata = await Device.getInfo();
      this.device = devicedata.name;
      this.platform = devicedata.platform;
      // console.log('deviceinfo haettu..');
      
      const appdata = await App.getInfo();
      this.app_version = appdata.version;
      // console.log('appversio haettu..');

      // console.log('1 2 3 4', this.device, this.platform, this.app_version, push_token);
      if (this.device && this.platform && this.app_version && push_token) {
        // console.log('device_data löytyi, ajetaan rajapintakutsu');
        const device = this.device;
        const platform = this.platform;
        const app_version = this.app_version;
        const url = `${environment.apiUrl}/users/device/update`;
        this._http.post<any>(url, { push_token, device, platform, app_version }).subscribe((response: any) => {
          resolve(response);
          // console.log(response);
        }, e => reject(e));
      } else {
        resolve('asd');
      }
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // Public methods
  // -----------------------------------------------------------------------------------------------------

  // getCurrentUser() {
  //   return this._http.get<any>(`${environment.apiUrl}/auth/user`, { headers: { Authorization: this.curruser.token_type + ' ' + this.curruser.accessToken }, withCredentials: true })
  //     .pipe(first())
  //     .subscribe(response => {
  //       console.log(response)
  //       // return response;
  //     });
  // }

  /**
   * Toggle sidebar open
   *
   * @param key
   */
  toggleSidebar(key): void {
    this._coreSidebarService.getSidebarRegistry(key).toggleOpen();
  }
}
function useEffect(arg0: () => void, arg1: undefined[]) {
  throw new Error('Function not implemented.');
}


export const colors = {
  solid: {
    // primary: '#0D758C',
    primary: '#084259',
    // secondary: '#14A68B',
    secondary: '#F76B5E',
    success: '#14D6B3',
    info: '#12A6A6',
    warning: '#FF9F43',
    danger: '#CE601E',
    dark: '#4B4B4B',
    black: '#000',
    white: '#fff',
    body: '#f8f8f8'
  },
  light: {
    primary: '#0842591a',
    secondary: '#F76B5E1a',
    // primary: '#0D758C1a',
    // secondary: '#14A68B1a',
    success: '#14D6B31a',
    info: '#12A6A61a',
    warning: '#FF9F431a',
    danger: '#CE601E1a',
    dark: '#4B4B4B1a'
  }
};

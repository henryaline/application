﻿export enum Role {
  Admin = 'Admin',
  Client = 'Client',
  User = 'User',
  Yrittäjä = 'Yrittäjä',
  Työntekijä = 'Työntekijä',
  Moderator = 'Moderator'
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { environment } from 'environments/environment';
import { User, Role } from 'app/auth/models';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  //public
  public currentUser: Observable<User>;

  //private
  private currentUserSubject: BehaviorSubject<User>;

  /**
   *
   * @param {HttpClient} _http
   * @param {ToastrService} _toastrService
   */
  constructor(
    private _http: HttpClient,
    private _toastrService: ToastrService,
    private _router: Router
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  // getter: currentUserValue
  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  /**
   *  Confirms if user is admin
   */
  get isAdmin() {
    // console.log(Role.Admin + ' | ' + this.currentUserSubject.value.role);
    return this.currentUser && this.currentUserSubject.value.role === Role.Admin;
  }

  /**
   *  Confirms if user is client
   */
  get isClient() {
    return this.currentUser && this.currentUserSubject.value.role === Role.Client;
  }

  /**
   * User login
   *
   * @param email
   * @param mobile
   * @param password
   * @returns user
   */
  // login(email: string, password: string) {
  login(mobile: string, password: string) {
    return this._http
      // .post<any>(`${environment.apiUrl}/users/authenticate`, { email, password })
      .post<any>(`${environment.apiUrl}/auth/login`, { mobile, password })
      .pipe(
        map(user => {
          // login successful if there's a jwt token in the response
          if (user && user.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user));

            // Display welcome toast!
            setTimeout(() => {
              this._toastrService.success(
                'Olet kirjautunut sisään onnistuneesti ' +
                  user.role +
                  ' käyttäjänä Trafipointiin.🎉',
                '👋 Tervetuloa, ' + user.firstName + ' ' + user.lastName + '!',
                // '👋 Tervetuloa, ' + user.fullName + '!',
                { toastClass: 'toast ngx-toastr', closeButton: true }
              );
            }, 2500);

            // notify
            this.currentUserSubject.next(user);
          }

          return user;
        })
      );
  }

  /**
   * User register
   *
   * @param firstname
   * @param lastname
   * @param email
   * @param password_c
   * @param mobile
   * @param password
   * @returns user
   */
  // login(email: string, password: string) {
  register(firstname: string, lastname: string, mobile: string, email: string, password: string, password_c: string) {
    return this._http
      // .post<any>(`${environment.apiUrl}/users/authenticate`, { email, password })
      .post<any>(`${environment.apiUrl}/auth/register`, { firstname, lastname, mobile, email, password, password_c })
      .pipe(
        map(user => {
          // login successful if there's a jwt token in the response
          if (user && user.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user));

            // Display welcome toast!
            setTimeout(() => {
              this._toastrService.success(
                'Olet kirjautunut sisään onnistuneesti ' +
                user.role +
                ' käyttäjänä Trafipointiin.🎉',
                '👋 Tervetuloa, ' + user.firstName + ' ' + user.lastName + '!',
                // '👋 Tervetuloa, ' + user.fullName + '!',
                { toastClass: 'toast ngx-toastr', closeButton: true }
              );
            }, 2500);

            // notify
            this.currentUserSubject.next(user);
          }

          return user;
        })
      );
  }

  // Vanha
  // addCompany(contactperson: string, name: string, business_id: string, mobile: string, email: string) {
  //   return this._http
  //     // .post<any>(`${environment.apiUrl}/users/authenticate`, { email, password })
  //     .post<any>(`${environment.apiUrl}/companies/add`, { contactperson, name, business_id, mobile, email })
  //     .pipe(
  //       map(company => {
  //         // login successful if there's a jwt token in the response
  //         if (company) {
  //           // store user details and jwt token in local storage to keep user logged in between page refreshes
  //           const curruser: any = JSON.parse(localStorage.getItem('currentUser'));
  //           console.log(company[0]);
  //           curruser.company = company[0];
  //           localStorage.setItem('currentUser', JSON.stringify(curruser));

  //           // Display welcome toast!
  //           setTimeout(() => {
  //             this._toastrService.success(
  //               company[0].name +
  //               ' liitetty profiiliisi.🎉',
  //               '',
  //               // '👋 Tervetuloa, ' + user.fullName + '!',
  //               { toastClass: 'toast ngx-toastr', closeButton: true }
  //             );
  //           }, 2500);

  //           // notify
  //           // this.currentUserSubject.next(user);
  //         }

  //         return;
  //       })
  //     );
  // }

  /**
   * User logout
   *
   */
  logout() {
    return new Promise((resolve, reject) => {
      const url = `${environment.apiUrl}/users/device/unlink`;

      this._http.post<any>(url, {}).subscribe((response: any) => {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        // notify
        this.currentUserSubject.next(null);
        resolve(response);
        // console.log(response);
        this._router.navigate(['/pages/authentication/login-v2']);

      }, e => {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        // notify
        this.currentUserSubject.next(null);
        this._router.navigate(['/pages/authentication/login-v2']);
        reject(e)
      } );
    });
  }

  sendPasswordReset(mobile: string) {
    return this._http
      .post<any>(`${environment.apiUrl}/auth/reset`, { mobile })
      .pipe(
        // map(user => {
        //   // login successful if there's a jwt token in the response
        //   if (user && user.token) {
        //     // store user details and jwt token in local storage to keep user logged in between page refreshes
        //     localStorage.setItem('currentUser', JSON.stringify(user));

        //     // Display welcome toast!
        //     setTimeout(() => {
        //       this._toastrService.success(
        //         'Olet kirjautunut sisään onnistuneesti ' +
        //         user.role +
        //         ' käyttäjänä Trafipointiin.🎉',
        //         '👋 Tervetuloa, ' + user.firstName + ' ' + user.lastName + '!',
        //         // '👋 Tervetuloa, ' + user.fullName + '!',
        //         { toastClass: 'toast ngx-toastr', closeButton: true }
        //       );
        //     }, 2500);

        //     // notify
        //     this.currentUserSubject.next(user);
        //   }

        //   return user;
        // })
      );
  }

  sendPasswordResetCode(code: any, password:any, mobile:any) {
    return this._http
      .post<any>(`${environment.apiUrl}/auth/reset/code`, { code, password, mobile })
      .pipe(
        // map(user => {
        //   // login successful if there's a jwt token in the response
        //   if (user && user.token) {
        //     // store user details and jwt token in local storage to keep user logged in between page refreshes
        //     localStorage.setItem('currentUser', JSON.stringify(user));

        //     // Display welcome toast!
        //     setTimeout(() => {
        //       this._toastrService.success(
        //         'Olet kirjautunut sisään onnistuneesti ' +
        //         user.role +
        //         ' käyttäjänä Trafipointiin.🎉',
        //         '👋 Tervetuloa, ' + user.firstName + ' ' + user.lastName + '!',
        //         // '👋 Tervetuloa, ' + user.fullName + '!',
        //         { toastClass: 'toast ngx-toastr', closeButton: true }
        //       );
        //     }, 2500);

        //     // notify
        //     this.currentUserSubject.next(user);
        //   }

        //   return user;
        // })
      );
  }
}

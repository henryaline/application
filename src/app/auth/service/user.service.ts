import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'environments/environment';
import { User } from 'app/auth/models';

@Injectable({ providedIn: 'root' })
export class UserService {
  /**
   *
   * @param {HttpClient} _http
   */
  constructor(private _http: HttpClient) {}

  /**
   * Get all users
   */
  getAll() {
    // console.log(`${environment.apiUrl}/users`);
    // return this._http.get<any>(`${environment.apiUrl}/users`, { headers: { Authorization: 'Bearer 14|R3ggqey8mdrr8nZwyfXGkYvfgkQKFh7A7n5OGGXq' }, withCredentials: true });
    return this._http.get<any>(`${environment.apiUrl}/users`);
  }

  /**
   * Get user by id
   */
  getById(id: number) {
    return this._http.get<User>(`${environment.apiUrl}/users/${id}`);
  }
}

import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';

import { first, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { CoreConfigService } from '@core/services/config.service';

import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from 'app/auth/service/authentication.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-auth-forgot-password-v1',
  templateUrl: './auth-forgot-password-v1.component.html',
  styleUrls: ['./auth-forgot-password-v1.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AuthForgotPasswordV1Component implements OnInit {
  @ViewChild('c0', { static: true }) c0;
  @ViewChild('c1', { static: true }) c1;
  public resetPasswordForm: FormGroup;
  public enterCodeForm: FormGroup;
  public mobile = '';
  public alreadyHaveACode = false;
  // Public
  public mobileVar;
  public coreConfig: any;
  public loading = false;
  public submitted = false;
  public returnUrl: string;
  public error = '';

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   */
  constructor(
    private _coreConfigService: CoreConfigService,
    private formBuilder: FormBuilder,
    private _authenticationService: AuthenticationService,
    private _router: Router,
    private _toastrService: ToastrService
  ) {
    this._unsubscribeAll = new Subject();

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        menu: {
          hidden: true
        },
        customizer: false,
        enableLocalStorage: false
      }
    };

    this.resetPasswordForm = this.formBuilder.group({
      mobile: ['', Validators.required],
    });

    this.enterCodeForm = this.formBuilder.group({
      code: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(6)])],
      passwords: this.formBuilder.group({
        password: ['', Validators.required],
        passwordConfirm: ['', Validators.required],
      }, {
        validator(group) {
          return group.controls.password.value === group.controls.passwordConfirm.value ? null : { passwordDoesntMatch: true };
        }
      }),
    });
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    // Subscribe to config changes
    this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
      this.coreConfig = config;
    });
    setTimeout(() => {
      if (this.c0) {
        this.c0.setFocus();
      } else if (this.c1) {
        this.c1.setFocus();
      }
    }, 650);
  }

  iHaveAlreadyTheCode() {
    this.alreadyHaveACode = true;
    this.enterCodeForm.addControl('mobile', this.formBuilder.control('', Validators.required));
  }

  phoneNumberEntered() {
    return this.mobile.length > 0;
  }

  clearPhoneNumber() {
    this.resetPasswordForm.setValue({ mobile: '' });
    this.mobile = '';
  }

  async sendForm() {
    this.loading = true;
    this._authenticationService
      .sendPasswordReset(this.resetPasswordForm.value.mobile)
      .pipe(first())
      .subscribe(
        data => {
          this.mobile = this.resetPasswordForm.value.mobile;
          this.loading = false;
        },
        error => {
          this.error = error;
          this.loading = false;
          // console.log(this.error);
        }
      );
  }

  async sendCodeForm() {
    this.loading = true;

    if (this.alreadyHaveACode) {
      this.mobile = this.enterCodeForm.value.mobile;
    }

    const cde = this.enterCodeForm.value.code;
    const pwd = this.enterCodeForm.controls.passwords.value.password;
    const mobil = this.mobile;

    this._authenticationService.sendPasswordResetCode(cde, pwd, mobil)
      .pipe(first())
      .subscribe(
        data => {
          this._router.navigate(['']);
          setTimeout(() => {
              this._toastrService.success(
                'Salasana vaihdettu!🎉',
                '',
                { toastClass: 'toast ngx-toastr', closeButton: true }
              );
            }, 2500);
        },
        error => {
          this.error = error;
          this.loading = false;
          // console.log(this.error);
        }
      );
  }


  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { takeUntil, first } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { CoreConfigService } from '@core/services/config.service';
import { AuthenticationService } from 'app/auth/service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-auth-register-v1',
  templateUrl: './auth-register-v1.component.html',
  styleUrls: ['./auth-register-v1.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AuthRegisterV1Component implements OnInit {
  // Public
  // public userNameVar;
  public firstNameVar;
  public lastNameVar;
  public mobileVar;
  public emailVar;
  public passwordVar;
  public password_cVar;
  public coreConfig: any;
  public passwordTextType: boolean;
  public password_cTextType: boolean;
  public loading = false;
  public submitted = false;
  public authForm: FormGroup;
  public error = '';
  public returnUrl: string;



  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   */
  constructor(private _coreConfigService: CoreConfigService,
              private _authenticationService: AuthenticationService,
              private _formBuilder: FormBuilder,
              private _route: ActivatedRoute,
              private _router: Router ) {
    this._unsubscribeAll = new Subject();

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        menu: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        customizer: false,
        enableLocalStorage: false
      }
    };
  }

  get f() {
    return this.authForm.controls;
  }

  /**
   * Toggle password
   */
  togglePasswordTextType() {
    this.passwordTextType = !this.passwordTextType;
    
  }
  togglePassword_cTextType() {
    this.password_cTextType = !this.password_cTextType;
  }

  onSubmit() {
    this.submitted = true;
    // this.authForm.submitted = true;

    // stop here if form is invalid
    if (this.authForm.invalid) {
      return;
    }

    // Login
    this.loading = true;
    this._authenticationService
      // .login(this.f.email.value, this.f.password.value)
      .register(
        this.f.firstname.value,
        this.f.lastname.value,
        this.f.mobile.value,
        this.f.email.value,
        this.f.password.value,
        this.f.password_c.value
      )
      .pipe(first())
      .subscribe(
        data => {
          this._router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
          this.loading = false;
          // console.log(this.error);
        }
      );
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.authForm = this._formBuilder.group({
      // email: ['admin@demo.com', [Validators.required, Validators.email]],
      // password: ['admin', Validators.required]
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      mobile: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      password_c: ['', Validators.required]
    });

    this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';

    // Subscribe to config changes
    this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
      this.coreConfig = config;
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

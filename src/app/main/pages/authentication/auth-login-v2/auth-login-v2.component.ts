import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil, first } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { AuthenticationService } from 'app/auth/service';
import { CoreConfigService } from '@core/services/config.service';

import { AppComponent } from 'app/app.component';
import { Capacitor } from '@capacitor/core';
import { Device } from '@capacitor/device';


@Component({
  selector: 'app-auth-login-v2',
  templateUrl: './auth-login-v2.component.html',
  styleUrls: ['./auth-login-v2.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AuthLoginV2Component implements OnInit {
  //  Public
  public coreConfig: any;
  public loginForm: FormGroup;
  public loading = false;
  public submitted = false;
  public returnUrl: string;
  public error = '';
  public passwordTextType: boolean;
  public isPushNotificationsAvailable;
  public platform = '';

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   */
  constructor(
    private _coreConfigService: CoreConfigService,
    private _formBuilder: FormBuilder,
    private _route: ActivatedRoute,
    private _router: Router,
    private _authenticationService: AuthenticationService,
    private _appComponent: AppComponent,
  ) {
    this.isPushNotificationsAvailable = Capacitor.isPluginAvailable('PushNotifications');

    // redirect to home if already logged in
    if (this._authenticationService.currentUserValue) {
      // console.log(this._authenticationService.currentUserValue);
      if (this._authenticationService.currentUserValue.role == 'Yrittäjä') {
        this._router.navigate(['/dashboard/tenders/all']);
      } else if (this._authenticationService.currentUserValue.role == 'Työntekijä') {
        this._router.navigate(['/dashboard/tenders/company-listed']);
      } else {
        this._router.navigate(['/']);
      }
    }

    Device.getInfo().then(info => {
      this.platform = info.platform;
      // console.log(this.platform);
    });

    this._unsubscribeAll = new Subject();

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        menu: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        customizer: false,
        enableLocalStorage: false
      }
    };
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  /**
   * Toggle password
   */
  togglePasswordTextType() {
    this.passwordTextType = !this.passwordTextType;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    // Login
    this.loading = true;
    this._authenticationService
      .login(this.f.mobile.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          if (this.isPushNotificationsAvailable) {
            this._appComponent.initPushNotifications();
          }
          // console.log(data);
          if (data.role == 'Yrittäjä') {
            this._router.navigate(['/dashboard/tenders/all']);
          } else if (data.role == 'Työntekijä') {
            this._router.navigate(['/dashboard/tenders/company-listed']);
          } else {
            this._router.navigate(['/']);
          }
          // this._router.navigate([this.returnUrl]);
        },
        error => {
          // console.log(error);
          this.error = error;
          this.loading = false;
          // console.log(this.error);
        }
      );
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      // email: ['admin@demo.com', [Validators.required, Validators.email]],
      // password: ['admin', Validators.required]
      mobile: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';

    // Subscribe to config changes
    this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
      this.coreConfig = config;
    });
    
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil, first } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { AuthenticationService } from 'app/auth/service';
import { CoreConfigService } from '@core/services/config.service';

@Component({
  selector: 'app-auth-add-companyinfo-v1',
  templateUrl: './auth-add-companyinfo-v1.component.html',
  styleUrls: ['./auth-add-companyinfo-v1.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AuthCompanyInfoV1Component implements OnInit {
  // Public
  // public userNameVar;
  // public emailVar;
  // public passwordVar;
  // public coreConfig: any;
  // public passwordTextType: boolean;
  public coreConfig: any;
  public loginForm: FormGroup;
  public loading = false;
  public submitted = false;
  public returnUrl: string;
  public error = '';
  public passwordTextType: boolean;
  public password_cTextType: boolean;

  public usrName: string;
  public usrMobile: string;
  public usrEmail: string;



  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   */
  constructor(
    private _coreConfigService: CoreConfigService,
    private _formBuilder: FormBuilder,
    private _route: ActivatedRoute,
    private _router: Router,
    private _authenticationService: AuthenticationService) {
    // redirect to home if already logged in
    if (this._authenticationService.currentUserValue) {
      // this._router.navigate(['/']);
      // console.log(this._authenticationService.currentUserValue);
      this.usrName = this._authenticationService.currentUserValue.firstName + ' ' + this._authenticationService.currentUserValue.lastName;
      this.usrMobile = this._authenticationService.currentUserValue.mobile;
      this.usrEmail = this._authenticationService.currentUserValue.email;
    }
    
    this._unsubscribeAll = new Subject();

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        menu: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        customizer: false,
        enableLocalStorage: false
      }
    };
  }

  get f() {
    return this.loginForm.controls;
  }

  /**
   * Toggle password
   */
  togglePasswordTextType() {
    this.passwordTextType = !this.passwordTextType;
    this.password_cTextType = !this.password_cTextType;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    // Login
    this.loading = true;
    // this._authenticationService
    //   // .login(this.f.email.value, this.f.password.value)
    //   .addCompany(
    //     this.f.contactperson.value,
    //     this.f.name.value,
    //     this.f.business_id.value,
    //     this.f.mobile.value,
    //     this.f.email.value,
    //   )
    //   .pipe(first())
    //   .subscribe(
    //     data => {
    //       this._router.navigate([this.returnUrl]);
    //     },
    //     error => {
    //       this.error = error;
    //       this.loading = false;
    //       console.log(this.error);
    //     }
    //   );
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      contactperson: [this.usrName, Validators.required],
      name: ['', Validators.required],
      business_id: ['', Validators.required],
      email: [this.usrEmail, [Validators.required, Validators.email]],
      mobile: [this.usrMobile, Validators.required],
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';

    // Subscribe to config changes
    this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
      this.coreConfig = config;
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';

import { CoreCommonModule } from '@core/common.module';
import { CoreDirectivesModule } from '@core/directives/directives';
import { CorePipesModule } from '@core/pipes/pipes.module';
import { CoreSidebarModule } from '@core/components';

import { InvoiceListService } from 'app/main/apps/invoice/invoice-list/invoice-list.service';
import { InvoiceModule } from 'app/main/apps/invoice/invoice.module';

import { CampaingEditComponent } from 'app/main/apps/campaing/campaing-edit/campaing-edit.component';
import { CampaingEditService } from 'app/main/apps/campaing/campaing-edit/campaing-edit.service';

import { CampaingListComponent } from 'app/main/apps/campaing/campaing-list/campaing-list.component';
import { CampaingListService } from 'app/main/apps/campaing/campaing-list/campaing-list.service';

import { CampaingAddComponent } from 'app/main/apps/campaing/campaing-add/campaing-add.component';
import { CampaingAddService } from 'app/main/apps/campaing/campaing-add/campaing-add.service';

import { CampaingViewComponent } from 'app/main/apps/campaing/campaing-view/campaing-view.component';
import { CampaingViewService } from 'app/main/apps/campaing/campaing-view/campaing-view.service';

import { TranslateModule } from '@ngx-translate/core';


// const curruser: any = JSON.parse(localStorage.getItem('currentUser'));
// let redirRouteV: any = '/apps/user/user-view/';
// let redirRouteE: any = '/apps/user/user-edit/';
// if (curruser) {
//   redirRouteV = '/apps/user/user-view/' + curruser.id;
//   redirRouteE = '/apps/user/user-edit/' + curruser.id;
// }

// routing
const routes: Routes = [
  {
    path: 'campaing-add',
    component: CampaingAddComponent,
    resolve: {
      uls: CampaingAddService
    }
  },
  {
    path: 'campaing-list',
    component: CampaingListComponent,
    resolve: {
      uls: CampaingListService
    }
  },
  {
    path: 'campaing-view/:id',
    component: CampaingViewComponent,
    resolve: {
      data: CampaingViewService,
      InvoiceListService
    },
    data: { path: 'view/:id' }
  },
  {
    path: 'campaing-edit/:id',
    component: CampaingEditComponent,
    resolve: {
      ues: CampaingEditService
    }
  },
  // Tähän hae kirjautuneen käyttäjän id
  // {
  //   path: 'user-view',
  //   // redirectTo: '/apps/user/user-view/' + (curruser.id ?? null), // Redirection
  //   redirectTo: redirRouteV
  // },
  // {
  //   path: 'user-edit',
  //   // redirectTo: '/apps/user/user-edit/' + (curruser.id ?? null) // Redirection
  //   redirectTo: redirRouteE
  // }
];

@NgModule({
  declarations: [CampaingListComponent, CampaingViewComponent, CampaingEditComponent, CampaingAddComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreCommonModule,
    FormsModule,
    NgbModule,
    NgSelectModule,
    Ng2FlatpickrModule,
    NgxDatatableModule,
    CorePipesModule,
    CoreDirectivesModule,
    InvoiceModule,
    CoreSidebarModule,
    TranslateModule
  ],
  providers: [CampaingListService, CampaingViewService, CampaingEditService, CampaingAddService]
})
export class CampaingModule {
}

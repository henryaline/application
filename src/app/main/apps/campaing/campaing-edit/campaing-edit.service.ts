import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class CampaingEditService implements Resolve<any> {
  public apiData: any;
  public apiDataa: any;
  public onDataChanged: BehaviorSubject<any>;
  public onDataChangedd: BehaviorSubject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _toastrService: ToastrService, private _router: Router) {
    // Set the defaults
    this.onDataChanged = new BehaviorSubject({});
    this.onDataChangedd = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    let currentId = Number(route.paramMap.get('id'));
    return new Promise<void>((resolve, reject) => {
      // Promise.all([this.getApiData()]).then(() => {
      Promise.all([this.getApiData(currentId), this.getApiDataa()]).then(() => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get API Data
   */
  getApiData(id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/campaings`;
    return new Promise((resolve, reject) => {
      this._httpClient.get(url).subscribe((response: any) => {
        this.apiData = response.campaings.find(i => i.id == id);
        this.onDataChanged.next(this.apiData);
        resolve(this.apiData);
      }, reject);
    });
  }
  getApiDataa(): Promise<any[]> {
    const url = `${environment.apiUrl}/companies`;
    return new Promise((resolve, reject) => {
      this._httpClient.get(url).subscribe((response: any) => {
        this.apiDataa = response.companies;
        this.onDataChangedd.next(this.apiDataa);
        resolve(this.apiDataa);
      }, reject);
    });
  }

  // updateCampaing(id: number, firstname: string, lastname: string, email: string, mobile: string, role:number): Promise<any[]> {
  //   const url = `${environment.apiUrl}/campaings/edit`;
  //   return new Promise((resolve, reject) => {
  //     this._httpClient.post<any>(url, { id, firstname, lastname, email, mobile, role }).subscribe((response: any) => {
  //       this._router.navigate(['/apps/campaing/campaing-view/' + id]);
  //       setTimeout(() => {
  //         this._toastrService.success(
  //           'Kampanjan tiedot päivitetty.',
  //           '',
  //           { toastClass: 'toast ngx-toastr', closeButton: true }
  //         );
  //       }, 2500);
  //       // console.log(response);
  //       resolve(response.message);
  //     }, reject);
  //   });
  // }
  updateCampaing(id, name, description, is_general, is_solid, companies, subscription_price,
    event_muutto_price, event_rahti_price, event_ajoneuvot_price, event_massa_price, event_paketti_price,
    start_date, start_time, end_date, end_time): Promise<any[]> {
    const url = `${environment.apiUrl}/campaings/edit`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, {
        id, name, description, is_general, is_solid, companies, subscription_price,
        event_muutto_price, event_rahti_price, event_ajoneuvot_price, event_massa_price, event_paketti_price,
        start_date, start_time, end_date, end_time
      }).subscribe((response: any) => {
        this._router.navigate(['/apps/campaing/campaing-view/' + id]);
        setTimeout(() => {
          this._toastrService.success(
            'Kampanja päivitetty.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);

      }, reject);
    });
  }
}

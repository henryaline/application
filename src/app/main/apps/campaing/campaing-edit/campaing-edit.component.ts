import { Component, Injectable, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FlatpickrOptions } from 'ng2-flatpickr';

import { CampaingEditService } from 'app/main/apps/campaing/campaing-edit/campaing-edit.service';

import { CoreTranslationService } from '@core/services/translation.service';

import { locale as english } from 'app/main/apps/user/i18n/en';
import { locale as finnish } from 'app/main/apps/user/i18n/fi';
import { locale as french } from 'app/main/apps/user/i18n/fr';
import { locale as german } from 'app/main/apps/user/i18n/de';
import { locale as portuguese } from 'app/main/apps/user/i18n/pt';

import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbDate, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import moment from 'moment';

/**
* This Service handles how the date is represented in scripts i.e. ngModel.
*/
@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {

  readonly DELIMITER = '-';

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day: parseInt(date[0], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[2], 10)
      };
    }
    return null;
  }

  toModel(date: NgbDateStruct | null): string | null {
    return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null;
  }
}
/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day: parseInt(date[0], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : '';
  }
}
@Component({
  selector: 'app-campaing-edit',
  templateUrl: './campaing-edit.component.html',
  styleUrls: ['./campaing-edit.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: NgbDateAdapter, useClass: CustomAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter }
  ]
})
export class CampaingEditComponent implements OnInit {
  // Public
  public url = this.router.url;
  public urlLastValue;
  public row;
  public companies;
  public c_row;
  public loginForm: FormGroup;
  public pwForm: FormGroup;
  public companyForm: FormGroup;
  public passwordTextTypeOld = false;
  public passwordTextTypeNew = false;
  public passwordTextTypeRetype = false;
  public curruser: any;
  public campaingForm: FormGroup;
  public numRegex = /^-?\d*[.]?\d{0,2}$/;
  public selectMulti: Observable<any[]>;
  public selectMultiLimitedSelected = [];
  public basicDPdata: NgbDateStruct;
  public basicTP = { hour: 0, minute: 0 };
  public basicDPdata2: NgbDateStruct;
  public basicTP2 = { hour: 0, minute: 0 };
  public errors;

  public campainggeneral;
  public campaingsolid;
  public campaingcompanies;
  public generalstyle = "none";
  public companystyle = "none";

  public birthDateOptions: FlatpickrOptions = {
    altInput: true
  };

  public selectMultiLanguages = ['English', 'Spanish', 'French', 'Russian', 'German', 'Arabic', 'Sanskrit'];
  public selectMultiLanguagesSelected = [];

  // Private
  private _unsubscribeAll: Subject<any>;
  private _unsubscribeAlll: Subject<any>;

  /**
   * Constructor
   *
   * @param {Router} router
   * @param {CampaingEditService} _campaingAddService
   */
  constructor(private router: Router, private _campaingAddService: CampaingEditService,
    private _coreTranslationService: CoreTranslationService,
    private location: Location,
    private _formBuilder: FormBuilder) {
    this._unsubscribeAll = new Subject();
    this._unsubscribeAlll = new Subject();
    this.urlLastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
    this._coreTranslationService.translate(finnish, english, french, german, portuguese);

  }

  get f() {
    return this.loginForm.controls;
  }
  get ff() {
    return this.campaingForm.controls;
  }
  multiLimitedClearModel() {
    this.selectMultiLimitedSelected = [];
  }

  styleSelected(val) {
    if (val == 1) {
      const general = document.getElementById('campaing-style-general');
      const gn = general as HTMLElement;
      gn.style.display = "block";
      const notgeneral = document.getElementById('campaing-style-not-general');
      const ngn = notgeneral as HTMLElement;
      ngn.style.display = "none";
    }
    if (val == 2) {
      const notgeneral = document.getElementById('campaing-style-not-general');
      const ngn = notgeneral as HTMLElement;
      ngn.style.display = "block";
      const general = document.getElementById('campaing-style-general');
      const gn = general as HTMLElement;
      gn.style.display = "none";
    }
  }
  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this.curruser = JSON.parse(localStorage.getItem('currentUser'));
    this._campaingAddService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.row = response;
      // console.log(this.row);
    });
    this._campaingAddService.onDataChangedd.pipe(takeUntil(this._unsubscribeAlll)).subscribe(response => {
      this.companies = response;
      // console.log(this.row);
      this.selectMulti = this.companies;
    });
    if (this.row.is_general) {
      this.campainggeneral = "true";
      this.generalstyle = 'block';
    }
    if (!this.row.is_general) {
      this.campainggeneral = "false";
      this.companystyle = 'block';
      this.selectMultiLimitedSelected = this.row.companies;
    }
    // this.row.s_date = moment(this.row.start_time).format('DD/MM/YYYY');
    this.row.ss_date = moment(this.row.start_time).format('D-M-YYYY');
    const s_time_h = parseInt(moment(this.row.start_time).format('HH'));
    const s_time_m = parseInt(moment(this.row.start_time).format('mm'));
    this.basicTP = { hour: s_time_h, minute: s_time_m };
    // this.row.e_date = moment(this.row.end_time).format('D/M/YYYY');
    this.row.ee_date = moment(this.row.end_time).format('D-M-YYYY');
    const e_time_h = parseInt(moment(this.row.end_time).format('HH'));
    const e_time_m = parseInt(moment(this.row.end_time).format('mm'));
    this.basicTP2 = { hour: e_time_h, minute: e_time_m };
    // this.basicDPdata = this.row.s_date;
    // this.basicDPdata2 = this.row.e_date;


    this.campaingForm = this._formBuilder.group({
      name: [this.row.name, Validators.required],
      description: [this.row.description, Validators.required],
      is_general: [this.campainggeneral, Validators.required],
      is_solid: [this.row.is_solid],
      companies: [],
      monthly_subscription_price: [this.row.subscription_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_muutto_price: [this.row.event_muutto_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_rahti_price: [this.row.event_rahti_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_ajoneuvot_price: [this.row.event_ajoneuvot_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_massa_price: [this.row.event_massa_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_paketti_price: [this.row.event_paketti_price, [Validators.required, Validators.pattern(this.numRegex)]],
      start_date: [, Validators.required],
      start_time: [, Validators.required],
      end_date: [, Validators.required],
      end_time: [, Validators.required],
    });


    
  }

  ngAfterViewInit() {

    setTimeout(() => this.setDates(), 500);

  }

  setDates() {
    this.basicDPdata = this.row.ss_date;
    this.basicDPdata2 = this.row.ee_date;
  }

  goBack() {
    this.location.back();
  }

  submitForm(id: number) {
    // console.log(this.ff);
    this._campaingAddService.updateCampaing(
      id,
      this.ff.name.value,
      this.ff.description.value,
      this.ff.is_general.value,
      this.ff.is_solid.value,
      this.ff.companies.value,

      this.ff.monthly_subscription_price.value,
      this.ff.event_muutto_price.value,
      this.ff.event_rahti_price.value,
      this.ff.event_ajoneuvot_price.value,
      this.ff.event_massa_price.value,
      this.ff.event_paketti_price.value,

      this.ff.start_date.value,
      this.ff.start_time.value,
      this.ff.end_date.value,
      this.ff.end_time.value,
    ).catch((msg) => {
      this.errors = msg;
    });
  }
  // submit(id: number) {
  //   console.log(this.f);
  //   this._campaingAddService.updateCampaing(
  //     id,
  //     this.f.firstName.value,
  //     this.f.lastName.value,
  //     this.f.email.value,
  //     this.f.mobile.value,
  //     this.f.role.value,
  //   );
  // }
}

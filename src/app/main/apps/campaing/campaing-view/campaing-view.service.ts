import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable()
export class CampaingViewService implements Resolve<any> {
  public rows: any;
  public onDataChanged: BehaviorSubject<any>;
  public id;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _toastrService: ToastrService, private _router: Router) {
    // Set the defaults
    this.onDataChanged = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    let currentId = Number(route.paramMap.get('id'));
    return new Promise<void>((resolve, reject) => {
      Promise.all([this.getApiData(currentId)]).then(() => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get rows
   */
  getApiData(id: number): Promise<any[]> {
    // const url = `api/users-data/${id}`;
    const url = `${environment.apiUrl}/campaings`;


    return new Promise((resolve, reject) => {
      this._httpClient.get(url).subscribe((response: any) => {
        // this.rows = response;
        this.rows = response.campaings.find(i => i.id == id);
        this.onDataChanged.next(this.rows);
        resolve(this.rows);
      }, reject);
    });
  }

  deleteCampaing(id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/campaings/delete`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id }).subscribe((response: any) => {
        this._router.navigate(['/apps/campaing/campaing-list']);
        setTimeout(() => {
          this._toastrService.success(
            'Kampanja poistettu.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        resolve(response.message);
      }, reject);
    });
  }
}

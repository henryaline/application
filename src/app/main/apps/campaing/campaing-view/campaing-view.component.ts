import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { CampaingViewService } from 'app/main/apps/campaing/campaing-view/campaing-view.service';

import { CoreTranslationService } from '@core/services/translation.service';

import { locale as english } from 'app/main/apps/user/i18n/en';
import { locale as finnish } from 'app/main/apps/user/i18n/fi';
import { locale as french } from 'app/main/apps/user/i18n/fr';
import { locale as german } from 'app/main/apps/user/i18n/de';
import { locale as portuguese } from 'app/main/apps/user/i18n/pt';

@Component({
  selector: 'app-campaing-view',
  templateUrl: './campaing-view.component.html',
  styleUrls: ['./campaing-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CampaingViewComponent implements OnInit {
  // public
  public url = this.router.url;
  public lastValue;
  public data;
  public curruser: any;

  // private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {Router} router
   * @param {CampaingViewService} _campaingViewService
   */
  constructor(private router: Router, private _campaingViewService: CampaingViewService,
              private _coreTranslationService: CoreTranslationService) {
    this._unsubscribeAll = new Subject();
    this.lastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
    this._coreTranslationService.translate(finnish, english, french, german, portuguese);
  }

  clickMethod(name: string, id: number) {
    if (confirm("Haluatko varmasti poistaa kampanjan: " + name)) {
      this._campaingViewService.deleteCampaing(id);
    }
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this.curruser = JSON.parse(localStorage.getItem('currentUser'));
    this._campaingViewService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.data = response;
      // console.log(this.data);
    });
    if (this.data.subscription_price == null) {
      this.data.subprice = 'Ei määritelty';
    } else {
      this.data.subprice = this.data.subscription_price + '€';
    }

    if (this.data.event_muutto_price == null) {
      this.data.muuttoprice = 'Ei määritelty';
    } else {
      this.data.muuttoprice = this.data.event_muutto_price + '€';
    }

    if (this.data.event_rahti_price == null) {
      this.data.rahtiprice = 'Ei määritelty';
    } else {
      this.data.rahtiprice = this.data.event_rahti_price + '€';
    }

    if (this.data.event_ajoneuvot_price == null) {
      this.data.ajoneuvoprice = 'Ei määritelty';
    } else {
      this.data.ajoneuvoprice = this.data.event_ajoneuvot_price + '€';
    }
    
    if (this.data.event_massa_price == null) {
      this.data.massaprice = 'Ei määritelty';
    } else {
      this.data.massaprice = this.data.event_massa_price + '€';
    }

    if (this.data.event_paketti_price == null) {
      this.data.pakettiprice = 'Ei määritelty';
    } else {
      this.data.pakettiprice = this.data.event_paketti_price + '€';
    }
  }
}

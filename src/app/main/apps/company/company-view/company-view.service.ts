import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable()
export class CompanyViewService implements Resolve<any> {
  public rows: any;
  public rowss: any;
  public onDataChanged: BehaviorSubject<any>;
  public onDataChangedd: BehaviorSubject<any>;
  public id;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _toastrService: ToastrService, private _router: Router) {
    // Set the defaults
    this.onDataChanged = new BehaviorSubject({});
    this.onDataChangedd = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    let currentId = Number(route.paramMap.get('id'));
    return new Promise<void>((resolve, reject) => {
      Promise.all([]).then(() => {
      // Promise.all([this.getApiData(currentId), this.getApiDataa(currentId)]).then(() => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get rows
   */
  getApiData(id: number): Promise<any[]> {
    // const url = `api/users-data/${id}`;

    const url = `${environment.apiUrl}/companies/get/${id}`;
    // const url = `${environment.apiUrl}/companies`;

    return new Promise((resolve, reject) => {
      this._httpClient.get(url).subscribe((response: any) => {
        this.rows = response.companies.find(i => i.id == id);
        this.onDataChanged.next(this.rows);
        // console.log(this.rows);
        resolve(this.rows);
      }, reject);
    });
  }
  getApiDataa(id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/companies/get/${id}`;
    return new Promise((resolve, reject) => {
      this._httpClient.get(url).subscribe((response: any) => {
        this.rowss = response.campaings;
        this.onDataChangedd.next(this.rowss);
        // console.log(this.rows);
        resolve(this.rowss);
      }, reject);
    });
  }

  /**
   * Get rows
   */
  deactivateCompany(id: number): Promise<any[]> {
    // const url = `api/users-data/${id}`;

    // const url = `${environment.apiUrl}/companies/${id}`;
    const url = `${environment.apiUrl}/companies/deactivate`;

    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id }).subscribe((response: any) => {
      // this._httpClient.get(url).subscribe((response: any) => {
        // this.rows = response.companies.find(i => i.id == id);
        // this.onDataChanged.next(this.rows);
        // console.log(this.rows);
        // Display welcome toast!
        
        // this._router.navigate(['/apps/company/company-list']);
        
        setTimeout(() => {
          this._toastrService.success(
            'Yritys deaktivoitu',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        resolve(response.message);
      }, reject);
    });
  }
  deleteCompany(id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/companies/delete`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id }).subscribe((response: any) => {
        this._router.navigate(['/apps/company/company-list']);
        setTimeout(() => {
          this._toastrService.success(
            'Yritys poistettu onnistuneesti',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        resolve(response.message);
      }, reject);
    });
  }
  activateCompany(id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/companies/activate`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id }).subscribe((response: any) => {
        setTimeout(() => {
          this._toastrService.success(
            'Yritys aktivoitu',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        resolve(response.message);
      }, reject);
    });
  }

  addTemplate(company_id: number, for_type: string, name:string, content): Promise<any[]> {
    // const url = `api/users-data/${id}`;

    // const url = `${environment.apiUrl}/companies/${id}`;
    const url = `${environment.apiUrl}/companies/add-template`;

    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { company_id, for_type, name, content }).subscribe((response: any) => {
        // this._httpClient.get(url).subscribe((response: any) => {
        // this.rows = response.companies.find(i => i.id == id);
        // this.onDataChanged.next(this.rows);
        // console.log(this.rows);
        // Display welcome toast!

        // this._router.navigate(['/apps/company/company-list']);

        setTimeout(() => {
          this._toastrService.success(
            'Uusi tarjouspohja lisätty',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 1500);
        // this._companyComponent.updateData();
        resolve(response.message);
      }, reject);
    });
  }
  editTemplate(template_id: number, for_type: string, name: string, content): Promise<any[]> {
    // const url = `api/users-data/${id}`;

    // const url = `${environment.apiUrl}/companies/${id}`;
    const url = `${environment.apiUrl}/companies/edit-template`;

    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { template_id, for_type, name, content }).subscribe((response: any) => {
        // this._httpClient.get(url).subscribe((response: any) => {
        // this.rows = response.companies.find(i => i.id == id);
        // this.onDataChanged.next(this.rows);
        // console.log(this.rows);
        // Display welcome toast!

        // this._router.navigate(['/apps/company/company-list']);

        setTimeout(() => {
          this._toastrService.success(
            'Tarjouspohja muokattu',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 1500);
        // this._companyComponent.updateData();
        resolve(response.message);
      }, reject);
    });
  }
  deleteTemplate(template_id: number): Promise<any[]> {
    // console.log(template_id);
    const url = `${environment.apiUrl}/companies/delete-template`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { template_id }).subscribe((response: any) => {
        // this._httpClient.get(url).subscribe((response: any) => {
        // this.rows = response.companies.find(i => i.id == id);
        // this.onDataChanged.next(this.rows);
        // console.log(this.rows);
        // Display welcome toast!
        // this._router.navigate(['/apps/company/company-list']);

        setTimeout(() => {
          this._toastrService.success(
            'Tarjouspohja poistettu',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 1500);
        // this._companyComponent.updateData();
        resolve(response.message);
      }, reject);
    });
  }

  addTerm(company_id: number, name: string, content): Promise<any[]> {
    // const url = `api/users-data/${id}`;

    // const url = `${environment.apiUrl}/companies/${id}`;
    const url = `${environment.apiUrl}/companies/add-term`;

    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { company_id, name, content }).subscribe((response: any) => {
        // this._httpClient.get(url).subscribe((response: any) => {
        // this.rows = response.companies.find(i => i.id == id);
        // this.onDataChanged.next(this.rows);
        // console.log(this.rows);
        // Display welcome toast!

        // this._router.navigate(['/apps/company/company-list']);

        setTimeout(() => {
          this._toastrService.success(
            'Uusi Sopimusehto lisätty',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 1500);
        // this._companyComponent.updateData();
        resolve(response.message);
      }, reject);
    });
  }
  editTerm(template_id: number, name: string, content): Promise<any[]> {
    // const url = `api/users-data/${id}`;

    // const url = `${environment.apiUrl}/companies/${id}`;
    const url = `${environment.apiUrl}/companies/edit-term`;

    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { template_id, name, content }).subscribe((response: any) => {
        // this._httpClient.get(url).subscribe((response: any) => {
        // this.rows = response.companies.find(i => i.id == id);
        // this.onDataChanged.next(this.rows);
        // console.log(this.rows);
        // Display welcome toast!

        // this._router.navigate(['/apps/company/company-list']);

        setTimeout(() => {
          this._toastrService.success(
            'Sopimusehto muokattu',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 1500);
        // this._companyComponent.updateData();
        resolve(response.message);
      }, reject);
    });
  }
  deleteTerm(template_id: number): Promise<any[]> {
    // console.log(template_id);
    const url = `${environment.apiUrl}/companies/delete-term`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { template_id }).subscribe((response: any) => {
        // this._httpClient.get(url).subscribe((response: any) => {
        // this.rows = response.companies.find(i => i.id == id);
        // this.onDataChanged.next(this.rows);
        // console.log(this.rows);
        // Display welcome toast!
        // this._router.navigate(['/apps/company/company-list']);

        setTimeout(() => {
          this._toastrService.success(
            'Sopimusehto poistettu',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 1500);
        // this._companyComponent.updateData();
        resolve(response.message);
      }, reject);
    });
  }

  retryPayment(payment_id: number, company_id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/stripe/retry-payment`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { payment_id, company_id }).subscribe((response: any) => {
        if (response.fail == 'false') {
          setTimeout(() => {
            this._toastrService.success(
              'Maksu suoritettu',
              '',
              { toastClass: 'toast ngx-toastr', closeButton: true }
            );
          }, 1500);
        } else {
          setTimeout(() => {
            this._toastrService.error(
              response.message,
              '',
              { toastClass: 'toast ngx-toastr', closeButton: true }
            );
          }, 1500);
        }
        resolve(response.message);
      }, reject);
    });
  }

}

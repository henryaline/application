import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { CompanyViewService } from 'app/main/apps/company/company-view/company-view.service';

import { CoreTranslationService } from '@core/services/translation.service';
import { CoreSidebarService } from '@core/components/core-sidebar/core-sidebar.service';

import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';

import { locale as english } from 'app/main/apps/company/i18n/en';
import { locale as finnish } from 'app/main/apps/company/i18n/fi';
import { locale as french } from 'app/main/apps/company/i18n/fr';
import { locale as german } from 'app/main/apps/company/i18n/de';
import { locale as portuguese } from 'app/main/apps/company/i18n/pt';
import { EventsService } from 'services/events.service';
import moment from 'moment';

@Component({
  selector: 'app-company-view',
  templateUrl: './company-view.component.html',
  styleUrls: ['./company-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CompanyViewComponent implements OnInit {
  // public
  public url = this.router.url;
  public lastValue;
  public data;
  public campaings;
  public curruser: any;

  public offertemplateid = 0;
  public contracttemplateid = 0;
  public loading = false;
  public loadingdata = true;

  public show = false;


  // private
  private _unsubscribeAll: Subject<any>;
  private _unsubscribeAlll: Subject<any>;

  /**
   * Constructor
   *
   * @param {Router} router
   * @param {CompanyViewService} _companyViewService
   */
  constructor(private router: Router, private _companyViewService: CompanyViewService, private _httpClient: HttpClient,
    private _coreTranslationService: CoreTranslationService, private _coreSidebarService: CoreSidebarService, private event: EventsService
  ) {
    this._unsubscribeAll = new Subject();
    this._unsubscribeAlll = new Subject();
    this.lastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
    this._coreTranslationService.translate(finnish, english, french, german, portuguese);
    // console.log(this.curruser);
    // console.log(this.url);
    // console.log(this.lastValue);
  }

  deactivate(name: string, id: number) {
    if (confirm("Haluatko varmasti deaktivoida yrityksen: " + name)) {
      this._companyViewService.deactivateCompany(id).then(() => this._companyViewService.getApiData(this.data.id));
    }
  }
  activate(name: string, id: number) {
    if (confirm("Haluatko varmasti aktivoida yrityksen: " + name)) {
      this._companyViewService.activateCompany(id).then(() => this._companyViewService.getApiData(this.data.id));
    }
  }
  delete(name: string, id: number) {
    if (confirm("Haluatko varmasti poistaa yrityksen " + name + " pysyvästi?")) {
      // this._companyViewService.deleteTemplate(id).then(() => this._companyViewService.getApiData(this.data.id));
      this._companyViewService.deleteCompany(id);
    }
  }
  deleteTemplate(id: number) {
    if (confirm("Haluatko varmasti poistaa tarjouspohjan?")) {
      this._companyViewService.deleteTemplate(id).then(() => this._companyViewService.getApiData(this.data.id));
    }
  }
  deleteTerm(id: number) {
    if (confirm("Haluatko varmasti poistaa sopimusehdon?")) {
      this._companyViewService.deleteTerm(id).then(() => this._companyViewService.getApiData(this.data.id));
    }
  }
  retryPayment(payment_id: number, total:string) {
    if (confirm("Haluatko varmasti suorittaa maksun? Summa " + total + "€ veloitetaan yritykseesi liitetyltä maksukortilta.")) {
      // console.log('uudelleenmaksuyritys idllä ' + payment_id);
      this._companyViewService.retryPayment(payment_id, this.data.id).then(() => {
        // this._companyViewService.getApiData(this.data.id)
        this.getCompanyData();
      });
    }
  }


  /**
   * Toggle the sidebar
   *
   * @param name
   */
  toggleSidebar(name, iid): void {
    // console.log(this.loading);
    // console.log(name);
    this.loading = true;
    this.offertemplateid = iid;
    // console.log(this.offertemplateid);
    setTimeout(() => {
      this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
      this.event.publish('sidebar:opened');
      this.loading = false;
    }, 500);
  }
  toggleSidebarId(nm, iid) {
    // console.log(nm);
    this.offertemplateid = iid;
    this.toggleSidebar(nm, this.offertemplateid);
  }
  // Contracteille eri ku ei vittu toimi
  ctoggleSidebar(name, iid): void {
    // console.log(this.loading);
    // console.log(name);
    this.loading = true;
    this.contracttemplateid = iid;
    // console.log(this.offertemplateid);
    setTimeout(() => {
      this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
      this.event.publish('csidebar:opened');
      this.loading = false;
    }, 500);
  }
  ctoggleSidebarId(nm, iid) {
    // console.log(nm);
    this.contracttemplateid = iid;
    this.ctoggleSidebar(nm, this.contracttemplateid);
  }

  updateData() {
    this._companyViewService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.data = response;
      // console.log(this.data);
      if (this.data.events.length > 0) {
        for (const ev of this.data.events) {
          ev.causer = this.data.users.find(i => i.id == ev.causer_id)
          ev.time = moment(ev.created_at).format('DD.MM.YYYY HH:mm:ss');
        }
        this.data.events.sort(function (a, b) { return b.id - a.id });
      }
      if (this.data.balances.length > 0) {
        this.data.balances.sort(function (aa, bb) { return bb.id - aa.id });
      }
      if (this.data.invoices.length > 0) {
        this.data.invoices.sort(function (aaa, bbb) { return bbb.id - aaa.id });
      }
      if (this.data.payments.length > 0) {
        this.data.payments.sort(function (aaaa, bbbb) { return bbbb.id - aaaa.id });
      }
      if (this.data.ratings.length > 0) {
        let totalrates = 0;
        let ratecount = 0;
        for (const rating of this.data.ratings) {
          totalrates += rating.rating
          ratecount++;
        }
        this.data.ratingaverage = (totalrates / ratecount).toFixed(1);


        this.data.ratings.sort(function (aaaa, bbbb) { return bbbb.id - aaaa.id });
      }
    });
    this._companyViewService.onDataChangedd.pipe(takeUntil(this._unsubscribeAlll)).subscribe(response => {
      this.campaings = response;
      // console.log(this.campaings);
    });
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    // this.updateData();
    this.curruser = JSON.parse(localStorage.getItem('currentUser'));
    this.loadingdata = true;
    this.getCompanyData();

    
    if (this.curruser && (this.curruser.role_id == 1 || this.curruser.role_id == 2)) {
      this.show = true;
    }

    // console.log(this.show);
    // this._companyViewService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
    //   this.data = response;
    //   console.log(this.data);
    // });
  }

  getCompanyData() {
    const url = `${environment.apiUrl}/companies/get/${this.lastValue}`;
    this._httpClient.get(url).subscribe((response: any) => {
      this.data = response.companies.find(i => i.id == this.lastValue);
      this.campaings = response.campaings;

      // console.log(this.data);
      if (this.data.events.length > 0) {
        for (const ev of this.data.events) {
          ev.causer = this.data.users.find(i => i.id == ev.causer_id)
          ev.time = moment(ev.created_at).format('DD.MM.YYYY HH:mm:ss');
        }
        this.data.events.sort(function (a, b) { return b.id - a.id });
      }
      if (this.data.balances.length > 0) {
        this.data.balances.sort(function (aa, bb) { return bb.id - aa.id });
      }
      if (this.data.invoices.length > 0) {
        this.data.invoices.sort(function (aaa, bbb) { return bbb.id - aaa.id });
      }
      if (this.data.payments.length > 0) {
        this.data.payments.sort(function (aaaa, bbbb) { return bbbb.id - aaaa.id });
      }
      if (this.data.ratings.length > 0) {
        let totalrates = 0;
        let ratecount = 0;
        for (const rating of this.data.ratings) {
          totalrates += rating.rating
          ratecount++;
        }
        this.data.ratingaverage = (totalrates / ratecount).toFixed(1);

        this.data.ratings.sort(function (aaaa, bbbb) { return bbbb.id - aaaa.id });
      }
      if (this.data.users.length > 0) {
        this.data.users.forEach(element => {
          if (this.curruser && this.curruser.id == element.id) {
            this.show = true;
          }
        });
      }
      this.loadingdata = false;
    });
  }
}

import { Component, Input, OnInit } from '@angular/core';

import { CoreSidebarService } from '@core/components/core-sidebar/core-sidebar.service';
import { CompanyViewService } from '../../company-view.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompanyViewComponent } from '../../company-view.component';
import { EventsService } from 'services/events.service';
import { CompanyListService } from '../../../company-list/company-list.service';


@Component({
  selector: 'app-add-payment-sidebar-preview',
  templateUrl: './add-payment-sidebar-preview.component.html'
})
export class AddPaymentSidebarPreviewComponent implements OnInit {
  // ng2-flatpickr options
  public paymentDateOptions = {
    altInput: true,
    mode: 'single',
    altInputClass: 'form-control flat-picker flatpickr-input invoice-edit-input',
    defaultDate: ['2020-05-01'],
    altFormat: 'Y-n-j'
  };
  @Input() companyid: any;
  @Input() offerid: any;
  @Input() offers: any;
  // @Input() tendertype: any;
  public loginForm: FormGroup;


  constructor(private _coreSidebarService: CoreSidebarService,
    private _companyViewService: CompanyViewService,
    private _companyViewComponent: CompanyViewComponent,
    private _formBuilder: FormBuilder,
    private event: EventsService,
    private _companyListService: CompanyListService
  ) {
    this.event.subscribe('sidebar:opened', () => {
      // console.log('sidebar:opened');
      // console.log('tarjouspohja juttu avattu');
      this.setFForm();
    });
  }

  /**
   * Toggle the sidebar
   *
   * @param name
   */
  toggleSidebar(name): void {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }
  /**
   * Submit new offertemplate and toggle the sidebar
   *
   * @param name
   */
  submitAndToggleSidebar(name): void {
    // this.submitNew();
    if (this.offerid == 0) {
      this.submitNew();
    } else {
      this.submitEdit();
    }
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }
  
  get f() {
    return this.loginForm.controls;
  }

  submitNew() {
    this._companyViewService.addTemplate(
      this.f.companyid.value,
      this.f.for_type.value,
      this.f.name.value,
      this.f.content.value,
    ).then(() => this._companyViewComponent.getCompanyData());
    // ).then(() => this._companyViewService.getApiData(this.companyid));
    // setTimeout(() => {
    //   this._companyViewComponent.reloadComponent();
    // }, 500);
  }

  submitEdit() {
    this._companyViewService.editTemplate(
      this.offerid,
      this.f.for_type.value,
      this.f.name.value,
      this.f.content.value,
    ).then(() => this._companyViewComponent.getCompanyData());
    // ).then(() => this._companyViewService.getApiData(this.companyid));
    // setTimeout(() => {
    //   this._companyViewComponent.reloadComponent();
    // }, 500);
    // this._companyListService.getDataTableRows();
    
  }

  setFForm() {
    // console.log(this.offerid);
    if (this.offerid == 0) {
      this.loginForm = this._formBuilder.group({
        companyid: [this.companyid, Validators.required],
        for_type: ['', Validators.required],
        name: ['', Validators.required],
        content: ['', Validators.required],
      });
    } else {
      this.loginForm = this._formBuilder.group({
        companyid: [this.companyid, Validators.required],
        for_type: [this.offers.find(i => i.id == this.offerid).for_type, Validators.required],
        name: [this.offers.find(i => i.id == this.offerid).name, Validators.required],
        content: [this.offers.find(i => i.id == this.offerid).content, Validators.required],
      });
    }
  }

  ngOnInit(): void {
    // console.log(this.offers);
    this.setFForm();
  }
}

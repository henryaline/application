import { Component, OnInit, Input } from '@angular/core';

import { CoreSidebarService } from '@core/components/core-sidebar/core-sidebar.service';
import { CompanyViewService } from '../../company-view.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompanyViewComponent } from '../../company-view.component';
import { EventsService } from 'services/events.service';
import { CompanyListService } from '../../../company-list/company-list.service';

@Component({
  selector: 'app-send-tender-sidebar-preview',
  templateUrl: './send-tender-sidebar-preview.component.html'
})
export class SendTenderSidebarPreviewComponent implements OnInit {

  @Input() companyid: any;
  @Input() offerid: any;
  @Input() offers: any;
  // @Input() tendertype: any;
  public loginForm: FormGroup;

  constructor(private _coreSidebarService: CoreSidebarService,
    private _companyViewService: CompanyViewService,
    private _companyViewComponent: CompanyViewComponent,
    private _formBuilder: FormBuilder,
    private event: EventsService,
    private _companyListService: CompanyListService) {
      this.event.subscribe('csidebar:opened', () => {
        // console.log('sidebar:opened');
        // console.log('sopimusehtopohja juttu avattu');
        this.setForm();
      });
    }

  /**
   * Toggle the sidebar
   *
   * @param name
   */
  toggleSidebar(name): void {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }

  submitAndToggleSidebar(name): void {
    // this.submitNew();
    if (this.offerid == 0) {
      this.submitNew();
    } else {
      this.submitEdit();
    }
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }

  get f() {
    return this.loginForm.controls;
  }

  submitNew() {
    this._companyViewService.addTerm(
      this.f.companyid.value,
      this.f.name.value,
      this.f.content.value,
    ).then(() => this._companyViewComponent.getCompanyData());
    // ).then(() => this._companyViewService.getApiData(this.companyid));
    // setTimeout(() => {
    //   this._companyViewComponent.reloadComponent();
    // }, 500);
  }

  submitEdit() {
    this._companyViewService.editTerm(
      this.offerid,
      this.f.name.value,
      this.f.content.value,
    ).then(() => this._companyViewComponent.getCompanyData());
    // ).then(() => this._companyViewService.getApiData(this.companyid));
    // setTimeout(() => {
    //   this._companyViewComponent.reloadComponent();
    // }, 500);
    // this._companyListService.getDataTableRows();

  }

  setForm() {
    // console.log(this.offerid);
    if (this.offerid == 0) {
      this.loginForm = this._formBuilder.group({
        companyid: [this.companyid, Validators.required],
        name: ['', Validators.required],
        content: ['', Validators.required],
      });
    } else {
      this.loginForm = this._formBuilder.group({
        companyid: [this.companyid, Validators.required],
        name: [this.offers.find(i => i.id == this.offerid).name, Validators.required],
        content: [this.offers.find(i => i.id == this.offerid).content, Validators.required],
      });
    }
  }

  ngOnInit(): void {
    // console.log(this.offers);
    this.setForm();
  }
}

import { Component, OnInit, ViewEncapsulation, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FlatpickrOptions } from 'ng2-flatpickr';

import { CompanyEditService } from 'app/main/apps/company/company-edit/company-edit.service';

import { CoreTranslationService } from '@core/services/translation.service';

import { locale as english } from 'app/main/apps/company/i18n/en';
import { locale as finnish } from 'app/main/apps/company/i18n/fi';
import { locale as french } from 'app/main/apps/company/i18n/fr';
import { locale as german } from 'app/main/apps/company/i18n/de';
import { locale as portuguese } from 'app/main/apps/company/i18n/pt';

import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { StripeService, StripeCardComponent } from 'ngx-stripe';
import {
  StripeCardElementOptions,
  StripeElementsOptions
} from '@stripe/stripe-js';
import { environment } from 'environments/environment';
import { Device } from '@capacitor/device';
import { EventsService } from 'services/events.service';
import { Browser } from '@capacitor/browser';
import { App } from '@capacitor/app';


@Component({
  selector: 'app-company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CompanyEditComponent implements OnInit {
  @ViewChild(StripeCardComponent) card: StripeCardComponent;
  cardOptions: StripeCardElementOptions = {
    style: {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        fontWeight: '300',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#CFD7E0'
        }
      }
    }
  };
  elementsOptions: StripeElementsOptions = {
    locale: 'fi'
  };
  stripeTest: FormGroup;
  // Public
  public url = this.router.url;
  public urlLastValue;
  public row;
  public loginForm: FormGroup;
  public workerForm: FormGroup;
  public User:any;
  public workerformopen = false;
  public errors;
  public ernor;
  public ernznor;
  public active = 1;
  public stripeUrl;
  public numRegex = /^-?\d*[.]?\d{0,2}$/;
  public platform;
  public linkdata = null;
  private handleIncomingData = null;
  public loadingform = false;
  public show = false;


  public birthDateOptions: FlatpickrOptions = {
    altInput: true
  };

  public selectMultiLanguages = ['English', 'Spanish', 'French', 'Russian', 'German', 'Arabic', 'Sanskrit'];
  public selectMultiLanguagesSelected = [];

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {Router} router
   * @param {CompanyEditService} _companyEditService
   */
  constructor (
    private router: Router,
    private _companyEditService: CompanyEditService,
    private _coreTranslationService: CoreTranslationService,
    private location: Location,
    private _formBuilder: FormBuilder,
    private stripeService: StripeService,
    private activatedRoute: ActivatedRoute,
    public events: EventsService,
    private change: ChangeDetectorRef,
  ) {
    this.stripeUrl = environment.serverUrl;
    this._unsubscribeAll = new Subject();
    this.urlLastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
    this._coreTranslationService.translate(finnish, english, french, german, portuguese);
    this.activatedRoute.queryParams.subscribe(params => {
      let nav = params['nav'];
      if (nav === undefined || nav === null) {
        this.active = 1;
      } else {
        this.active = parseInt(nav);
      }
      // console.log(nav); // Print the parameter to the console. 
    });
    this.handleIncomingData = linkdata => {
      this.handleIncomingDeepLinkData(linkdata);
    };
    // Browser.addListener('browserFinished', (err, result) => {
    //   console.log(err, result, 'browserFinished');
    //   if (err) {
    //     return console.warn('[Browser-Finished] Something went wrong!');
    //   }
    //   console.log(result, 'Browser Finished Listen Result');
    // });
    // Browser.addListener("browserFinished", (info: any) => {
    //   console.log(info);
    // });
  }

  async handleIncomingDeepLinkData(data: any) {
    console.log('handleIncomingDeepLinkData');
    if (data.status == 1) {
      // console.log('Tapahtuma ok');
      // Heitä ilmoitus kortti tapahtumasta
    } else {
      // console.log('Tapahtuma peruttu');
    }
  }

  get f() {
    return this.loginForm.controls;
  }
  get ff() {
    return this.workerForm.controls;
  }

  getData() {
    this._companyEditService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.row = response;
      // console.log(this.row);
      if (this.row.users.length > 0) {
        this.row.users.forEach(element => {
          if (this.User && this.User.id == element.id) {
            this.show = true;
          }
        });
      }
    });
  }
  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this.events.subscribe('deeplink', this.handleIncomingData);
    
    Browser.addListener('browserFinished', () => {
      console.log('suljettu browserFinished');
      this._companyEditService.getApiData(this.row.id);
    });
    App.addListener('appUrlOpen', data => {
      if (data.url.indexOf('trafipoint://') > -1) {
        Browser.close().then(() => {
          console.log('suljettu appUrlOpen');
          setTimeout(() => {
            this._companyEditService.getApiData(this.row.id).then(() => {
              console.log('getapidata tehty');
              this.change.detectChanges();
              console.log('change.detectChanges');
            });
          }, 100);

        });
      }
    });

    Device.getInfo().then(info => {
      this.platform = info.platform;
    });
    this.User = JSON.parse(localStorage.getItem('currentUser'));
    this.getData();
    if (this.User && (this.User.role_id == 1 || this.User.role_id == 2)) {
      this.show = true;
    }
    this.loginForm = this._formBuilder.group({
      contactperson: [this.row.contactperson, Validators.required],
      name: [this.row.name, Validators.required],
      business_id: [this.row.business_id, Validators.required],
      email: [this.row.email, [Validators.required, Validators.email]],
      mobile: [this.row.mobile, Validators.required],
      subscription_price: [this.row.subscription_price, [Validators.required, Validators.pattern(this.numRegex)]],
      click_price: [this.row.click_price],
      event_muutto_price: [this.row.event_muutto_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_rahti_price: [this.row.event_rahti_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_ajoneuvot_price: [this.row.event_ajoneuvot_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_massa_price: [this.row.event_massa_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_paketti_price: [this.row.event_paketti_price, [Validators.required, Validators.pattern(this.numRegex)]],
    });
    this.workerForm = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mobile: ['', Validators.required],
      role: ['', Validators.required],
    });
    this.stripeTest = this._formBuilder.group({
      namee: ['', [Validators.required]]
    });
  }

  ngOnDestroy(): void {
    this.events.unsubscribe('deeplink', this.handleIncomingData);
  }
  goBack() {
    this.location.back();
    // console.log('goBack()...');
  }

  toggleWorkerForm() {
    if (this.workerformopen) {
      this.workerformopen = false;
    } else if (!this.workerformopen) {
      this.workerformopen = true;
    }
  }
  addWorker() {
    this.loadingform = true;
    this._companyEditService.addWorker(
      this.row.id,
      this.ff.firstName.value,
      this.ff.lastName.value,
      this.ff.email.value,
      this.ff.mobile.value,
      this.ff.role.value,
    ).then(() => {
      this._companyEditService.getApiData(this.row.id);
      this.workerForm.reset();
      this.loadingform = false;
    }).catch((msg) => {
      this.errors = msg;
      this.loadingform = false;
    });;
  }

  submit(id: number) {
    // console.log(this.f);
    this._companyEditService.updateCompany(
      id,
      this.f.contactperson.value,
      this.f.name.value,
      this.f.business_id.value,
      this.f.email.value,
      this.f.mobile.value,
      this.f.subscription_price.value,
      this.f.click_price.value,

      this.f.event_muutto_price.value,
      this.f.event_rahti_price.value,
      this.f.event_ajoneuvot_price.value,
      this.f.event_massa_price.value,
      this.f.event_paketti_price.value,

    ).catch((e) => {
      this.ernor = e;
      // console.log(e);
    });
  }

  detachUser(userid: number, companyid: number, name:string, lname:string) {
    if (confirm("Haluatko varmasti poistaa yrityksestä käyttäjän: " + name + ' ' + lname)) {
      this._companyEditService.detachUser(
        userid,
        companyid
      );
    }
  }


  cancelSubscription(companyid: number) {
    if (confirm("Haluatko varmasti lopettaa tilauksen? Voit silti käyttää palvelua kuukauden loppuun saakka. Tämän jälkeen sinulta veloitetaan tämän kuun tapahtumakulut, mutta tilauksesi ei jatku.")) {
      this._companyEditService.cancelSubscription(
        companyid
      ).then((resp) => {
        // this.getData();
        this._companyEditService.getApiData(this.row.id);
        // this.row.is_subscribed = 0;
        // this.row.subscription_ends_at = resp;
        this.ernznor = "";
      }).catch((e) => {
        this.ernznor = e;
      });
    }
  }
  continueSubscription(companyid: number) {
    if (confirm("Haluatko jatkaa tilausta? Sinulta veloitetaan loppukuukauden käyttömaksu, mikäli sitä ei ole vielä veloitettu.")) {
      this._companyEditService.continueSubscription(
        companyid
      ).then(() => {
        // this.getData();
        this._companyEditService.getApiData(this.row.id);
        // this.row.is_subscribed = 0;
        // this.row.subscription_ends_at = null;
        this.ernznor = "";
      }).catch((e) => {
        this.ernznor = e;
      });
    }
  }
  deleteCard(companyid: number) {
    if (confirm("Haluatko varmasti poistaa maksukortin?")) {
      this._companyEditService.deleteCard(
        companyid
      ).then(() => {
        // this.getData();
        this._companyEditService.getApiData(this.row.id);
        // this.row.card_brand = null;
        this.ernznor = "";
      }).catch((e) => {
        this.ernznor = e;
      });
    }
  }

  openCardPage() {
    Browser.open({
      url: this.stripeUrl + '/stripe/?company=' + this.row.stripe_id + '&platform=' + this.platform,
      toolbarColor: '#084259',
      presentationStyle: 'popover'
    }).then(() => {
      // addListener("browserFinished", (info: any) => {
    //   console.log(info);
    // });
      // console.log('juu');
      // this._companyEditService.getApiData(this.row.id);
      console.log('openCardPage then')
    }).catch(err => console.error(err));
  }


  pauseSubscription(companyid: number) {
    if (confirm("Haluatko varmasti asettaa tilauksen tauolle? Tällöin et voi avata uusia tarjouksia.")) {
      this._companyEditService.pauseSubscription(
        companyid
      ).then(() => {
        this.getData();
        this.row.is_subscribed = 0;
        this._companyEditService.getApiData(this.row.id);
      }).catch((e) => {
        this.ernznor = e;
      });
    }
  }
  unpauseSubscription(companyid: number) {
    if (confirm("Haluatko varmasti jatkaa tilausta?")) {
      this._companyEditService.unpauseSubscription(
        companyid
      ).then(() => {
        this.getData();
        this.row.is_subscribed = 1;
        this._companyEditService.getApiData(this.row.id);
      }).catch((e) => {
        this.ernznor = e;
      });
    }
  }

  createToken(): void {
    const name = this.stripeTest.get('namee').value;
    // console.log(name);
    // console.log(this.card.element);
    // if (this.row.stripe_id == null) {
    //   this.stripeService
    //     .customers
    //     .create(this.card.element, { name })
    //     .subscribe((result) => {
    //       if (result.token) {
    //         // Use the token
    //         console.log(result);
    //         console.log(result.token.id);
    //       } else if (result.error) {
    //         // Error creating the token
    //         console.log(result.error.message);
    //       }
    //     });
    // }
    this.stripeService
      .createToken(this.card.element, { name })
      .subscribe((result) => {
        if (result.token) {
          // Use the token
          // this.stripeService.createSource(this.card.element, result.token).subscribe((res) => {
          //   console.log(res);
          // });
          // this._companyEditService.addCard(
          //   result.token
          // );
          // console.log(result);
          // this._companyEditService.addCard(
          //   result.token.id
          // );
        } else if (result.error) {
          // Error creating the token
          // console.log(result.error.message);
        }
      });
  }

}

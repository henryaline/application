import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class CompanyEditService implements Resolve<any> {
  public apiData: any;
  public onDataChanged: BehaviorSubject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _toastrService: ToastrService, private _router: Router) {
    // Set the defaults
    this.onDataChanged = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    let currentId = Number(route.paramMap.get('id'));
    return new Promise<void>((resolve, reject) => {
      // Promise.all([this.getApiData()]).then(() => {
      Promise.all([this.getApiData(currentId)]).then(() => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get API Data
   */
  getApiData(id:number): Promise<any[]> {
    // const url = `${environment.apiUrl}/companies`;
    const url = `${environment.apiUrl}/companies/get/${id}`;

    return new Promise((resolve, reject) => {
      // this._httpClient.get('http://localhost:4000/api/users-data').subscribe((response: any) => {
      this._httpClient.get(url).subscribe((response: any) => {
        // this.apiData = response;
        this.apiData = response.companies.find(i => i.id == id);
        this.onDataChanged.next(this.apiData);
        resolve(this.apiData);
      }, reject);
    });
  }

  updateCompany(id: number, contactperson: string, name: string, business_id: string,
                email: string, mobile: string, subscription_price: string, click_price: string,
    event_muutto_price: string, event_rahti_price: string, event_ajoneuvot_price: string, event_massa_price: string, event_paketti_price: string): Promise<any[]> {
    const url = `${environment.apiUrl}/companies/edit`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id, contactperson, name, business_id, email, mobile, subscription_price, click_price, event_muutto_price, event_rahti_price, event_ajoneuvot_price, event_massa_price, event_paketti_price }).subscribe((response: any) => {
        this._router.navigate(['/apps/company/company-view/' + id]);
        setTimeout(() => {
          this._toastrService.success(
            'Yrityksen tiedot päivitetty onnistuneesti.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }
  addWorker(companyid: number, firstName: string, lastName: string, email: string, mobile: string, role: number): Promise<any[]> {
    const url = `${environment.apiUrl}/users/addworker`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { companyid, firstName, lastName, email, mobile, role }).subscribe((response: any) => {
        // this._router.navigate(['/apps/company/company-view/' + id]);
        setTimeout(() => {
          this._toastrService.success(
            'Käyttäjä lisätty yritykseen.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }
  detachUser(userid: number, companyid: number): Promise<any[]> {
    const url = `${environment.apiUrl}/companies/detach-user`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { userid, companyid }).subscribe((response: any) => {
        this._router.navigate(['/apps/company/company-view/' + companyid]);
        setTimeout(() => {
          this._toastrService.success(
            'Käyttäjä poistettu yrityksestä onnistuneesti.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }

  cancelSubscription(company_id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/stripe/cancel`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { company_id }).subscribe((response: any) => {
        setTimeout(() => {
          this._toastrService.success(
            'Tilaus lopetettu.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
            );
          }, 2500);
          // console.log(response);
        // resolve(response);
        resolve(response.ending);
        // resolve(response.message);
        }, reject);
      });
  }
  continueSubscription(company_id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/stripe/continue`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { company_id }).subscribe((response: any) => {
        setTimeout(() => {
          this._toastrService.success(
            'Tilaus jatkettu.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }
  deleteCard(company_id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/stripe/delete-payment-method`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { company_id }).subscribe((response: any) => {
        setTimeout(() => {
          this._toastrService.success(
            'Maksukortti poistettu.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }

  pauseSubscription(company_id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/stripe/pause`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { company_id }).subscribe((response: any) => {
        setTimeout(() => {
          this._toastrService.success(
            'Tilaus asetettu tauolle.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }
  unpauseSubscription(company_id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/stripe/unpause`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { company_id }).subscribe((response: any) => {
        setTimeout(() => {
          this._toastrService.success(
            'Tilausta jatkettu.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }

  addCard(token): Promise<any[]> {
    const url = `${environment.apiUrl}/companies/add-card`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { token }).subscribe((response: any) => {
        // this._router.navigate(['/apps/company/company-view/' + companyid]);
        setTimeout(() => {
          this._toastrService.success(
            response.message,
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }

}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FlatpickrOptions } from 'ng2-flatpickr';

import { CompanyAddService } from 'app/main/apps/company/company-add/company-add.service';

import { CoreTranslationService } from '@core/services/translation.service';

import { locale as english } from 'app/main/apps/company/i18n/en';
import { locale as finnish } from 'app/main/apps/company/i18n/fi';
import { locale as french } from 'app/main/apps/company/i18n/fr';
import { locale as german } from 'app/main/apps/company/i18n/de';
import { locale as portuguese } from 'app/main/apps/company/i18n/pt';

import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-company-add',
  templateUrl: './company-add.component.html',
  styleUrls: ['./company-add.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CompanyAddComponent implements OnInit {
  // Public
  public url = this.router.url;
  public urlLastValue;
  public row;
  public loginForm: FormGroup;
  public errorz;
  public prices;
  public numRegex = /^-?\d*[.]?\d{0,2}$/;


  public birthDateOptions: FlatpickrOptions = {
    altInput: true
  };

  public selectMultiLanguages = ['English', 'Spanish', 'French', 'Russian', 'German', 'Arabic', 'Sanskrit'];
  public selectMultiLanguagesSelected = [];

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {Router} router
   * @param {CompanyAddService} _companyAddService
   */
  constructor(private router: Router, private _companyAddService: CompanyAddService,
    private _coreTranslationService: CoreTranslationService,
    private location: Location,
    private _formBuilder: FormBuilder,) {
    this._unsubscribeAll = new Subject();
    this.urlLastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
    this._coreTranslationService.translate(finnish, english, french, german, portuguese);

  }

  get f() {
    return this.loginForm.controls;
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this._companyAddService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.row = response;
      // console.log(this.row);
    });
    this.loginForm = this._formBuilder.group({
      contactperson: [this.row.contactperson, Validators.required],
      name: [this.row.name, Validators.required],
      business_id: [this.row.business_id, Validators.required],
      email: [this.row.email, [Validators.required, Validators.email]],
      mobile: [this.row.mobile, Validators.required],

      subscription_price: [this.row.defaultPricing.monthly_subscription_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_muutto_price: [this.row.defaultPricing.event_muutto_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_rahti_price: [this.row.defaultPricing.event_rahti_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_ajoneuvot_price: [this.row.defaultPricing.event_ajoneuvot_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_massa_price: [this.row.defaultPricing.event_massa_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_paketti_price: [this.row.defaultPricing.event_paketti_price, [Validators.required, Validators.pattern(this.numRegex)]],
      
      click_price: [this.row.click_price],

    });
  }

  goBack() {
    this.location.back();
    // console.log('goBack()...');
  }

  submit() {
    this._companyAddService.addCompany(
      this.f.contactperson.value,
      this.f.name.value,
      this.f.business_id.value,
      this.f.email.value,
      this.f.mobile.value,
      this.f.subscription_price.value,
      this.f.click_price.value,
      this.f.event_muutto_price.value,
      this.f.event_rahti_price.value,
      this.f.event_ajoneuvot_price.value,
      this.f.event_massa_price.value,
      this.f.event_paketti_price.value
    ).catch(e => {
      this.errorz = e;
    });
  }
}

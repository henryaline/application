import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class CompanyAddService implements Resolve<any> {
  public apiData: any;
  public onDataChanged: BehaviorSubject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _toastrService: ToastrService, private _router: Router) {
    // Set the defaults
    this.onDataChanged = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    // let currentId = Number(route.paramMap.get('id'));
    return new Promise<void>((resolve, reject) => {
      // Promise.all([this.getApiData()]).then(() => {
      Promise.all([this.getApiData()]).then(() => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get API Data
   */
  getApiData(): Promise<any[]> {
    const url = `${environment.apiUrl}/companies`;

    return new Promise((resolve, reject) => {
      // this._httpClient.get('http://localhost:4000/api/users-data').subscribe((response: any) => {
      this._httpClient.get(url).subscribe((response: any) => {
        this.apiData = response;
        // this.apiData = response.companies.find(i => i.id == id);
        this.onDataChanged.next(this.apiData);
        resolve(this.apiData);
      }, reject);
    });
  }

  addCompany(contactperson: string, name: string, business_id: string, email: string, mobile: string, subscription_price: string, click_price: string,
    event_muutto_price: string, event_rahti_price: string, event_ajoneuvot_price: string, event_massa_price: string, event_paketti_price: string): Promise<any[]> {
    const url = `${environment.apiUrl}/companies/add`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { contactperson, name, business_id, email, mobile, subscription_price, click_price, event_muutto_price, event_rahti_price, event_ajoneuvot_price, event_massa_price, event_paketti_price }).subscribe((response: any) => {
        this._router.navigate(['/apps/company/company-view/' + response.company.id]);
        setTimeout(() => {
          this._toastrService.success(
            'Yritys lisätty onnistuneesti.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';

import { CoreCommonModule } from '@core/common.module';
import { CoreDirectivesModule } from '@core/directives/directives';
import { CorePipesModule } from '@core/pipes/pipes.module';
import { CoreSidebarModule } from '@core/components';

import { InvoiceListService } from 'app/main/apps/invoice/invoice-list/invoice-list.service';
import { InvoiceModule } from 'app/main/apps/invoice/invoice.module';

import { CompanyEditComponent } from 'app/main/apps/company/company-edit/company-edit.component';
import { CompanyEditService } from 'app/main/apps/company/company-edit/company-edit.service';

import { CompanyAddComponent } from 'app/main/apps/company/company-add/company-add.component';
import { CompanyAddService } from 'app/main/apps/company/company-add/company-add.service';

import { CompanyListComponent } from 'app/main/apps/company/company-list/company-list.component';
import { CompanyListService } from 'app/main/apps/company/company-list/company-list.service';

import { CompanyViewComponent } from 'app/main/apps/company/company-view/company-view.component';
import { CompanyViewService } from 'app/main/apps/company/company-view/company-view.service';
import { NewCompanySidebarComponent } from 'app/main/apps/company/company-list/new-company-sidebar/new-company-sidebar.component';

import { AddPaymentSidebarPreviewComponent } from 'app/main/apps/company/company-view/sidebar/add-payment-sidebar-preview/add-payment-sidebar-preview.component';
import { SendTenderSidebarPreviewComponent } from 'app/main/apps/company/company-view/sidebar/send-tender-sidebar-preview/send-tender-sidebar-preview.component';

import { TranslateModule } from '@ngx-translate/core';

// Import your library
import { NgxStripeModule } from 'ngx-stripe';

const curruser: any = JSON.parse(localStorage.getItem('currentUser'));
let redirRouteV: any = '/apps/company/company-list';
let redirRouteE: any = '/apps/company/company-list';
if (curruser && curruser.company) {
  // console.log('joo');
  redirRouteV = '/apps/company/company-view/' + curruser.company.id;
  redirRouteE= '/apps/company/company-edit/' + curruser.company.id;
}
// routing
const routes: Routes = [
  {
    path: 'company-add',
    component: CompanyAddComponent,
    resolve: {
      uls: CompanyAddService
    }
  },
  {
    path: 'company-list',
    component: CompanyListComponent,
    resolve: {
      uls: CompanyListService
    }
  },
  {
    path: 'company-view/:id',
    component: CompanyViewComponent,
    // resolve: {
    //   data: CompanyViewService,
    //   InvoiceListService
    // },
    resolve: {
      uls: CompanyViewService
    },
    data: { path: 'view/:id' }
  },
  {
    path: 'company-edit/:id',
    component: CompanyEditComponent,
    resolve: {
      ues: CompanyEditService
    }
  },
  {
    path: 'company-view',
    // redirectTo: '/apps/company/company-view/' + (curruser.company.id ?? null) // Redirection
    redirectTo: redirRouteV
  },
  {
    path: 'company-edit',
    // redirectTo: '/apps/company/company-edit/' + (curruser.company.id ?? null) // Redirection
    redirectTo: redirRouteE
  }
];

@NgModule({
  declarations: [CompanyListComponent, CompanyViewComponent, CompanyEditComponent, CompanyAddComponent, NewCompanySidebarComponent, SendTenderSidebarPreviewComponent, AddPaymentSidebarPreviewComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreCommonModule,
    FormsModule,
    NgbModule,
    NgSelectModule,
    Ng2FlatpickrModule,
    NgxDatatableModule,
    CorePipesModule,
    CoreDirectivesModule,
    InvoiceModule,
    CoreSidebarModule,
    TranslateModule,
    NgxStripeModule.forRoot('pk_test_51Hc33MCjKv7aQAwPQXckpeJTHcxS9LOOADNX4X5faiw3u2DRxcl7VtjghEOoVcg6DfdaoYZiZFPeF3ddabQlSC2K00WB3RiZKG'),
  ],
  providers: [CompanyListService, CompanyViewService, CompanyEditService, CompanyAddService]
})
export class CompanyModule {}

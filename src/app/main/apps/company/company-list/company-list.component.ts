import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { CoreSidebarService } from '@core/components/core-sidebar/core-sidebar.service';

import { CompanyListService } from 'app/main/apps/company/company-list/company-list.service';

import { CoreTranslationService } from '@core/services/translation.service';

import { locale as english } from 'app/main/apps/company/i18n/en';
import { locale as finnish } from 'app/main/apps/company/i18n/fi';
import { locale as french } from 'app/main/apps/company/i18n/fr';
import { locale as german } from 'app/main/apps/company/i18n/de';
import { locale as portuguese } from 'app/main/apps/company/i18n/pt';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CompanyListComponent implements OnInit {
  // Public
  public sidebarToggleRef = false;
  public rows;
  public selectedOption = 10;
  public ColumnMode = ColumnMode;
  public curruser: any;

  // Decorator
  @ViewChild(DatatableComponent) table: DatatableComponent;

  // Private
  private tempData = [];
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {CompanyListService} _companyListService
   * @param {CoreSidebarService} _coreSidebarService
   */
  constructor(private _companyListService: CompanyListService, private _coreSidebarService: CoreSidebarService,
              private _coreTranslationService: CoreTranslationService) {
    this._unsubscribeAll = new Subject();
    this._coreTranslationService.translate(finnish, english, french, german, portuguese);
  }

  // Public Methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * filterUpdate
   *
   * @param event
   */
  filterUpdate(event) {
    const val = event.target.value.toLowerCase();

    // Filter Our Data
    const temp = this.tempData.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // Update The Rows
    this.rows = temp;
    // Whenever The Filter Changes, Always Go Back To The First Page
    this.table.offset = 0;
  }

  deleteRow(id: number, name:string) {
    // console.log(id);
    if (confirm("Haluatko deaktivoida yrityksen: " + name)) {
      this._companyListService.deleteCompany(
        id
      ).then(() => {
        this.loadData();
      });
    }
  }

  /**
   * Toggle the sidebar
   *
   * @param name
   */
  toggleSidebar(name): void {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this.curruser = JSON.parse(localStorage.getItem('currentUser'));
    this.loadData();
  }
  
  loadData() {
    this._companyListService.onDatatablessChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.rows = response;
      this.tempData = this.rows;
    });
  }
}

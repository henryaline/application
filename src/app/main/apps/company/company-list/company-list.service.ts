import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable()
export class CompanyListService implements Resolve<any> {
  public rows: any;
  public onDatatablessChanged: BehaviorSubject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _toastrService: ToastrService, private _router: Router) {
    // Set the defaults
    this.onDatatablessChanged = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise<void>((resolve, reject) => {
      Promise.all([this.getDataTableRows()]).then(() => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get rows
   */
  getDataTableRows(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      // this._httpClient.get('api/users-data').subscribe((response: any) => {
      this._httpClient.get(`${environment.apiUrl}/companies`).subscribe((response: any) => {
        this.rows = response.companies;
        this.onDatatablessChanged.next(this.rows);
        // console.log(this.rows);
        resolve(this.rows);
      }, reject);
    });
  }

  deleteCompany(id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/companies/deactivate`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id }).subscribe((response: any) => {
        // this._router.navigate(['/apps/company/company-list']);
        // this.redirectTo('/apps/company/company-list');
        setTimeout(() => {
          this._toastrService.success(
            'Yritys deaktivoitu',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        resolve(response.message);
      }, reject);
    });
  }

  redirectTo(uri: string) {
    this._router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
      this._router.navigate([uri]));
  }
}

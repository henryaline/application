import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { CoreSidebarService } from '@core/components/core-sidebar/core-sidebar.service';

import { InvoicePreviewService } from 'app/main/apps/invoice/invoice-preview/invoice-preview.service';

// import { Printer, PrintOptions } from '@awesome-cordova-plugins/printer/ngx';
import { Device } from '@capacitor/device';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-invoice-preview',
  templateUrl: './invoice-preview.component.html',
  styleUrls: ['./invoice-preview.service.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoicePreviewComponent implements OnInit {
  // public
  public apiData;
  public urlLastValue;
  public url = this.router.url;
  public sidebarToggleRef = false;
  public paymentSidebarToggle = false;
  public paymentDetails = {
    totalDue: '$12,110.55',
    bankName: 'American Bank',
    country: 'United States',
    iban: 'ETD95476213874685',
    swiftCode: 'BR91905'
  };
  public platform;
  // private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {Router} router
   * @param {InvoicePreviewService} _invoicePreviewService
   * @param {CoreSidebarService} _coreSidebarService
   */
  constructor(
    private router: Router,
    private _invoicePreviewService: InvoicePreviewService,
    private _coreSidebarService: CoreSidebarService,
    // private printer: Printer
  ) {
    this._unsubscribeAll = new Subject();
    this.urlLastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
  }

  // Public Methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Toggle the sidebar
   *
   * @param name
   */
  toggleSidebar(name): void {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this._invoicePreviewService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.apiData = response;
      // console.log(this.apiData);
    });
    Device.getInfo().then(info => {
      this.platform = info.platform;
    });
  }

  print(id) {
    // console.log(this.platform);
    if (this.platform == 'web') {
      window.print();
    } else {
      const url = `${environment.serverUrl}/invoice/${id}`;
      window.open(url);
      // this.printer.isAvailable().then(() => {
      //   this.printer.print();
      // }).catch((e) => {
      //   console.log(e);
      // });
      // let options: PrintOptions = {
      //   name: 'MyDocument',
      //   duplex: true,
      //   orientation: 'landscape',
      //   monochrome: true
      // } 
    }
  }
}

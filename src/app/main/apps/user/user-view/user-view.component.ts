import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { UserViewService } from 'app/main/apps/user/user-view/user-view.service';

import { CoreTranslationService } from '@core/services/translation.service';

import { locale as english } from 'app/main/apps/i18n/en';
import { locale as finnish } from 'app/main/apps/i18n/fi';
import { locale as french } from 'app/main/apps/user/i18n/fr';
import { locale as german } from 'app/main/apps/user/i18n/de';
import { locale as portuguese } from 'app/main/apps/user/i18n/pt';
import { Device } from '@capacitor/device';
import { App } from '@capacitor/app';


@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserViewComponent implements OnInit {
  // public
  public url = this.router.url;
  public lastValue;
  public data;
  public curruser: any;
  public showdata = false;

  // private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {Router} router
   * @param {UserViewService} _userViewService
   */
  constructor(private router: Router, private _userViewService: UserViewService,
              private _coreTranslationService: CoreTranslationService) {
    this._unsubscribeAll = new Subject();
    this.lastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
    this._coreTranslationService.translate(finnish, english, french, german, portuguese);
  }

  delete(firstName: string, lastName: string, id: number) {
    if (confirm("Haluatko poistaa pysyvästi käyttäjän: " + firstName + ' ' + lastName)) {
      // console.log("Implement delete functionality here");
      this._userViewService.deleteUser(id);
    }
  }
  activate(firstName: string, lastName: string, id: number) {
    if (confirm("Haluatko aktivoida käyttäjän: " + firstName + ' ' + lastName)) {
      // console.log("Implement delete functionality here");
      this._userViewService.activateUser(id).then(() => { this.updateData(id); });
    }
  }
  deactivate(firstName: string, lastName: string, id: number) {
    if (confirm("Haluatko deaktivoida käyttäjän: " + firstName + ' ' + lastName)) {
      // console.log("Implement delete functionality here");
      this._userViewService.deactivateUser(id).then(() => { this.updateData(id); });
    }
  }

  notificate() {
    const token = '1F4EEF81889EEBF1E895911A7202CA6268735B86CC2893C59B0F17C4367B2466';
    const token2 = '5AD7074990C0D24B50F7EEF3642E54117222A933AC46FEF51AC2EDA7BE78CE68';
    const token3 = '99842555';
    const token4 = 'F55C82608F4608AF5131D8E99738A93C21EEE10B93BA3B2D0163C692883BF17C';
    const userid = 1;
    let appv = '';
    // App.getInfo().then(ai => {
    //   appv = ai.version;
    // })
    Device.getInfo().then(info => {
      // const uid = this.auth.isAuthenticated() ? this.auth.getUser().id : '';

      const dets = {
        // device: info.name,
        platform: info.platform,
        // app_version: appv
      }

      // console.log(dets);

    });
    // this._userViewService.SubNotifications(token4, userid).then((rsp) => { console.log(rsp); });
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this.curruser = JSON.parse(localStorage.getItem('currentUser'));
    this.getData();
  }
  getData() {
    this._userViewService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.data = response;
      // console.log(this.data);
      // if (this.curruser && (this.curruser.id == this.data.id || this.curruser.role_id == 1 || this.curruser.role_id == 2)) {
      if (this.data) {
        this.showdata = true;
      }
    });
  }
  updateData(id) {
    this._userViewService.getApiData(id);
  }
}

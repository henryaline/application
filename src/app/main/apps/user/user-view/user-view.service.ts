import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable()
export class UserViewService implements Resolve<any> {
  public rows: any;
  public onDataChanged: BehaviorSubject<any>;
  public id;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _toastrService: ToastrService, private _router: Router) {
    // Set the defaults
    this.onDataChanged = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    let currentId = Number(route.paramMap.get('id'));
    return new Promise<void>((resolve, reject) => {
      Promise.all([this.getApiData(currentId)]).then(() => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get rows
   */
  getApiData(id: number): Promise<any[]> {
    // const url = `api/users-data/${id}`;
    // const url = `${environment.apiUrl}/users`;
    const url = `${environment.apiUrl}/users/get/${id}`;

    return new Promise((resolve, reject) => {
      this._httpClient.get(url).subscribe((response: any) => {
        // this.rows = response;
        this.rows = response.users.find(i => i.id == id);
        this.onDataChanged.next(this.rows);
        resolve(this.rows);
      }, reject);
    });
  }

  /**
   * Get rows
   */
  deleteUser(id: number): Promise<any[]> {
    // const url = `api/users-data/${id}`;

    // const url = `${environment.apiUrl}/companies/${id}`;
    const url = `${environment.apiUrl}/users/delete`;

    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id }).subscribe((response: any) => {
        // this._httpClient.get(url).subscribe((response: any) => {
        // this.rows = response.companies.find(i => i.id == id);
        // this.onDataChanged.next(this.rows);
        // console.log(this.rows);
        // Display welcome toast!

        this._router.navigate(['/apps/user/user-list']);

        setTimeout(() => {
          this._toastrService.success(
            'Käyttäjä poistettu onnistuneesti',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        resolve(response.message);
      }, reject);
    });
  }
  deactivateUser(id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/users/deactivate`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id }).subscribe((response: any) => {
        setTimeout(() => {
          this._toastrService.success(
            'Käyttäjä deaktivoitu',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        resolve(response.message);
      }, reject);
    });
  }
  activateUser(id: number): Promise<any[]> {
    const url = `${environment.apiUrl}/users/activate`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id }).subscribe((response: any) => {
        setTimeout(() => {
          this._toastrService.success(
            'Käyttäjä aktivoitu',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        resolve(response.message);
      }, reject);
    });
  }

  SubNotifications(expo_token: string, userid: number): Promise<any[]> {
    // const url = `${environment.apiUrl}/exponent/devices/subscribe`;
    const url = `${environment.apiUrl}/notificationsub`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { expo_token, userid }).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  // SubNotifications(expo_token: string, userid: number) {
  //   return this._httpClient
  //     .post<any>(`${environment.apiUrl}/notificationsub`, { expo_token, userid })
  //     .pipe(
  //       map(response => {
  //         return response;
  //       })
  //     );
  // }
}

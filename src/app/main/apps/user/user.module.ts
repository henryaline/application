import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';

import { CoreCommonModule } from '@core/common.module';
import { CoreDirectivesModule } from '@core/directives/directives';
import { CorePipesModule } from '@core/pipes/pipes.module';
import { CoreSidebarModule } from '@core/components';

import { InvoiceListService } from 'app/main/apps/invoice/invoice-list/invoice-list.service';
import { InvoiceModule } from 'app/main/apps/invoice/invoice.module';

import { UserEditComponent } from 'app/main/apps/user/user-edit/user-edit.component';
import { UserEditService } from 'app/main/apps/user/user-edit/user-edit.service';

import { UserListComponent } from 'app/main/apps/user/user-list/user-list.component';
import { UserListService } from 'app/main/apps/user/user-list/user-list.service';

import { UserAddComponent } from 'app/main/apps/user/user-add/user-add.component';
import { UserAddService } from 'app/main/apps/user/user-add/user-add.service';

import { UserViewComponent } from 'app/main/apps/user/user-view/user-view.component';
import { UserViewService } from 'app/main/apps/user/user-view/user-view.service';
import { NewUserSidebarComponent } from 'app/main/apps/user/user-list/new-user-sidebar/new-user-sidebar.component';

import { TranslateModule } from '@ngx-translate/core';


const curruser: any = JSON.parse(localStorage.getItem('currentUser'));
let redirRouteV: any = '/apps/user/user-view/';
let redirRouteE: any = '/apps/user/user-edit/';
if (curruser) {
  redirRouteV = '/apps/user/user-view/' + curruser.id;
  redirRouteE = '/apps/user/user-edit/' + curruser.id;
}

// routing
const routes: Routes = [
  {
    path: 'user-add',
    component: UserAddComponent,
    resolve: {
      uls: UserAddService
    }
  },
  {
    path: 'user-list',
    component: UserListComponent,
    resolve: {
      uls: UserListService
    }
  },
  {
    path: 'user-view/:id',
    component: UserViewComponent,
    resolve: {
      data: UserViewService,
      InvoiceListService
    },
    data: { path: 'view/:id' }
  },
  {
    path: 'user-edit/:id',
    component: UserEditComponent,
    resolve: {
      ues: UserEditService
    }
  },
  // Tähän hae kirjautuneen käyttäjän id
  {
    path: 'user-view',
    // redirectTo: '/apps/user/user-view/' + (curruser.id ?? null), // Redirection
    redirectTo: redirRouteV
  },
  {
    path: 'user-edit',
    // redirectTo: '/apps/user/user-edit/' + (curruser.id ?? null) // Redirection
    redirectTo: redirRouteE
  }
];

@NgModule({
  declarations: [UserListComponent, UserViewComponent, UserEditComponent, UserAddComponent, NewUserSidebarComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreCommonModule,
    FormsModule,
    NgbModule,
    NgSelectModule,
    Ng2FlatpickrModule,
    NgxDatatableModule,
    CorePipesModule,
    CoreDirectivesModule,
    InvoiceModule,
    CoreSidebarModule,
    TranslateModule
  ],
  providers: [UserListService, UserViewService, UserEditService, UserAddService]
})
export class UserModule {
}

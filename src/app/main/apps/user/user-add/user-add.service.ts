import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class UserAddService implements Resolve<any> {
  public apiData: any;
  public onDataChanged: BehaviorSubject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _toastrService: ToastrService, private _router: Router) {
    // Set the defaults
    this.onDataChanged = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    let currentId = Number(route.paramMap.get('id'));
    return new Promise<void>((resolve, reject) => {
      // Promise.all([this.getApiData()]).then(() => {
      Promise.all([this.getApiData()]).then(() => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get API Data
   */
  getApiData(): Promise<any[]> {
    const url = `${environment.apiUrl}/companies`;

    return new Promise((resolve, reject) => {
      // this._httpClient.get('api/users-data').subscribe((response: any) => {
      this._httpClient.get(url).subscribe((response: any) => {
        // this.apiData = response;
        this.apiData = response;
        this.onDataChanged.next(this.apiData);
        resolve(this.apiData);
      }, reject);
    });
  }

  addUser(firstname: string, lastname: string, email: string, mobile: string, role: number, add_company: string,
    c_name: string, c_businessid: string, c_contact: string, c_email: string, c_mobile: string, c_subscription_price: string, c_click_price: string,
    event_muutto_price: string, event_rahti_price: string, event_ajoneuvot_price: string, event_massa_price: string, event_paketti_price: string, companyid: number): Promise<any[]> {
    const url = `${environment.apiUrl}/users/add`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { firstname, lastname, email, mobile, role, add_company, c_name, c_businessid, c_contact, c_email, c_mobile, c_subscription_price, c_click_price, event_muutto_price, event_rahti_price, event_ajoneuvot_price, event_massa_price, event_paketti_price, companyid }).subscribe((response: any) => {
        this._router.navigate(['/apps/user/user-view/' + response.user.id]);
        setTimeout(() => {
          this._toastrService.success(
            'Käyttäjä lisätty onnistuneesti.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FlatpickrOptions } from 'ng2-flatpickr';

import { UserAddService } from 'app/main/apps/user/user-add/user-add.service';

import { CoreTranslationService } from '@core/services/translation.service';

import { locale as english } from 'app/main/apps/user/i18n/en';
import { locale as finnish } from 'app/main/apps/user/i18n/fi';
import { locale as french } from 'app/main/apps/user/i18n/fr';
import { locale as german } from 'app/main/apps/user/i18n/de';
import { locale as portuguese } from 'app/main/apps/user/i18n/pt';

import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserAddComponent implements OnInit {
  // Public
  public url = this.router.url;
  public urlLastValue;
  public row;
  public loginForm: FormGroup;
  public pwForm: FormGroup;
  public passwordTextTypeOld = false;
  public passwordTextTypeNew = false;
  public passwordTextTypeRetype = false;
  public curruser: any = JSON.parse(localStorage.getItem('currentUser'));
  public numRegex = /^-?\d*[.]?\d{0,2}$/;

  public errors;
  
  public existingCompany = false;
  public newCompany = false;

  public birthDateOptions: FlatpickrOptions = {
    altInput: true
  };

  public selectMultiLanguages = ['English', 'Spanish', 'French', 'Russian', 'German', 'Arabic', 'Sanskrit'];
  public selectMultiLanguagesSelected = [];

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {Router} router
   * @param {UserAddService} _userAddService
   */
  constructor(private router: Router, private _userAddService: UserAddService,
    private _coreTranslationService: CoreTranslationService,
    private location: Location,
    private _formBuilder: FormBuilder) {
    this._unsubscribeAll = new Subject();
    this.urlLastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
    this._coreTranslationService.translate(finnish, english, french, german, portuguese);

  }

  get f() {
    return this.loginForm.controls;
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this._userAddService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.row = response;
      // console.log(this.row);
    });
    this.loginForm = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mobile: ['', Validators.required],
      role: ['', Validators.required],

      add_company: ['', Validators.required],

      c_name: [''],
      c_businessid: [''],
      c_contact: [''],
      c_email: [''],
      c_mobile: [''],
      c_subscription_price: [''],
      c_click_price: [''],
      subscription_price: [this.row.defaultPricing.monthly_subscription_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_muutto_price: [this.row.defaultPricing.event_muutto_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_rahti_price: [this.row.defaultPricing.event_rahti_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_ajoneuvot_price: [this.row.defaultPricing.event_ajoneuvot_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_massa_price: [this.row.defaultPricing.event_massa_price, [Validators.required, Validators.pattern(this.numRegex)]], 
      event_paketti_price: [this.row.defaultPricing.event_paketti_price, [Validators.required, Validators.pattern(this.numRegex)]],

      companyid: [''],
    });
  }

  display(val) {
    if (val == 1) {
      this.existingCompany = true;
      this.newCompany = false;
    } else if (val == 2) {
      this.existingCompany = false;
      this.newCompany = true;
    } else {
      this.existingCompany = false;
      this.newCompany = false;
    }
  }

  goBack() {
    this.location.back();
  }

  submit() {
    this._userAddService.addUser(
      this.f.firstName.value,
      this.f.lastName.value,
      this.f.email.value,
      this.f.mobile.value,
      this.f.role.value,

      this.f.add_company.value,

      this.f.c_name.value,
      this.f.c_businessid.value,
      this.f.c_contact.value,
      this.f.c_email.value,
      this.f.c_mobile.value,
      this.f.subscription_price.value,
      this.f.c_click_price.value,

      this.f.event_muutto_price.value,
      this.f.event_rahti_price.value,
      this.f.event_ajoneuvot_price.value,
      this.f.event_massa_price.value,
      this.f.event_paketti_price.value,

      this.f.companyid.value,
    ).catch((msg) => {
      this.errors = msg;

      // console.log(this.errors);
    });
  }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FlatpickrOptions } from 'ng2-flatpickr';

import { UserEditService } from 'app/main/apps/user/user-edit/user-edit.service';

import { CoreTranslationService } from '@core/services/translation.service';

import { locale as english } from 'app/main/apps/i18n/en';
import { locale as finnish } from 'app/main/apps/i18n/fi';
import { locale as french } from 'app/main/apps/user/i18n/fr';
import { locale as german } from 'app/main/apps/user/i18n/de';
import { locale as portuguese } from 'app/main/apps/user/i18n/pt';

import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import moment from 'moment';


@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserEditComponent implements OnInit {
  // Public
  public url = this.router.url;
  public urlLastValue;
  public row;
  public c_row;
  public loginForm: FormGroup;
  public pwForm: FormGroup;
  public companyForm: FormGroup;
  public passwordTextTypeOld = false;
  public passwordTextTypeNew = false;
  public passwordTextTypeRetype = false;
  public curruser: any;
  public errormessage;
  public provinces: any;
  public time_to_mute;

  public disabled = false;

  public showbutton = false;
  public showpausetime = false;
  public pausetime = '';
  
  public showdata = false;

  public birthDateOptions: FlatpickrOptions = {
    altInput: true
  };

  public selectMultiLanguages = ['English', 'Spanish', 'French', 'Russian', 'German', 'Arabic', 'Sanskrit'];
  public selectMultiLanguagesSelected = [];

  // Private
  private _unsubscribeAll: Subject<any>;
  private _unsubscribeAlll: Subject<any>;

  /**
   * Constructor
   *
   * @param {Router} router
   * @param {UserEditService} _userEditService
   */
  constructor(private router: Router, private _userEditService: UserEditService,
    private _coreTranslationService: CoreTranslationService,
    private location: Location,
    private _formBuilder: FormBuilder,
    public _httpClient: HttpClient) {
    this._unsubscribeAll = new Subject();
    this._unsubscribeAlll = new Subject();
    this.urlLastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
    this._coreTranslationService.translate(finnish, english, french, german, portuguese);

  }

  get f() {
    return this.loginForm.controls;
  }

  get ff() {
    return this.pwForm.controls;
  }

  get fff() {
    return this.companyForm.controls;
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this.curruser = JSON.parse(localStorage.getItem('currentUser'));
    this._userEditService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.row = response;
      // console.log(this.row);
      // console.log(moment().unix())
      // console.log(moment(this.row.notifications_paused_untill, 'YYYY-M-D H:mm:ss').unix())
      if (moment().unix() < moment(this.row.notifications_paused_untill, 'YYYY-M-D H:mm:ss').unix()) {
        this.row.still_muted = true;
      } else {
        this.row.still_muted = false;
      }
      // console.log(this.row);
    });
    this._userEditService.onDataChangedd.pipe(takeUntil(this._unsubscribeAlll)).subscribe(responsee => {
      this.c_row = responsee;
      // console.log(this.c_row);
    });
    this.loginForm = this._formBuilder.group({
      firstName: [this.row.firstName, Validators.required],
      lastName: [this.row.lastName, Validators.required],
      email: [this.row.email, [Validators.required, Validators.email]],
      mobile: [this.row.mobile, Validators.required],
      role: [this.row.role_id, Validators.required],
    });
    this.pwForm = this._formBuilder.group({
      password: ['', Validators.required],
      new_password: ['', Validators.required],
      confirm_new_password: ['', Validators.required],
    });
    this.companyForm = this._formBuilder.group({
      companyid: [this.row.company_id, Validators.required],
    });
    this.getProvinces();

    if (this.curruser && (this.curruser.id == this.row.id || this.curruser.role_id == 1 || this.curruser.role_id == 2)) {
      this.showdata = true;
    }
  }

  checkForProvince(item) {
    return this.row.user_notifications.filter(e => e.province_id == item.id).length > 0;
  }

  getProvinces() {
    const url = `${environment.apiUrl}/provinces`;
    this._httpClient.get(url).subscribe((response: any) => {
      this.provinces = response.provinces;
    });
  }

  showSaveBtn(val) {
    if (val.target.value == '') {
      this.showbutton = false;
      this.showpausetime = false;
      this.pausetime = '';
      this.time_to_mute = 'unmute';
    } else if (val.target.value == 'permanent') {
      this.time_to_mute = 'permanent';
      this.showbutton = true;
      this.showpausetime = true;
      this.pausetime = 'Ilmoutukset mykistetään toistaiseksi.'
    } else {
      this.showbutton = true;
      this.showpausetime = true;
      const dt = new Date();
      const vall = parseInt(val.target.value);
      dt.setHours(dt.getHours() + vall);
      this.pausetime = 'Ilmoitukset mykistetään ' + dt.toLocaleString() + ' saakka.';
      this.time_to_mute = vall;
    }
  }
  saveNotificationTime(id: number) {
    this.disabled = true;
    this.showpausetime = false;
    this.showbutton = false;
    this._userEditService.updateNotificationSettings(
      id,
      this.time_to_mute
    ).then(() => {
      this._userEditService.getApiData(id);
      this.disabled = false;
    }).catch((msg) => {
      // console.log(msg);
    });
  }
  unmuteNotifications(id: number) {
    this.disabled = true;
    this._userEditService.updateNotificationSettings(
      id,
      'unmute'
    ).then(() => {
      this._userEditService.getApiData(id);
      this.disabled = false;
    }).catch((msg) => {
      // console.log(msg);
    });
  }
  changeNotificationSetting(id: number, val) {
    // console.log(val.target.value, val.target.checked);
    this.disabled = true;
    this._userEditService.changeNotificationSettings(
      id,
      val.target.value,
      val.target.checked
    ).then(() => {
      this._userEditService.getApiData(id);
      this.disabled = false;
    }).catch((msg) => {
      // console.log(msg);
    });
  }
  updateProvinceNotifications(id: number, val) {
    // console.log(val.target.value, val.target.checked);
    this.disabled = true;
    this._userEditService.updateProvinceNotifications(
      id,
      val.target.value,
      val.target.checked
    ).then(() => {
      this._userEditService.getApiData(id);
      this.disabled = false;
    }).catch((msg) => {
      // console.log(msg);
    });
  }
  toggleAllOn(id: number) {
    this.disabled = true;
    this._userEditService.massUpdateProvinceNotifications(
      id,
      'add'
    ).then(() => {
      let checkboxes = document.getElementsByClassName('province-cb');
      for (var i = 0, n = checkboxes.length; i < n; i++) {
        const cb = checkboxes[i] as HTMLInputElement;
        cb.checked = true;
      }
      this._userEditService.getApiData(id);
      this.disabled = false;
    }).catch((msg) => {
      // console.log(msg);
    });
  }
  toggleAllOff(id: number) {
    this.disabled = true;
    this._userEditService.massUpdateProvinceNotifications(
      id,
      'remove'
    ).then(() => {
      let checkboxes = document.getElementsByClassName('province-cb');
      for (var i = 0, n = checkboxes.length; i < n; i++) {
        const cb = checkboxes[i] as HTMLInputElement;
        cb.checked = false;
      }
      this._userEditService.getApiData(id);
      this.disabled = false;
    }).catch((msg) => {
      // console.log(msg);
    });
  }

  goBack() {
    this.location.back();
    // console.log('goBack()...');
  }

  submit(id: number) {
    // console.log(this.f);
    this._userEditService.updateUser(
      id,
      this.f.firstName.value,
      this.f.lastName.value,
      this.f.email.value,
      this.f.mobile.value,
      this.f.role.value,
    );
  }

  submitPwForm(id: number) {
    // console.log(this.ff);
    this.errormessage = '';
    this._userEditService.updatePw(
      id,
      this.ff.password.value,
      this.ff.new_password.value,
      this.ff.confirm_new_password.value,
    ).catch((msg) => {
      // console.log(msg);
      this.errormessage = msg;
    });
  }

  submitCompanyForm(id: number) {
    // console.log(this.fff);
    this._userEditService.updateRelation(
      id,
      this.fff.companyid.value,
    );
  }

  /**
   * Toggle Password Text Type Old
   */
  togglePasswordTextTypeOld() {
    this.passwordTextTypeOld = !this.passwordTextTypeOld;
  }

  /**
   * Toggle Password Text Type New
   */
  togglePasswordTextTypeNew() {
    this.passwordTextTypeNew = !this.passwordTextTypeNew;
  }

  /**
   * Toggle Password Text Type Retype
   */
  togglePasswordTextTypeRetype() {
    this.passwordTextTypeRetype = !this.passwordTextTypeRetype;
  }
}

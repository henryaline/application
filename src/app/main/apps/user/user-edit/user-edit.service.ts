import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class UserEditService implements Resolve<any> {
  public apiData: any;
  public apiDataa: any;
  public onDataChanged: BehaviorSubject<any>;
  public onDataChangedd: BehaviorSubject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _toastrService: ToastrService, private _router: Router) {
    // Set the defaults
    this.onDataChanged = new BehaviorSubject({});
    this.onDataChangedd = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    let currentId = Number(route.paramMap.get('id'));
    return new Promise<void>((resolve, reject) => {
      // Promise.all([this.getApiData()]).then(() => {
      Promise.all([this.getApiData(currentId), this.getApiDataa()]).then(() => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get API Data
   */
  getApiData(id: number): Promise<any[]> {
    // const url = `${environment.apiUrl}/users`;
    const url = `${environment.apiUrl}/users/get/${id}`;

    return new Promise((resolve, reject) => {
      // this._httpClient.get('api/users-data').subscribe((response: any) => {
      this._httpClient.get(url).subscribe((response: any) => {
        // this.apiData = response;
        this.apiData = response.users.find(i => i.id == id);
        this.onDataChanged.next(this.apiData);
        resolve(this.apiData);
      }, reject);
    });
  }
  getApiDataa(): Promise<any[]> {
    // const c_url = `${environment.apiUrl}/companies`;
    const c_url = `${environment.apiUrl}/companies-for-relation`;
    return new Promise((resolve, reject) => {
      this._httpClient.get(c_url).subscribe((responsee: any) => {
        this.apiDataa = responsee.companies;
        this.onDataChangedd.next(this.apiDataa);
        resolve(this.apiDataa);
      }, reject);
    });
  }

  updateUser(id: number, firstname: string, lastname: string, email: string, mobile: string, role:number): Promise<any[]> {
    const url = `${environment.apiUrl}/users/edit`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id, firstname, lastname, email, mobile, role }).subscribe((response: any) => {
        this._router.navigate(['/apps/user/user-view/' + id]);
        setTimeout(() => {
          this._toastrService.success(
            'Käyttäjän tiedot päivitetty onnistuneesti.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }

  updatePw(id: number, password: string, new_password: string, confirm_new_password: string): Promise<any[]> {
    const url = `${environment.apiUrl}/users/change-password`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id, password, new_password, confirm_new_password }).subscribe((response: any) => {
        this._router.navigate(['/apps/user/user-view/' + id]);
        setTimeout(() => {
          this._toastrService.success(
            'Salasana vaihdettu onnistuneesti.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }

  updateRelation(id: number, companyid: number): Promise<any[]> {
    const url = `${environment.apiUrl}/users/edit-relation`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id, companyid }).subscribe((response: any) => {
        this._router.navigate(['/apps/user/user-view/' + id]);
        setTimeout(() => {
          this._toastrService.success(
            'Liitos yritykseen päivitetty onnistuneesti.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }

  updateNotificationSettings(id:number, time_to_mute): Promise<any[]> {
    const url = `${environment.apiUrl}/users/update-notification-settings`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id, time_to_mute }).subscribe((response: any) => {
        // this._router.navigate(['/apps/user/user-view/' + id]);
        setTimeout(() => {
          this._toastrService.success(
            'Ilmoitusasetukset päivitetty.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }
  changeNotificationSettings(id: number, changevalue, newvalue): Promise<any[]> {
    const url = `${environment.apiUrl}/users/change-notification-settings`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id, changevalue, newvalue }).subscribe((response: any) => {
        // this._router.navigate(['/apps/user/user-view/' + id]);
        setTimeout(() => {
          this._toastrService.success(
            'Ilmoitusasetukset päivitetty.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }
  updateProvinceNotifications(id: number, changevalue, newvalue): Promise<any[]> {
    const url = `${environment.apiUrl}/users/update-province-notifications`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id, changevalue, newvalue }).subscribe((response: any) => {
        // this._router.navigate(['/apps/user/user-view/' + id]);
        setTimeout(() => {
          this._toastrService.success(
            'Ilmoitusasetukset päivitetty.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }
  massUpdateProvinceNotifications(id: number, assignment): Promise<any[]> {
    const url = `${environment.apiUrl}/users/mass-update-province-notifications`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { id, assignment }).subscribe((response: any) => {
        // this._router.navigate(['/apps/user/user-view/' + id]);
        setTimeout(() => {
          this._toastrService.success(
            'Ilmoitusasetukset päivitetty.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }
}

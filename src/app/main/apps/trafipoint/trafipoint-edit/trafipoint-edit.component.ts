import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FlatpickrOptions } from 'ng2-flatpickr';

import { TrafipointEditService } from 'app/main/apps/trafipoint/trafipoint-edit/trafipoint-edit.service';

import { CoreTranslationService } from '@core/services/translation.service';

import { locale as english } from 'app/main/apps/trafipoint/i18n/en';
import { locale as finnish } from 'app/main/apps/trafipoint/i18n/fi';
import { locale as french } from 'app/main/apps/trafipoint/i18n/fr';
import { locale as german } from 'app/main/apps/trafipoint/i18n/de';
import { locale as portuguese } from 'app/main/apps/trafipoint/i18n/pt';

import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-trafipoint-edit',
  templateUrl: './trafipoint-edit.component.html',
  styleUrls: ['./trafipoint-edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TrafipointEditComponent implements OnInit {
  // Public
  public url = this.router.url;
  // public urlLastValue;
  public row;
  public c_row;
  public loginForm: FormGroup;
  public curruser: any;
  public numRegex = /^-?\d*[.]?\d{0,2}$/;
  public errors;
  public isFree;


  public birthDateOptions: FlatpickrOptions = {
    altInput: true
  };

  public selectMultiLanguages = ['English', 'Spanish', 'French', 'Russian', 'German', 'Arabic', 'Sanskrit'];
  public selectMultiLanguagesSelected = [];

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {Router} router
   * @param {TrafipointEditService} _tpEditService
   */
  constructor(
    private router: Router,
    private _tpEditService: TrafipointEditService,
    private _coreTranslationService: CoreTranslationService,
    private location: Location,
    private _formBuilder: FormBuilder
  ) {
    this._unsubscribeAll = new Subject();
    // this.urlLastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
    this._coreTranslationService.translate(finnish, english, french, german, portuguese);

  }

  get f() {
    return this.loginForm.controls;
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this.getData();
    this.loginForm = this._formBuilder.group({
      monthly_subscription_price: [this.row.monthly_subscription_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_muutto_price: [this.row.event_muutto_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_rahti_price: [this.row.event_rahti_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_ajoneuvot_price: [this.row.event_ajoneuvot_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_massa_price: [this.row.event_massa_price, [Validators.required, Validators.pattern(this.numRegex)]],
      event_paketti_price: [this.row.event_paketti_price, [Validators.required, Validators.pattern(this.numRegex)]],
      max_opens: [this.row.max_opens, Validators.required]
    });
  }

  getData() {
    this._tpEditService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.row = response.prices;
      this.curruser = response.user;
      this.isFree = this.row.subscription_free_first_month;
      // console.log(this.row);
      // console.log(this.curruser);
      // console.log(this.isFree);
    });
  }

  changeval(val) {
    this.isFree = val.target.checked;
    // console.log(this.isFree);
  }

  goBack() {
    this.location.back();
    // console.log('goBack()...');
  }

  submit() {
    // console.log(this.f);
    // console.log(this.isFree);
    this._tpEditService.updatePrices(
      this.f.monthly_subscription_price.value,
      this.f.event_muutto_price.value,
      this.f.event_rahti_price.value,
      this.f.event_ajoneuvot_price.value,
      this.f.event_massa_price.value,
      this.f.event_paketti_price.value, 
      this.f.max_opens.value,
      this.isFree,
    ).then(() => {
      this.getData();
    }).catch((e) => {
      this.errors = e;
    });
  }
}

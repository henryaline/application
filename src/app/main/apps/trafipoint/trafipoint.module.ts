import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';

import { CoreCommonModule } from '@core/common.module';
import { CoreDirectivesModule } from '@core/directives/directives';
import { CorePipesModule } from '@core/pipes/pipes.module';
import { CoreSidebarModule } from '@core/components';

import { InvoiceListService } from 'app/main/apps/invoice/invoice-list/invoice-list.service';
import { InvoiceModule } from 'app/main/apps/invoice/invoice.module';

import { TrafipointEditComponent } from 'app/main/apps/trafipoint/trafipoint-edit/trafipoint-edit.component';
import { TrafipointEditService } from 'app/main/apps/trafipoint/trafipoint-edit/trafipoint-edit.service';

import { TrafipointInfoComponent } from 'app/main/apps/trafipoint/trafipoint-info/trafipoint-info.component';
import { TrafipointInfoService } from 'app/main/apps/trafipoint/trafipoint-info/trafipoint-info.service';


import { TranslateModule } from '@ngx-translate/core';



// routing
const routes: Routes = [
  {
    path: 'trafipoint-edit',
    component: TrafipointEditComponent,
    resolve: {
      ues: TrafipointEditService
    }
  },
  {
    path: 'trafipoint-info',
    component: TrafipointInfoComponent,
    resolve: {
      ues: TrafipointInfoService
    }
  },
];

@NgModule({
  declarations: [TrafipointEditComponent, TrafipointInfoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreCommonModule,
    FormsModule,
    NgbModule,
    NgSelectModule,
    Ng2FlatpickrModule,
    NgxDatatableModule,
    CorePipesModule,
    CoreDirectivesModule,
    InvoiceModule,
    CoreSidebarModule,
    TranslateModule
  ],
  providers: [TrafipointEditService, TrafipointInfoService]
})
export class TrafipointModule {
}

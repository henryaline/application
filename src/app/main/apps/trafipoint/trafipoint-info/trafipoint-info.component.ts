import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FlatpickrOptions } from 'ng2-flatpickr';

import { TrafipointInfoService } from 'app/main/apps/trafipoint/trafipoint-info/trafipoint-info.service';

import { CoreTranslationService } from '@core/services/translation.service';

import { locale as english } from 'app/main/apps/trafipoint/i18n/en';
import { locale as finnish } from 'app/main/apps/trafipoint/i18n/fi';
import { locale as french } from 'app/main/apps/trafipoint/i18n/fr';
import { locale as german } from 'app/main/apps/trafipoint/i18n/de';
import { locale as portuguese } from 'app/main/apps/trafipoint/i18n/pt';

import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-trafipoint-info',
  templateUrl: './trafipoint-info.component.html',
  styleUrls: ['./trafipoint-info.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TrafipointInfoComponent implements OnInit {
  // Public
  public url = this.router.url;
  // public urlLastValue;
  public row;
  public c_row;
  public loginForm: FormGroup;
  public curruser: any;

  public birthDateOptions: FlatpickrOptions = {
    altInput: true
  };

  public selectMultiLanguages = ['English', 'Spanish', 'French', 'Russian', 'German', 'Arabic', 'Sanskrit'];
  public selectMultiLanguagesSelected = [];

  // Private


  /**
   * Constructor
   *
   * @param {Router} router
   * @param {TrafipointInfoService} _tpEditService
   */
  constructor(
    private router: Router,
    private _tpEditService: TrafipointInfoService,
    private _coreTranslationService: CoreTranslationService,
    private location: Location,
    private _formBuilder: FormBuilder
  ) {
    // this.urlLastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
    this._coreTranslationService.translate(finnish, english, french, german, portuguese);

  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
  }

  getData() {
  }

  goBack() {
    this.location.back();
    // console.log('goBack()...');
  }
}

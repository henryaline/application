import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class TrafipointInfoService implements Resolve<any> {
  public apiData: any;
  public apiDataa: any;
  public onDataChanged: BehaviorSubject<any>;
  public onDataChangedd: BehaviorSubject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _toastrService: ToastrService, private _router: Router) {
    // Set the defaults
    this.onDataChanged = new BehaviorSubject({});
    this.onDataChangedd = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise<void>((resolve, reject) => {
      // Promise.all([this.getApiData()]).then(() => {
      Promise.all([]).then(() => {
        resolve();
      }, reject);
    });
  }

  // /**
  //  * Get API Data
  //  */
  // getApiData(): Promise<any[]> {
  //   const url = `${environment.apiUrl}/pricing`;
  //   return new Promise((resolve, reject) => {
  //     this._httpClient.get(url).subscribe((response: any) => {
  //       this.apiData = response;
  //       this.onDataChanged.next(this.apiData);
  //       resolve(this.apiData);
  //     }, reject);
  //   });
  // }

  // updatePrices(
  //   monthly_subscription_price: string,
  //   event_muutto_price: string,
  //   event_rahti_price: string,
  //   event_ajoneuvot_price: string,
  //   event_massa_price: string
  // ): Promise<any[]> {
  //   const url = `${environment.apiUrl}/edit-pricing`;
  //   return new Promise((resolve, reject) => {
  //     this._httpClient.post<any>(url, { monthly_subscription_price, event_muutto_price, event_rahti_price, event_ajoneuvot_price, event_massa_price }).subscribe((response: any) => {
  //       setTimeout(() => {
  //         this._toastrService.success(
  //           'Hinnasto päivitetty.',
  //           '',
  //           { toastClass: 'toast ngx-toastr', closeButton: true }
  //         );
  //       }, 1500);
  //       // console.log(response);
  //       resolve(response.message);
  //     }, reject);
  //   });
  // }
}

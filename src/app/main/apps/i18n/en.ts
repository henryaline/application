export const locale = {
  lang: 'en',
  data: {
    SAMPLE: {
      CONGRATULATIONS: 'Congratulations',
      BADGE: 'You have won gold medal'
    },
    USEREDIT: {
      USER: 'User',
      CHANGEPW: 'Change password',
      NOTIFICATIONS: 'Notifications',
      FIRSTNAME: 'Firstname',
      LASTNAME: 'Lastname',
      EMAIL: 'Email',
      MOBILE: 'Mobile',
      SAVEC: 'Save changes',
      BACK: 'Back',
      OLDPW: 'Old password',
      NEWPW: 'New password',
      NEWPWAGN: 'New password again',
      SAVE: 'Save',
      REVERT: 'Cancel',
      ATTACHEDCMPN: 'Attached company',
      SELECTNOTIFCS: 'Select prefered push-notifications',
      NEWTNDR: 'New tender in the app',
      MOVE: 'Move',
      CARGO: 'Cargo',
      VEHICLE: 'Vehicle/boat/machine',
      SUPPLY: 'Soil/supply',
      PACKAGE: 'Packages',
      ROLE: 'Rooli'
    },
    USERVIEW: {
      EDIT: 'Edit',
      NAME: 'Name',
      CONTACT: 'Contact',
      ATTACHEDCMPN: 'Attached company',
      CMPDETAILS: 'Company details',
      ROLE: 'Role',
      DEACT: 'Deactivate',
      ACT: 'Activate',
      REMOVEFG: 'Remove',
      NOATC: 'No attached company',
      ATTACHCMP: 'Attach to company',
    },
  }
};

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';

import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class TenderListService implements Resolve<any> {
  rows: any;
  rowss: any;
  rowsss: any;
  onDatatablessChanged: BehaviorSubject<any>;
  onDatatablessChangedd: BehaviorSubject<any>;
  onDatatablessChangeddd: BehaviorSubject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {
    // Set the defaults
    this.onDatatablessChanged = new BehaviorSubject({});
    this.onDatatablessChangedd = new BehaviorSubject({});
    this.onDatatablessChangeddd = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise<void>((resolve, reject) => {
      // Promise.all([this.getDataTableRows(), this.getDataTableRowsResponded(), this.getDataTableRowsAccepted()]).then(() => {
      Promise.all([]).then(() => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get rows
   */
  getDataTableRows(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      // this._httpClient.get('http://localhost:4200/api/invoice-data').subscribe((response: any) => {
      this._httpClient.get(`${environment.apiUrl}/tenders/all`).subscribe((response: any) => {
        this.rows = response;
        this.onDatatablessChanged.next(this.rows);
        resolve(this.rows);
      }, reject);
    });
  }
  getDataTableRowsResponded(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this._httpClient.get(`${environment.apiUrl}/tenders/responded`).subscribe((responsee: any) => {
        this.rowss = responsee;
        this.onDatatablessChangedd.next(this.rowss);
        resolve(this.rowss);
      }, reject);
    });
  }
  getDataTableRowsAccepted(): Promise<any[]> { // Ei toiminnallisuutta vielä
    return new Promise((resolve, reject) => {
      this._httpClient.get(`${environment.apiUrl}/tenders/accepted`).subscribe((responseee: any) => {
        this.rowsss = responseee;
        this.onDatatablessChangeddd.next(this.rowsss);
        resolve(this.rowsss);
      }, reject);
    });
  }

  newEvent(tender_id: string) {
    return this._httpClient
      // .post<any>(`${environment.apiUrl}/users/authenticate`, { email, password })
      .post<any>(`${environment.apiUrl}/events/check`, { tender_id })
      .pipe(
        map(response => {
          return response;
        })
      );
  }
}

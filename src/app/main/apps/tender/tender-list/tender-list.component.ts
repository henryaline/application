import { Component, Injectable, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';

import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import * as moment from 'moment';
import * as countdown from 'countdown';
import * as momentcountdown from 'moment-countdown';
import { TenderListService } from 'app/main/apps/tender/tender-list/tender-list.service';
import { FlatpickrOptions } from 'ng2-flatpickr';
import { NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';

/**
* This Service handles how the date is represented in scripts i.e. ngModel.
*/
@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {

  readonly DELIMITER = '-';

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day: parseInt(date[0], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[2], 10)
      };
    }
    return null;
  }

  toModel(date: NgbDateStruct | null): string | null {
    return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null;
  }
}
/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day: parseInt(date[0], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : '';
  }
}
@Component({
  selector: 'app-tender-list',
  templateUrl: './tender-list.component.html',
  styleUrls: ['./tender-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: NgbDateAdapter, useClass: CustomAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter }
  ]
})

export class TenderListComponent implements OnInit {
  // public
  public data: any;
  public selectedOption = 10;
  public selectedOption1 = 10;
  public selectedOption2 = 10;
  public selectedOption3 = 10;
  public selectedOption4 = 10;
  public selectedOption5 = 10;
  public selectedStatusOption = '';
  public ColumnMode = ColumnMode;

  public dateTimeOptions: FlatpickrOptions = {
    altInput: true,
    enableTime: true
  };

  // decorator
  @ViewChild(DatatableComponent) table: DatatableComponent;

  // private
  private tempData = [];
  private tempDataMuutot = [];
  private tempDataRahdit = [];
  private tempDataAjoneuvot = [];
  private tempDataMassat = [];
  private tempDataPaketit = [];
  private _unsubscribeAll: Subject<any>;
  private _unsubscribeAlll: Subject<any>;
  private _unsubscribeAllll: Subject<any>;
  public rows;
  public usr;

  public muutot;
  public rahdit;
  public ajoneuvot;
  public massat;
  public paketit;

  basicDPdata: string;
  basicDPdata2: string;
  basicDPdata3: string;
  basicDPdata4: string;
  basicDPdata5: string;

  public kaikkiRivit = 1;
  public vastatutRivit = 0;

  public clicked = false;
  public selectedvalue = "Muutot";

  public loading = true;

  /**
   * Constructor
   *
   * @param {TenderListService} _tenderListService
   */
  constructor(
    private _tenderListService: TenderListService,
    private ngbCalendar: NgbCalendar,
    private dateAdapter: NgbDateAdapter<string>,
    public router: Router,
    private _httpClient: HttpClient
    ) {
    this._unsubscribeAll = new Subject();
    this._unsubscribeAlll = new Subject();
    this._unsubscribeAllll = new Subject();
  }

  // Public Methods
  // -----------------------------------------------------------------------------------------------------

  // Tarjouspyyntöjen filtteröintitapahtumat
  // Ensimmäinen parametri on klikkauksen/syötön kohde ja antaa arvon
  // Toinen parametri on tyyppi, eli minkä luokan tarjouspyyntöjä suodatetaan
  // 1=Muutot, 2=Rahdit, 3=Ajoneuvot, 4=Massa, 5=Paketit
  // 
  // Järjestä valinnan mukaan

  sortBySth(event, type) {
    if (type == 1) {
      const val = event.target.value.toLowerCase();
      if (val == 1) {
        const tempMuutot = this.tempDataMuutot.sort(function (a, b) {
          return (b.isOpened === a.isOpened) ? 0 : b.isOpened ? -1 : 1;
        });
        this.muutot = tempMuutot;
      } else if (val == 2) {
        const tempMuutot = this.tempDataMuutot.sort(function (a, b) {
          if (a.time < b.time) {
            return -1;
          }
          if (a.time > b.time) {
            return 1;
          }
          return 0;
        });
        this.muutot = tempMuutot;
      } else if (val == 3) {
        const tempMuutot = this.tempDataMuutot.sort(function (a, b) {
          if (a.created_time < b.created_time) {
            return -1;
          }
          if (a.created_time > b.created_time) {
            return 1;
          }
          return 0;
        });
        this.muutot = tempMuutot;
      } else if (val == 4) {
        const tempMuutot = this.tempDataMuutot.sort(function (a, b) {
          if (a.subtype_from < b.subtype_from) {
            return -1;
          }
          if (a.subtype_from > b.subtype_from) {
            return 1;
          }
          return 0;
        });
        this.muutot = tempMuutot;
      } else {
        const tempMuutot = this.tempDataMuutot.sort(function (a, b) {
          if (a.id < b.id) {
            return -1;
          }
          if (a.id > b.id) {
            return 1;
          }
          return 0;
        });
        this.muutot = tempMuutot;
      }
    } else if (type == 2) {
      const val = event.target.value.toLowerCase();
      if (val == 1) {
        const tempRahdit = this.tempDataRahdit.sort(function (a, b) {
          return (b.isOpened === a.isOpened) ? 0 : b.isOpened ? -1 : 1;
        });
        this.rahdit = tempRahdit;
      } else if (val == 2) {
        const tempRahdit = this.tempDataRahdit.sort(function (a, b) {
          if (a.time < b.time) {
            return -1;
          }
          if (a.time > b.time) {
            return 1;
          }
          return 0;
        });
        this.rahdit = tempRahdit;
      } else if (val == 3) {
        const tempRahdit = this.tempDataRahdit.sort(function (a, b) {
          if (a.created_time < b.created_time) {
            return -1;
          }
          if (a.created_time > b.created_time) {
            return 1;
          }
          return 0;
        });
        this.rahdit = tempRahdit;
      } else if (val == 4) {
        const tempRahdit = this.tempDataRahdit.sort(function (a, b) {
          if (a.subtype_from < b.subtype_from) {
            return -1;
          }
          if (a.subtype_from > b.subtype_from) {
            return 1;
          }
          return 0;
        });
        this.rahdit = tempRahdit;
      } else {
        const tempRahdit = this.tempDataRahdit.sort(function (a, b) {
          if (a.id < b.id) {
            return -1;
          }
          if (a.id > b.id) {
            return 1;
          }
          return 0;
        });
        this.rahdit = tempRahdit;
      }
    } else if (type == 3) {
      const val = event.target.value.toLowerCase();
      if (val == 1) {
        const tempAjoneuvot = this.tempDataAjoneuvot.sort(function (a, b) {
          return (b.isOpened === a.isOpened) ? 0 : b.isOpened ? -1 : 1;
        });
        this.ajoneuvot = tempAjoneuvot;
      } else if (val == 2) {
        const tempAjoneuvot = this.tempDataAjoneuvot.sort(function (a, b) {
          if (a.time < b.time) {
            return -1;
          }
          if (a.time > b.time) {
            return 1;
          }
          return 0;
        });
        this.ajoneuvot = tempAjoneuvot;
      } else if (val == 3) {
        const tempAjoneuvot = this.tempDataAjoneuvot.sort(function (a, b) {
          if (a.created_time < b.created_time) {
            return -1;
          }
          if (a.created_time > b.created_time) {
            return 1;
          }
          return 0;
        });
        this.ajoneuvot = tempAjoneuvot;
      } else if (val == 4) {
        const tempAjoneuvot = this.tempDataAjoneuvot.sort(function (a, b) {
          if (a.subtype_from < b.subtype_from) {
            return -1;
          }
          if (a.subtype_from > b.subtype_from) {
            return 1;
          }
          return 0;
        });
        this.ajoneuvot = tempAjoneuvot;
      } else {
        const tempAjoneuvot = this.tempDataAjoneuvot.sort(function (a, b) {
          if (a.id < b.id) {
            return -1;
          }
          if (a.id > b.id) {
            return 1;
          }
          return 0;
        });
        this.ajoneuvot = tempAjoneuvot;
      }
    } else if (type == 4) {
      const val = event.target.value.toLowerCase();
      if (val == 1) {
        const tempMassat = this.tempDataMassat.sort(function (a, b) {
          return (b.isOpened === a.isOpened) ? 0 : b.isOpened ? -1 : 1;
        });
        this.massat = tempMassat;
      } else if (val == 2) {
        const tempMassat = this.tempDataMassat.sort(function (a, b) {
          if (a.time < b.time) {
            return -1;
          }
          if (a.time > b.time) {
            return 1;
          }
          return 0;
        });
        this.massat = tempMassat;
      } else if (val == 3) {
        const tempMassat = this.tempDataMassat.sort(function (a, b) {
          if (a.created_time < b.created_time) {
            return -1;
          }
          if (a.created_time > b.created_time) {
            return 1;
          }
          return 0;
        });
        this.massat = tempMassat;
      } else if (val == 4) {
        const tempMassat = this.tempDataMassat.sort(function (a, b) {
          if (a.subtype_from < b.subtype_from) {
            return -1;
          }
          if (a.subtype_from > b.subtype_from) {
            return 1;
          }
          return 0;
        });
        this.massat = tempMassat;
      } else {
        const tempMassat = this.tempDataMassat.sort(function (a, b) {
          if (a.id < b.id) {
            return -1;
          }
          if (a.id > b.id) {
            return 1;
          }
          return 0;
        });
        this.massat = tempMassat;
      }
    } else if (type == 5) {
      const val = event.target.value.toLowerCase();
      if (val == 1) {
        const tempPaketit = this.tempDataPaketit.sort(function (a, b) {
          return (b.isOpened === a.isOpened) ? 0 : b.isOpened ? -1 : 1;
        });
        this.paketit = tempPaketit;
      } else if (val == 2) {
        const tempPaketit = this.tempDataPaketit.sort(function (a, b) {
          if (a.time < b.time) {
            return -1;
          }
          if (a.time > b.time) {
            return 1;
          }
          return 0;
        });
        this.paketit = tempPaketit;
      } else if (val == 3) {
        const tempPaketit = this.tempDataPaketit.sort(function (a, b) {
          if (a.created_time < b.created_time) {
            return -1;
          }
          if (a.created_time > b.created_time) {
            return 1;
          }
          return 0;
        });
        this.paketit = tempPaketit;
      } else if (val == 4) {
        const tempPaketit = this.tempDataPaketit.sort(function (a, b) {
          if (a.subtype_from < b.subtype_from) {
            return -1;
          }
          if (a.subtype_from > b.subtype_from) {
            return 1;
          }
          return 0;
        });
        this.paketit = tempPaketit;
      } else {
        const tempPaketit = this.tempDataPaketit.sort(function (a, b) {
          if (a.id < b.id) {
            return -1;
          }
          if (a.id > b.id) {
            return 1;
          }
          return 0;
        });
        this.paketit = tempPaketit;
      }
    } else {
      // -
    }
  }
  // Filtteröi syöttämän tekstin mukaan (kaupunki, tyyppi)
  filterUpdate(event, type) {
    if (type == 1) {
      const val = event.target.value.toLowerCase();
      const tempMuutot = this.tempDataMuutot.filter(function (d) {
        return d.where_from_city.toLowerCase().indexOf(val) !== -1 ||
          d.where_to_city.toLowerCase().indexOf(val) !== -1 ||
          d.subtype_from.toLowerCase().indexOf(val) !== -1 || !val;
      })
      this.muutot = tempMuutot;
    } else if (type == 2) {
      const val = event.target.value.toLowerCase();
      const tempRahdit = this.tempDataRahdit.filter(function (d) {
        return d.where_from_city.toLowerCase().indexOf(val) !== -1 ||
          d.where_to_city.toLowerCase().indexOf(val) !== -1 ||
          d.subtype_from.toLowerCase().indexOf(val) !== -1 || !val;
      })
      this.rahdit = tempRahdit;
    } else if (type == 3) {
      const val = event.target.value.toLowerCase();
      const tempAjoneuvot = this.tempDataAjoneuvot.filter(function (d) {
        return d.where_from_city.toLowerCase().indexOf(val) !== -1 ||
          d.where_to_city.toLowerCase().indexOf(val) !== -1 ||
          d.subtype_from.toLowerCase().indexOf(val) !== -1 || !val;
      })
      this.ajoneuvot = tempAjoneuvot;
    } else if (type == 4) {
      const val = event.target.value.toLowerCase();
      const tempMassat = this.tempDataMassat.filter(function (d) {
        return d.where_from_city.toLowerCase().indexOf(val) !== -1 ||
          d.where_to_city.toLowerCase().indexOf(val) !== -1 ||
          d.subtype_from.toLowerCase().indexOf(val) !== -1 || !val;
      })
      this.massat = tempMassat;
    } else if (type == 5) {
      const val = event.target.value.toLowerCase();
      const tempPaketit = this.tempDataPaketit.filter(function (d) {
        return d.where_from_city.toLowerCase().indexOf(val) !== -1 ||
          d.where_to_city.toLowerCase().indexOf(val) !== -1 ||
          d.subtype_from.toLowerCase().indexOf(val) !== -1 || !val;
      })
      this.paketit = tempPaketit;
    } else {
      // -
    }
  }
  // Filtteröi pvm valinnan mukaan (jätetty)
  dateFilterUpdate(evnt, type) {
    if (type == 1) {
      const val = this.basicDPdata;
      const tempMuutot = this.tempDataMuutot.filter(function (d) {
        return d.createddi.indexOf(val) !== -1 || !val;
      });
      this.muutot = tempMuutot;
    } else if (type == 2) {
      const val = this.basicDPdata2;
      const tempRahdit = this.tempDataRahdit.filter(function (d) {
        return d.createddi.indexOf(val) !== -1 || !val;
      });
      this.rahdit = tempRahdit;
    } else if (type == 3) {
      const val = this.basicDPdata3;
      const tempAjoneuvot = this.tempDataAjoneuvot.filter(function (d) {
        return d.createddi.indexOf(val) !== -1 || !val;
      });
      this.ajoneuvot = tempAjoneuvot;
    } else if (type == 4) {
      const val = this.basicDPdata4;
      const tempMassat = this.tempDataMassat.filter(function (d) {
        return d.createddi.indexOf(val) !== -1 || !val;
      });
      this.massat = tempMassat;
    } else if (type == 5) {
      const val = this.basicDPdata5;
      const tempPaketit = this.tempDataPaketit.filter(function (d) {
        return d.createddi.indexOf(val) !== -1 || !val;
      });
      this.paketit = tempPaketit;
    } else {
      // -
    }
  }
  // Filtteröi kaupunki syötön mukaan
  cityFilterUpdate(event, type) {
    if (type == 1) {
      const val = event.target.value.toLowerCase();
      const tempMuutot = this.tempDataMuutot.filter(function (d) {
        return d.where_from_city.toLowerCase().indexOf(val) !== -1 || d.where_to_city.toLowerCase().indexOf(val) !== -1 || !val;
      });
      this.muutot = tempMuutot;
    } else if (type == 2) {
      const val = event.target.value.toLowerCase();
      const tempRahdit = this.tempDataRahdit.filter(function (d) {
        return d.where_from_city.toLowerCase().indexOf(val) !== -1 || d.where_to_city.toLowerCase().indexOf(val) !== -1 || !val;
      });
      this.rahdit = tempRahdit;
    } else if (type == 3) {
      const val = event.target.value.toLowerCase();
      const tempAjon = this.tempDataAjoneuvot.filter(function (d) {
        return d.where_from_city.toLowerCase().indexOf(val) !== -1 || d.where_to_city.toLowerCase().indexOf(val) !== -1 || !val;
      });
      this.ajoneuvot = tempAjon;
    } else if (type == 4) {
      const val = event.target.value.toLowerCase();
      const tempMassat = this.tempDataMassat.filter(function (d) {
        return d.where_from_city.toLowerCase().indexOf(val) !== -1 || d.where_to_city.toLowerCase().indexOf(val) !== -1 || !val;
      });
      this.massat = tempMassat;
    } else if (type == 5) {
      const val = event.target.value.toLowerCase();
      const tempPaketit = this.tempDataPaketit.filter(function (d) {
        return d.where_from_city.toLowerCase().indexOf(val) !== -1 || d.where_to_city.toLowerCase().indexOf(val) !== -1 || !val;
      });
      this.paketit = tempPaketit;
    } else {
      // -
    }
  }
  
  // Mobiilissa: avaa suodatusvalinnat; pois käytöstä.
  openFilters() {
    const slides = document.getElementsByClassName('filtersarea');
    
    if (!this.clicked) {
      this.clicked = true;
      for (let i = 0; i < slides.length; i++) {
        const slide = slides[i] as HTMLElement;
        slide.style.display = "flex";
      }
    } else if (this.clicked) {
      this.clicked = false;
      for (let i = 0; i < slides.length; i++) {
        const slide = slides[i] as HTMLElement;
        slide.style.display = "none";
      }
    }

    

  }
  // Mobiilissa: vaihtaa kilpailutusten tyypin
  changeTo(val) {
    if (val == 1) {
      this.selectedvalue = 'Muutot';
      const asd = document.getElementsByClassName('navilinkki');
      const asdasd = asd[0] as HTMLElement;
      asdasd.click();
      // const mto: HTMLElement = document.getElementById('muutotpainike') as HTMLElement;
      // mto.click();
    }
    if (val == 2) {
      this.selectedvalue = 'Rahdit';
      const asdd = document.getElementsByClassName('navilinkki');
      const asdasdd = asdd[1] as HTMLElement;
      asdasdd.click();
      // const rti: HTMLElement = document.getElementById('rahditpainike') as HTMLElement;
      // rti.click();
    }
    if (val == 3) {
      this.selectedvalue = 'Ajoneuvot ja veneet';
      const asddd = document.getElementsByClassName('navilinkki');
      const asdasddd = asddd[2] as HTMLElement;
      asdasddd.click();
      // const jnv: HTMLElement = document.getElementById('ajoneuvotpainike') as HTMLElement;
      // jnv.click();
    }
    if (val == 4) {
      this.selectedvalue = 'Puu ja maa-aines';
      const asdddd = document.getElementsByClassName('navilinkki');
      const asdasdddd = asdddd[3] as HTMLElement;
      asdasdddd.click();
      // const ns: HTMLElement = document.getElementById('ainespainike') as HTMLElement;
      // ns.click();
    }
    if (val == 5) {
      this.selectedvalue = 'Pientavara tai paketit';
      const asdddd = document.getElementsByClassName('navilinkki');
      const asdasdddd = asdddd[4] as HTMLElement;
      asdasdddd.click();
      // const ns: HTMLElement = document.getElementById('ainespainike') as HTMLElement;
      // ns.click();
    }
  }

  // Lähetä palvelimelle tieto avatusta kilpailutuksesta
  newEvent(tender_id) {
    this._tenderListService
      // .login(this.f.email.value, this.f.password.value)
      .newEvent(tender_id)
      .pipe(first())
      .subscribe(
        data => {
          // this._router.navigate([this.returnUrl]);
        },
        error => {
          // this.error = error;
          // this.loading = false;
          // console.log(error);
        }
      );
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this.loading = true;
    if (this.router.url.indexOf('/tenders/all') > -1) {
      // this.kaikkiRivit = 0;
      this.vastatutRivit = 0;
      this.getAllTenders();
    }
    if (this.router.url.indexOf('/tenders/responsed') > -1) {
      this.kaikkiRivit = 0;
      this.vastatutRivit = 1;
      this.getRespondedTenders();
    }
    if (this.router.url.indexOf('/tenders/company-listed') > -1) {
      this.kaikkiRivit = 0;
      this.vastatutRivit = 0;
      this.getAcceptedTenders();
    }
  }


  getAllTenders() {
    this._httpClient.get(`${environment.apiUrl}/tenders/all`).subscribe((response: any) => {
      this.data = response;
      this.rows = this.data.tenders;
      this.usr = this.data.usr;

      for (const row of this.rows) {
        // console.log(row.where_from_city);
        row.created_d = moment(row.created_at).format('DD.MM.YY');
        row.created_t = moment(row.created_at).format('HH:mm');
        row.createddi = moment(row.created_at).format('DD-M-YYYY');
        // row.created = '<strong>' + created_d + '</strong><br>klo <strong>' + created_t + '</strong>';
        // row.wherefrom = JSON.parse(row.where_from);
        
        // row.wherefrom.city = row.where_from_city;
        // row.wherefrom.type_size = row.where_from_type_size;
        // row.timeleft = momentcountdown(row.epoch).countdown().toString();
        row.created_time = new Date(row.created_at).getTime();
        countdown.setLabels(
          ' millisecond| second| min|t| päivä| week| month| year| decade| century| millennium',
          ' milliseconds| seconds| min|t| päivää| weeks| months| years| decades| centuries| millennia',
          ' ',
          ' ',
          ' ',
        );
        if (row.to_be_answered) {
          row.time = new Date(row.to_be_answered).getTime();
          if (row.time > new Date().getTime()) {
            row.timeleft = countdown(
              null,
              new Date(row.to_be_answered),
              countdown.DAYS | countdown.HOURS | countdown.MINUTES
            ).toString();
          } else {
            row.timeleft = 'Ajankohta ylitetty';
          }
        } else {
          row.time = new Date(row.epoch).getTime();
          if (row.time > new Date().getTime()) {
            row.timeleft = countdown(
              null,
              new Date(row.epoch),
              countdown.DAYS | countdown.HOURS | countdown.MINUTES
            ).toString();
          } else {
            row.timeleft = 'Ajankohta ylitetty';
          }
        }

      }

      this.muutot = this.rows.filter(rw => rw.type == 'Muutto');
      this.rahdit = this.rows.filter(rw => rw.type == 'Rahti');
      this.ajoneuvot = this.rows.filter(rw => rw.type == 'Ajoneuvo');
      this.massat = this.rows.filter(rw => rw.type == 'Massa');
      this.paketit = this.rows.filter(rw => rw.type == 'Paketti');

      this.tempDataMuutot = this.muutot;
      this.tempDataRahdit = this.rahdit;
      this.tempDataAjoneuvot = this.ajoneuvot;
      this.tempDataMassat = this.massat;
      this.tempDataPaketit = this.paketit;

      this.loading = false;
    });
  }
  getRespondedTenders() {
    this._httpClient.get(`${environment.apiUrl}/tenders/responded`).subscribe((response: any) => {
      this.data = response;
      this.rows = this.data.tenders;
      this.usr = this.data.usr;

      for (const row of this.rows) {
        row.created_d = moment(row.created_at).format('DD.MM.YY');
        row.created_t = moment(row.created_at).format('HH:mm');
        row.createddi = moment(row.created_at).format('DD-M-YYYY');
        // row.created = '<strong>' + created_d + '</strong><br>klo <strong>' + created_t + '</strong>';
        // row.wherefrom = JSON.parse(row.where_from);
        // row.timeleft = momentcountdown(row.epoch).countdown().toString();
        countdown.setLabels(
          ' millisecond| second| min|t| päivä| week| month| year| decade| century| millennium',
          ' milliseconds| seconds| min|t| päivää| weeks| months| years| decades| centuries| millennia',
          ' ',
          ' ',
          ' ',
        );
        row.timeleft = countdown(
          null,
          new Date(row.epoch),
          countdown.DAYS | countdown.HOURS | countdown.MINUTES
        ).toString();

      }

      this.muutot = this.rows.filter(rw => rw.type == 'Muutto');
      this.rahdit = this.rows.filter(rw => rw.type == 'Rahti');
      this.ajoneuvot = this.rows.filter(rw => rw.type == 'Ajoneuvo');
      this.massat = this.rows.filter(rw => rw.type == 'Massa');
      this.paketit = this.rows.filter(rw => rw.type == 'Paketti');


      this.tempDataMuutot = this.muutot;
      this.tempDataRahdit = this.rahdit;
      this.tempDataAjoneuvot = this.ajoneuvot;
      this.tempDataMassat = this.massat;
      this.tempDataPaketit = this.paketit;

      this.loading = false;
    });
  }
  getAcceptedTenders() {
    this._httpClient.get(`${environment.apiUrl}/tenders/accepted`).subscribe((response: any) => {
      this.data = response;
      this.rows = this.data.tenders;
      this.usr = this.data.usr;

      for (const row of this.rows) {
        row.created_d = moment(row.created_at).format('DD.MM.YY');
        row.created_t = moment(row.created_at).format('HH:mm');
        row.createddi = moment(row.created_at).format('DD-M-YYYY');
        // row.created = '<strong>' + created_d + '</strong><br>klo <strong>' + created_t + '</strong>';
        // row.wherefrom = JSON.parse(row.where_from);
        // row.timeleft = momentcountdown(row.epoch).countdown().toString();
        countdown.setLabels(
          ' millisecond| second| min|t| päivä| week| month| year| decade| century| millennium',
          ' milliseconds| seconds| min|t| päivää| weeks| months| years| decades| centuries| millennia',
          ' ',
          ' ',
          ' ',
        );
        row.timeleft = countdown(
          null,
          new Date(row.epoch),
          countdown.DAYS | countdown.HOURS | countdown.MINUTES
        ).toString();

      }

      this.muutot = this.rows.filter(rw => rw.type == 'Muutto');
      this.rahdit = this.rows.filter(rw => rw.type == 'Rahti');
      this.ajoneuvot = this.rows.filter(rw => rw.type == 'Ajoneuvo');
      this.massat = this.rows.filter(rw => rw.type == 'Massa');
      this.paketit = this.rows.filter(rw => rw.type == 'Paketti');

      this.tempDataMuutot = this.muutot;
      this.tempDataRahdit = this.rahdit;
      this.tempDataAjoneuvot = this.ajoneuvot;
      this.tempDataMassat = this.massat;
      this.tempDataPaketit = this.paketit;

      this.loading = false;
    });
  }
}

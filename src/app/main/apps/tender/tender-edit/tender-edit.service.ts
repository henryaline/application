import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class TenderEditService implements Resolve<any> {
  apiData: any;
  apiDataa: any;
  onDataChanged: BehaviorSubject<any>;
  onDataChangedd: BehaviorSubject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _toastrService: ToastrService, private router: Router) {
    // Set the defaults
    this.onDataChanged = new BehaviorSubject({});
    this.onDataChangedd = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    let currentId = Number(route.paramMap.get('id'));
    return new Promise<void>((resolve, reject) => {
      Promise.all([this.getApiData(currentId)]).then(() => {
        // console.log(this.apiData);
        resolve();
      }, reject);
    });

  }

  /**
   * Get API Data
   */
  getApiData(id: number): Promise<any[]> {
    // const url = `api/invoice-data/${id}`;
    // const url = `${environment.apiUrl}/tenders/all`;
    const url = `${environment.apiUrl}/tenders/get/${id}`;
    return new Promise((resolve, reject) => {
      this._httpClient.get(url).subscribe((response: any) => {
        this.apiData = response.tenders.find(i => i.id == id);
        this.apiDataa = response.usr;
        this.onDataChanged.next(this.apiData);
        this.onDataChangedd.next(this.apiDataa);
        resolve(this.apiData);
      }, reject);
    });
  }

  editTender(id, jatetty, ajankohta, plusminus, viimeistaan, kiireinen, tunniste, kieli, avauksia, etunimi, sukunimi, sposti, puhelin, yritys, ytunnus, kategoria, tyyppif, tyyppit, sisalto, koko, leveys, korkeus, pituus, maa, osoite, postinumero, kaupunki, lisatiedot, kohde_koko, kohde_maa, kohde_osoite, kohde_postinumero, kohde_kaupunki, kohde_lisatiedot, kaikki_lisatiedot): Promise<any[]> {
    const url = `${environment.apiUrl}/tenders/edit`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, {
        id, jatetty, ajankohta, plusminus, viimeistaan, kiireinen, tunniste, kieli, avauksia, etunimi, sukunimi, sposti, puhelin, yritys, ytunnus, kategoria, tyyppif, tyyppit, sisalto, koko, leveys, korkeus, pituus, maa, osoite, postinumero, kaupunki, lisatiedot, kohde_koko, kohde_maa, kohde_osoite, kohde_postinumero, kohde_kaupunki, kohde_lisatiedot, kaikki_lisatiedot
      }).subscribe((response: any) => {
        setTimeout(() => {
          this._toastrService.success(
            'Tarjouspyyntö tallennettu.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }

  deleteImages(poistettavat): Promise<any[]> {
    const url = `${environment.apiUrl}/tenders/delete-images`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, {
        poistettavat
      }).subscribe((response: any) => {
        setTimeout(() => {
          this._toastrService.success(
            'Valitut kuvat poistettu.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { CoreSidebarService } from '@core/components/core-sidebar/core-sidebar.service';

import { TenderEditService } from 'app/main/apps/tender/tender-edit/tender-edit.service';
import * as moment from 'moment';
import * as countdown from 'countdown';

import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Breadcrumb } from 'app/layout/components/content-header/breadcrumb/breadcrumb.component';
import { Location } from '@angular/common';
import { TenderListService } from '../tender-list/tender-list.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpEvent, HttpEventType, HttpRequest, HttpResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { resolve } from 'path';

@Component({
  selector: 'app-tender-edit',
  templateUrl: './tender-edit.component.html',
  styleUrls: ['./tender-edit.service.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TenderEditComponent implements OnInit {
  // public
  public breadcrumbDefault: Breadcrumb;

  public usr;
  public apiData;
  public urlLastValue;
  public url = this.router.url;
  public sidebarToggleRef = false;
  public paymentSidebarToggle = false;
  public paymentDetails = {
    totalDue: '$12,110.55',
    bankName: 'American Bank',
    country: 'United States',
    iban: 'ETD95476213874685',
    swiftCode: 'BR91905'
  };

  public swiperAutoplay: SwiperConfigInterface = {
    spaceBetween: 30,
    centeredSlides: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    }
  };

  // private
  private _unsubscribeAll: Subject<any>;
  private _unsubscribeAlll: Subject<any>;

  public form: FormGroup;
  public kuvatform: FormGroup;
  public tenderType;
  public tenderSubType;
  public tenderSubTypeTo;
  public errormessage;
  public ernormessage;

  kuvat = [];
  // public selectedFiles: FileList;
  // public progressInfos = [];
  // public message = '';

  selectedFiles: FileList;
  progressInfos = [];
  message = '';
  fileInfos: Observable<any> | null = null;

  public timeoutti = 500;
  public poistettavat = 0;

  /**
   * Constructor
   *
   * @param {Router} router
   * @param {TenderEditService} _tenderEditService
   * @param {CoreSidebarService} _coreSidebarService
   */
  constructor(
    private router: Router,
    private _tenderEditService: TenderEditService,
    private _coreSidebarService: CoreSidebarService,
    private location: Location,
    private _tenderListService: TenderListService,
    private _formBuilder: FormBuilder,
    private httpClient: HttpClient
  ) {
    this._unsubscribeAll = new Subject();
    this._unsubscribeAlll = new Subject();
    this.urlLastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
  }

  // Public Methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Toggle the sidebar
   *
   * @param name
   */
  toggleSidebar(name): void {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }

  goBack() {
    this.location.back();
  }

  saveChanges(id: number) {
    let confrm = confirm('Haluatko varmasti tallentaa muutokset?');
    if (confrm) {
      // console.log(id)
      this.editTender(id);
    } else {
      // console.log('fals')
    }
  }

  onTypeChange(val) {
    this.tenderType = val;
    this.form.controls['sisalto'].reset()
    this.form.controls['koko'].reset()
    this.form.controls['leveys'].reset()
    this.form.controls['korkeus'].reset()
    this.form.controls['pituus'].reset()
  }

  get f() {
    return this.form.controls;
  }

  editTender(id: number) {
    // console.log(this.f);
    this.errormessage = '';
    this._tenderEditService.editTender(
      id,
      this.f.jatetty.value,
      this.f.ajankohta.value,
      this.f.plusminus.value,
      this.f.viimeistaan.value,
      this.f.kiireinen.value,
      this.f.tunniste.value,
      this.f.kieli.value,
      this.f.avauksia.value,
      this.f.etunimi.value,
      this.f.sukunimi.value,
      this.f.sposti.value,
      this.f.puhelin.value,
      this.f.yritys.value,
      this.f.ytunnus.value,
      this.f.kategoria.value,
      this.f.tyyppif.value,
      this.f.tyyppit.value,
      this.f.sisalto.value,
      this.f.koko.value,
      this.f.leveys.value,
      this.f.korkeus.value,
      this.f.pituus.value,
      this.f.maa.value,
      this.f.osoite.value,
      this.f.postinumero.value,
      this.f.kaupunki.value,
      this.f.lisatiedot.value,
      this.f.kohde_koko.value,
      this.f.kohde_maa.value,
      this.f.kohde_osoite.value,
      this.f.kohde_postinumero.value,
      this.f.kohde_kaupunki.value,
      this.f.kohde_lisatiedot.value,
      this.f.kaikki_lisatiedot.value,
      // this.f.kuvat.value,
      // this.f.kuvatSource.value
    ).then(() => {
      // this.router.navigate(['/apps/tender/preview/' + id]);
    }).catch((msg) => {
      // console.log(msg);
      this.errormessage = msg;
    });
  }

  // Kuvien käsittely
  getTenderKuvat() {
    this.getKuvat().then((resp: any) => {
      this.kuvat = resp.images;
    }).catch((e) => this.ernormessage = e);
  }
  getKuvat(): Promise<any[]> {
    const id = this.apiData.id
    const url = `${environment.apiUrl}/tenders/get-images/${id}`;

    return new Promise((resolve, reject) => {
      this.httpClient.get(url).subscribe((response: any) => {
        // this.kuvat = response.images;
        resolve(response);
      }, reject);
    });
  }
  uploadToServer(file: File): Observable<HttpEvent<any>> {
    const url = `${environment.apiUrl}/tenders/add-images`;
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('id', this.apiData.id);
    const request = new HttpRequest('POST', url, formData, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.httpClient.request(request);
  }
  selectFiles(e): void {
    this.progressInfos = [];
    this.selectedFiles = e.target.files;
  }
  uploadFiles(): void {
    this.message = '';
    for (let i = 0; i < this.selectedFiles.length; i++) {
      this.upload(i, this.selectedFiles[i]);
    }
    setTimeout(() => {
      this.progressInfos = [];
      (<HTMLInputElement>document.getElementById('imgs')).value = null;
    }, this.timeoutti);
  }
  upload(idx, file): void {
    this.timeoutti = 500;
    this.progressInfos[idx] = { value: 0, fileName: file.name };
    this.uploadToServer(file).subscribe(
      event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progressInfos[idx].value = Math.round(100 * event.loaded / event.total);
        }
        this.getTenderKuvat();
      },
      err => {
        this.progressInfos[idx].value = 0;
        this.message = 'Kuvan lähetys epäonnistui: ' + file.name;
        console.log(err);
      });
  }
  onCheckboxChange(e) {
    const checkArray: FormArray = this.kuvatform.get('checkArray') as FormArray;
    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
    this.poistettavat = checkArray.length;
  }
  submitCheckboxForm() {
    let confrm = confirm('Poistetaanko kuvat? Tämä tapahtuma astuu voimaan heti.');
    if (confrm) {
      this.ernormessage = '';
      this._tenderEditService.deleteImages(
        this.kuvatform.value
      ).then((resp) => {
        this.getTenderKuvat();
        const formArray: FormArray = this.kuvatform.get('checkArray') as FormArray;
        while (formArray.length !== 0) {
          formArray.removeAt(0)
        }
        this.poistettavat = 0;
      }).catch((msg) => {
        this.ernormessage = msg;
      });
    }
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this._tenderEditService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.apiData = response;
      this.tenderType = this.apiData.type;
      this.tenderSubType = this.apiData.subtype_from
      this.tenderSubTypeTo = this.apiData.subtype_to
      this.apiData.created_d = moment(this.apiData.created_at).format('DD.MM.YYYY');
      this.apiData.created_t = moment(this.apiData.created_at).format('HH:mm');
      this.apiData.epoch_formatted = moment(this.apiData.epoch).format('DD.MM.YYYY');
      if (this.apiData.responded_at && this.apiData.responded_at.length > 0) {
        this.apiData.acceptedTime = moment(this.apiData.responded_at[0].accepted_at).format('DD.MM.YYYY HH:mm');
      }
      if (this.apiData.to_be_answered) {
        this.apiData.to_be_answered_formatted = moment(this.apiData.to_be_answered).format('DD.MM.YYYY');
      }

      // row.created = '<strong>' + created_d + '</strong><br>klo <strong>' + created_t + '</strong>';
      if (this.apiData.contact_details) {
        this.apiData.contactdetails = JSON.parse(this.apiData.contact_details);
      }
      this.apiData.wherefrom = JSON.parse(this.apiData.where_from);
      this.apiData.whereto = JSON.parse(this.apiData.where_to);
      // row.timeleft = momentcountdown(row.epoch).countdown().toString();
      countdown.setLabels(
        ' millisecond| second| min|t| päivä| week| month| year| decade| century| millennium',
        ' milliseconds| seconds| min|t| päivää| weeks| months| years| decades| centuries| millennia',
        ' ',
        ' ',
        ' ',
      );
      if (this.apiData.to_be_answered) {
        this.apiData.timeleft = countdown(
          null,
          new Date(this.apiData.to_be_answered),
          countdown.DAYS | countdown.HOURS | countdown.MINUTES
        ).toString();
      } else {
        this.apiData.timeleft = countdown(
          null,
          new Date(this.apiData.epoch),
          countdown.DAYS | countdown.HOURS | countdown.MINUTES
        ).toString();
      }
    });
    this._tenderEditService.onDataChangedd.pipe(takeUntil(this._unsubscribeAlll)).subscribe(resp => {
      this.usr = resp;
    });
    this.breadcrumbDefault = {
      links: [
        {
          name: 'Tarjouspyynnöt',
          isLink: true,
          link: '/dashboard/tenders/all'
        },
        {
          name: 'Tarjouspyyntö',
          isLink: false,
        }
      ]
    };
    this.form = this._formBuilder.group({
      jatetty: [this.apiData.created_d + ' ' + this.apiData.created_t, Validators.required],
      ajankohta: [this.apiData.epoch_formatted, Validators.required],
      plusminus: [this.apiData.epoch_plusminus_days],
      viimeistaan: [this.apiData.to_be_answered_formatted],
      kiireinen: [this.apiData.show_contact_details],
      tunniste: [this.apiData.identifier_code, Validators.required],
      kieli: [this.apiData.lang, Validators.required],
      avauksia: [this.apiData.opens_left, Validators.required],

      etunimi: [this.apiData.contactdetails.firstname, Validators.required],
      sukunimi: [this.apiData.contactdetails.lastname, Validators.required],
      sposti: [this.apiData.contactdetails.email, Validators.required],
      puhelin: [this.apiData.contactdetails.mobile, Validators.required],
      yritys: [this.apiData.contactdetails.company],
      ytunnus: [this.apiData.contactdetails.companyid],

      kategoria: [this.apiData.type, Validators.required],
      tyyppif: [this.apiData.subtype_from],
      tyyppit: [this.apiData.subtype_to],
      sisalto: [this.apiData.subtype_from],

      koko: [this.apiData.wherefrom.type_size, Validators.required],
      leveys: [this.apiData.wherefrom.width],
      korkeus: [this.apiData.wherefrom.height],
      pituus: [this.apiData.wherefrom.length],
      maa: [this.apiData.wherefrom.country, Validators.required],
      osoite: [this.apiData.wherefrom.address, Validators.required],
      postinumero: [this.apiData.wherefrom.postal_code, Validators.required],
      kaupunki: [this.apiData.wherefrom.city, Validators.required],
      lisatiedot: [this.apiData.wherefrom.info],

      kohde_koko: [this.apiData.whereto.type_size],
      kohde_maa: [this.apiData.whereto.country, Validators.required],
      kohde_osoite: [this.apiData.whereto.address, Validators.required],
      kohde_postinumero: [this.apiData.whereto.postal_code, Validators.required],
      kohde_kaupunki: [this.apiData.whereto.city, Validators.required],
      kohde_lisatiedot: [this.apiData.whereto.info],
      kaikki_lisatiedot: [this.apiData.information],

      // kuvat: [],
      // kuvatSource: []
    });
    this.kuvatform = this._formBuilder.group({
      checkArray: this._formBuilder.array([])
    });
    this.getTenderKuvat();
  }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { CoreSidebarService } from '@core/components/core-sidebar/core-sidebar.service';

import { TenderPreviewService } from 'app/main/apps/tender/tender-preview/tender-preview.service';
import * as moment from 'moment';
import * as countdown from 'countdown';

import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Breadcrumb } from 'app/layout/components/content-header/breadcrumb/breadcrumb.component';
import { Location } from '@angular/common';
import { TenderListService } from '../tender-list/tender-list.service';



@Component({
  selector: 'app-tender-preview',
  templateUrl: './tender-preview.component.html',
  styleUrls: ['./tender-preview.service.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TenderPreviewComponent implements OnInit {
  // public
  public breadcrumbDefault: Breadcrumb;

  public usr;
  public apiData;
  public urlLastValue;
  public url = this.router.url;
  public sidebarToggleRef = false;
  public paymentSidebarToggle = false;
  public paymentDetails = {
    totalDue: '$12,110.55',
    bankName: 'American Bank',
    country: 'United States',
    iban: 'ETD95476213874685',
    swiftCode: 'BR91905'
  };

  public swiperAutoplay: SwiperConfigInterface = {
    spaceBetween: 30,
    centeredSlides: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    }
  };

  // private
  private _unsubscribeAll: Subject<any>;
  private _unsubscribeAlll: Subject<any>;

  /**
   * Constructor
   *
   * @param {Router} router
   * @param {TenderPreviewService} _tenderPreviewService
   * @param {CoreSidebarService} _coreSidebarService
   */
  constructor(
    private router: Router,
    private _tenderPreviewService: TenderPreviewService,
    private _coreSidebarService: CoreSidebarService,
    private location: Location,
    private _tenderListService: TenderListService,
  ) {
    this._unsubscribeAll = new Subject();
    this._unsubscribeAlll = new Subject();
    this.urlLastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
  }

  // Public Methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Toggle the sidebar
   *
   * @param name
   */
  toggleSidebar(name): void {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }

  goBack() {
    this.location.back();
  }

  newEvent(tender_id) {
    this._tenderListService
      // .login(this.f.email.value, this.f.password.value)
      .newEvent(tender_id)
      .pipe(first())
      .subscribe(
        data => {
          // this._router.navigate([this.returnUrl]);
        },
        error => {
          // this.error = error;
          // this.loading = false;
          // console.log(error);
        }
      );
  }
  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    this._tenderPreviewService.onDataChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
      this.apiData = response;
      this.apiData.created_d = moment(this.apiData.created_at).format('DD.MM.YYYY');
      this.apiData.created_t = moment(this.apiData.created_at).format('HH:mm');
      this.apiData.epoch_formatted = moment(this.apiData.epoch).format('DD.MM.YYYY');
      if (this.apiData.responded_at && this.apiData.responded_at.length > 0) {
        this.apiData.acceptedTime = moment(this.apiData.responded_at[0].accepted_at).format('DD.MM.YYYY HH:mm');
      }
      if (this.apiData.to_be_answered) {
        this.apiData.to_be_answered_formatted = moment(this.apiData.to_be_answered).format('DD.MM.YYYY');
      }


      // row.created = '<strong>' + created_d + '</strong><br>klo <strong>' + created_t + '</strong>';
      if (this.apiData.contact_details) {
        this.apiData.contactdetails = JSON.parse(this.apiData.contact_details);
      }
      this.apiData.wherefrom = JSON.parse(this.apiData.where_from);
      this.apiData.whereto = JSON.parse(this.apiData.where_to);
      // row.timeleft = momentcountdown(row.epoch).countdown().toString();
      countdown.setLabels(
        ' millisecond| second| min|t| päivä| week| month| year| decade| century| millennium',
        ' milliseconds| seconds| min|t| päivää| weeks| months| years| decades| centuries| millennia',
        ' ',
        ' ',
        ' ',
      );
      if (this.apiData.to_be_answered) {
        this.apiData.timeleft = countdown(
          null,
          new Date(this.apiData.to_be_answered),
          countdown.DAYS | countdown.HOURS | countdown.MINUTES
        ).toString();
      } else {
        this.apiData.timeleft = countdown(
          null,
          new Date(this.apiData.epoch),
          countdown.DAYS | countdown.HOURS | countdown.MINUTES
        ).toString();
      }
    });
    
    this.newEvent(this.apiData.id);

    this._tenderPreviewService.onDataChangedd.pipe(takeUntil(this._unsubscribeAlll)).subscribe(resp => {
      this.usr = resp;
      // console.log(this.usr);
    });


    this.breadcrumbDefault = {
      links: [
        {
          name: 'Tarjouspyynnöt',
          isLink: true,
          link: '/dashboard/tenders/all'
        },
        {
          name: 'Tarjouspyyntö',
          isLink: false,
        }
      ]
    };

    
  }
}

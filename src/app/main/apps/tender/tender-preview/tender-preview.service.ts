import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';

import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class TenderPreviewService implements Resolve<any> {
  apiData: any;
  apiDataa: any;
  onDataChanged: BehaviorSubject<any>;
  onDataChangedd: BehaviorSubject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _toastrService: ToastrService) {
    // Set the defaults
    this.onDataChanged = new BehaviorSubject({});
    this.onDataChangedd = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    let currentId = Number(route.paramMap.get('id'));
    return new Promise<void>((resolve, reject) => {
      Promise.all([this.getApiData(currentId)]).then(() => {
        // console.log(this.apiData);
        resolve();
      }, reject);
    });

  }

  /**
   * Get API Data
   */
  getApiData(id: number): Promise<any[]> {
    // const url = `api/invoice-data/${id}`;
    // const url = `${environment.apiUrl}/tenders/all`;
    const url = `${environment.apiUrl}/tenders/get/${id}`;


    return new Promise((resolve, reject) => {
      this._httpClient.get(url).subscribe((response: any) => {
        this.apiData = response.tenders.find(i => i.id == id);
        this.apiDataa = response.usr;
        this.onDataChanged.next(this.apiData);
        this.onDataChangedd.next(this.apiDataa);
        resolve(this.apiData);
      }, reject);
    });
  }

  addResponse(tender_id: number, content: string, termcontent: string, sendcopy): Promise<any[]> {
    const url = `${environment.apiUrl}/events/new-response`;
    return new Promise((resolve, reject) => {
      this._httpClient.post<any>(url, { tender_id, content, termcontent, sendcopy }).subscribe((response: any) => {
        // this._router.navigate(['/apps/company/company-view/' + response.company.id]);
        setTimeout(() => {
          this._toastrService.success(
            'Vastaus lähetetty.',
            '',
            { toastClass: 'toast ngx-toastr', closeButton: true }
          );
        }, 2500);
        // console.log(response);
        resolve(response.message);
      }, reject);
    });
  }
}

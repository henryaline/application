import { Component, Input, OnInit } from '@angular/core';

import { CoreSidebarService } from '@core/components/core-sidebar/core-sidebar.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TenderPreviewService } from '../../tender-preview.service';

@Component({
  selector: 'app-add-payment-sidebar-preview',
  templateUrl: './add-payment-sidebar-preview.component.html',
  styleUrls: ['./add-payment-sidebar-preview.component.scss'],

})
export class AddPaymentSidebarPreviewComponent implements OnInit {
  // ng2-flatpickr options
  public paymentDateOptions = {
    altInput: true,
    mode: 'single',
    altInputClass: 'form-control flat-picker flatpickr-input invoice-edit-input',
    defaultDate: ['2020-05-01'],
    altFormat: 'Y-n-j'
  };
  @Input() tenderid: any;
  @Input() tendertype: any;

  onDatatablessChanged: BehaviorSubject<any>;
  rows: any;
  options: any = [];
  contractoptions: any = [];
  public cmpny: any;

  private _unsubscribeAll: Subject<any>;
  public form: FormGroup;



  constructor(
    private _coreSidebarService: CoreSidebarService,
    private _httpClient: HttpClient,
    private _formBuilder: FormBuilder,
    private _tenderPreviewService: TenderPreviewService) {
    this.onDatatablessChanged = new BehaviorSubject({});
    this._unsubscribeAll = new Subject();
  }

  get f() {
    return this.form.controls;
  }

  /**
   * Toggle the sidebar
   *
   * @param name
   */
  toggleSidebar(name): void {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }

  ngOnInit(): void {
    // console.log(this.options);
    this.getjotain();
    this.form = this._formBuilder.group({
      tender_id: [this.tenderid, Validators.required],
      content: ['', Validators.required],
      term_content: [''],
      sendcopy: [''],
    });
  }

  changeValue(event, type) {
    if (type == 'tp') {
      const val = event.target.value;
      const option = this.options.find(i => i.id == val);
      this.form.controls['content'].setValue(option.content);
    } else {
      const val = event.target.value;
      const term = this.contractoptions.find(i => i.id == val);
      this.form.controls['term_content'].setValue(term.content);
    }
  }

  submitResponse() {
    if (confirm("Haluatko varmasti vastata tarjouspyyntöön?")) {
      // console.log(this.f);
      this._tenderPreviewService.addResponse(
        this.f.tender_id.value,
        this.f.content.value,
        this.f.term_content.value,
        this.f.sendcopy.value,
      ).then(() => this._tenderPreviewService.getApiData(this.tenderid));
      setTimeout(() => {
        this.toggleSidebar('add-payment-sidebar');
      }, 500);
    }
  }

  // getDataTableRows(): Promise<any[]> {
  //   return new Promise((resolve, reject) => {
  //     // this._httpClient.get('http://localhost:4200/api/invoice-data').subscribe((response: any) => {
  //     this._httpClient.get(`${environment.apiUrl}/companies/offertemplates`).subscribe((response: any) => {
  //       this.rows = response;
  //       this.onDatatablessChanged.next(this.rows);
  //       resolve(this.rows);
  //     }, reject);
  //   });
  // }

  getjotain() {
    // this.getDataTableRows.onDatatablessChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(response => {
    //   console.log(response);
    // });
    this._httpClient.get(`${environment.apiUrl}/companies/offertemplates`).subscribe((response: any) => {
      this.options = response.templates;
      this.cmpny = response.company;
      this.contractoptions = response.terms;
      // console.log(this.options);
      // console.log(this.contractoptions);
    });
  }
}

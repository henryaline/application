import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';

import { CoreCommonModule } from '@core/common.module';
import { CoreDirectivesModule } from '@core/directives/directives';
import { CorePipesModule } from '@core/pipes/pipes.module';
import { CoreSidebarModule } from '@core/components';

import { TenderListComponent } from 'app/main/apps/tender/tender-list/tender-list.component';
import { TenderListService } from 'app/main/apps/tender/tender-list/tender-list.service';

// import { TenderAddComponent } from 'app/main/apps/tender/tender-add/tender-add.component';
// import { TenderAddService } from 'app/main/apps/tender/tender-add/tender-add.service';
// import { AddCustomerSidebarComponent } from 'app/main/apps/tender/tender-add/add-customer-sidebar/add-customer-sidebar.component';

import { TenderEditComponent } from 'app/main/apps/tender/tender-edit/tender-edit.component';
import { TenderEditService } from 'app/main/apps/tender/tender-edit/tender-edit.service';
// import { SendTenderSidebarComponent } from 'app/main/apps/tender/tender-edit/sidebar/send-tender-sidebar/send-tender-sidebar.component';
// import { AddPaymentSidebarComponent } from 'app/main/apps/tender/tender-edit/sidebar/add-payment-sidebar/add-payment-sidebar.component';

import { TenderPreviewComponent } from 'app/main/apps/tender/tender-preview/tender-preview.component';
import { TenderPreviewService } from 'app/main/apps/tender/tender-preview/tender-preview.service';
import { AddPaymentSidebarPreviewComponent } from 'app/main/apps/tender/tender-preview/sidebar/add-payment-sidebar-preview/add-payment-sidebar-preview.component';
import { SendTenderSidebarPreviewComponent } from 'app/main/apps/tender/tender-preview/sidebar/send-tender-sidebar-preview/send-tender-sidebar-preview.component';

import { SwiperConfigInterface, SwiperModule, SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { BreadcrumbModule } from 'app/layout/components/content-header/breadcrumb/breadcrumb.module';


// routing
const routes: Routes = [
  // {
  //   path: 'add',
  //   component: TenderAddComponent,
  //   resolve: {
  //     Ias: TenderAddService
  //   }
  // },
  {
    path: 'list',
    component: TenderListComponent,
    resolve: {
      uls: TenderListService
    }
  },
  // {
  //   path: 'list/company-listed',
  //   component: TenderListComponent,
  //   resolve: {
  //     uls: TenderListService
  //   }
  // },
  {
    path: 'preview/:id',
    component: TenderPreviewComponent,
    resolve: {
      data: TenderPreviewService
    },
    data: { path: 'preview/:id' }
  },
  {
    path: 'edit/:id',
    component: TenderEditComponent,
    resolve: {
      data: TenderEditService
    },
    data: { path: 'edit/:id' }
  },
  // {
  //   path: 'preview',
  //   redirectTo: '/apps/tender/preview/4989' // Redirection
  // },
  // {
  //   path: 'edit',
  //   redirectTo: '/apps/tender/edit/4989' // Redirection
  // }
];

@NgModule({
  declarations: [
    // TenderAddComponent,
    TenderListComponent,
    TenderPreviewComponent,
    TenderEditComponent,
    // AddCustomerSidebarComponent,
    // SendTenderSidebarComponent,
    // AddPaymentSidebarComponent,
    SendTenderSidebarPreviewComponent,
    AddPaymentSidebarPreviewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreCommonModule,
    CoreDirectivesModule,
    Ng2FlatpickrModule,
    NgxDatatableModule,
    FormsModule,
    CorePipesModule,
    NgbModule,
    NgSelectModule,
    CoreSidebarModule,
    SwiperModule,
    BreadcrumbModule
  ],
  providers: [
    TenderListService, TenderPreviewService,
    TenderEditService,
    // TenderAddService
  ],
  exports: [TenderListComponent, TenderPreviewComponent, TenderEditComponent]
})
export class TenderModule {}

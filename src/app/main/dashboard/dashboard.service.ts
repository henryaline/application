import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { BehaviorSubject, Observable } from 'rxjs';

import { environment } from 'environments/environment';


@Injectable()
export class DashboardService {
  // Public
  public apiData: any;
  public onApiDataChanged: BehaviorSubject<any>;
  public aapiData: any;
  public oonApiDataChanged: BehaviorSubject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {
    // Set the defaults
    this.onApiDataChanged = new BehaviorSubject({});
    this.oonApiDataChanged = new BehaviorSubject({});
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise<void>((resolve, reject) => {
      Promise.all([]).then(() => {
      // Promise.all([this.getApiData(), this.getDashboardData()]).then(() => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get Api Data
   */
  getApiData(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      // url = return this._http.get<User>(`${environment.apiUrl}/users/${id}`);
      // this._httpClient.get(`${environment.apiUrl}/companies`).subscribe((response: any) => {
      this._httpClient.get('http://localhost:4200/api/dashboard-data').subscribe((response: any) => {
      // this._httpClient.get('api/dashboard-data').subscribe((response: any) => {
        this.apiData = response;
        this.onApiDataChanged.next(this.apiData);
        resolve(this.apiData);
      }, reject);
    });
  }
  getDashboardData(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      // this._httpClient.get('api/users-data').subscribe((response: any) => {
      this._httpClient.get(`${environment.apiUrl}/dashboard`).subscribe((response: any) => {
        this.aapiData = response;
        this.oonApiDataChanged.next(this.aapiData);
        // console.log(this.apiData);
        resolve(this.aapiData);
      }, reject);
    });
  }
}

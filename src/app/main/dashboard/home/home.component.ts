import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';

import { first } from 'rxjs/operators';

import { CoreConfigService } from '@core/services/config.service';

import { colors } from 'app/colors.const';
import { User } from 'app/auth/models';
import { UserService } from 'app/auth/service';
import { DashboardService } from 'app/main/dashboard/dashboard.service';
import { Router } from '@angular/router';

import * as moment from 'moment';
import * as countdown from 'countdown';

import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';

import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  // Decorator
  @ViewChild('gainedChartRef') gainedChartRef: any;
  @ViewChild('orderChartRef') orderChartRef: any;
  @ViewChild('avgSessionChartRef') avgSessionChartRef: any;
  @ViewChild('supportChartRef') supportChartRef: any;
  @ViewChild('salesChartRef') salesChartRef: any;
  
  // Decorator
  @ViewChild(DatatableComponent) table: DatatableComponent;
  public ColumnMode = ColumnMode;

  // Public
  public data: any;
  public dashboardData: any;
  public currentUser: any;
  public loading = false;
  public users: User[] = [];
  public gainedChartoptions;
  public orderChartoptions;
  public avgsessionChartoptions;
  public supportChartoptions;
  public salesChartoptions;

  public isallowed = false;

  public loadingdata = true;

  public tendercount;
  public responsecount;
  public eventcount;
  public barchatDatasetsData = [];
  public barchatLabels = [];

  // Private
  private $primary = '#7367F0';
  private $warning = '#FF9F43';
  private $info = '#00cfe8';
  private $info_light = '#1edec5';
  private $strok_color = '#b9c3cd';
  private $label_color = '#e7eef7';
  private $white = '#fff';
  private $textHeadingColor = '#5e5873';

  // Chartti
  private successColorShade = '#28dac6';
  private tooltipShadow = 'rgba(0, 0, 0, 0.25)';
  private labelColor = '#6e6b7b';
  private grid_line_color = 'rgba(200, 200, 200, 0.2)'; // RGBA color helps in dark layout
  // Tilitykset
  public barChart = {
    chartType: 'bar',
    datasets: [
      {
        label: '€',
        data: [],
        backgroundColor: this.successColorShade,
        borderColor: 'transparent',
        hoverBackgroundColor: this.successColorShade,
        hoverBorderColor: this.successColorShade
      }
    ],
    // labels: ['7/12', '8/12', '9/12', '10/12', '11/12', '12/12', '13/12', '14/12', '15/12', '16/12', '17/12'],
    labels: [],
    options: {
      elements: {
        rectangle: {
          borderWidth: 2,
          borderSkipped: 'bottom'
        }
      },
      responsive: true,
      maintainAspectRatio: false,
      responsiveAnimationDuration: 500,
      legend: {
        display: false
      },
      tooltips: {
        // Updated default tooltip UI
        shadowOffsetX: 1,
        shadowOffsetY: 1,
        shadowBlur: 8,
        shadowColor: this.tooltipShadow,
        backgroundColor: colors.solid.white,
        titleFontColor: colors.solid.black,
        bodyFontColor: colors.solid.black,
        position: 'average'
      },
      scales: {
        xAxes: [
          {
            barThickness: 15,
            display: true,
            gridLines: {
              display: true,
              color: this.grid_line_color,
              zeroLineColor: this.grid_line_color
            },
            scaleLabel: {
              display: true
            },
            ticks: {
              fontColor: this.labelColor
            }
          }
        ],
        yAxes: [
          {
            display: true,
            gridLines: {
              color: this.grid_line_color,
              zeroLineColor: this.grid_line_color
            },
            ticks: {
              // stepSize: 100,
              min: 0,
              // max: 400,
              fontColor: this.labelColor
            }
          }
        ]
      }
    },
    legend: false
  };

  /**
   * Constructor
   *
   * @param {UserService} _userService
   * @param {DashboardService} _dashboardService
   * @param {CoreConfigService} _coreConfigService
   *
   */
  constructor(
    private _userService: UserService,
    private _dashboardService: DashboardService,
    private _coreConfigService: CoreConfigService,
    private router: Router,
    private _httpClient: HttpClient
  ) {
    // Subscribers Gained chart
    this.gainedChartoptions = {
      chart: {
        height: 100,
        type: 'area',
        toolbar: {
          show: false
        },
        sparkline: {
          enabled: true
        }
      },
      colors: [this.$primary],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth',
        width: 2.5
      },
      fill: {
        type: 'gradient',
        gradient: {
          shadeIntensity: 0.9,
          opacityFrom: 0.7,
          opacityTo: 0.5,
          stops: [0, 80, 100]
        }
      },
      tooltip: {
        x: { show: false }
      }
    };

    // Order Received Chart
    this.orderChartoptions = {
      chart: {
        height: 100,
        type: 'area',
        toolbar: {
          show: false
        },
        sparkline: {
          enabled: true
        }
      },
      colors: [this.$warning],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth',
        width: 2.5
      },
      fill: {
        type: 'gradient',
        gradient: {
          shadeIntensity: 0.9,
          opacityFrom: 0.7,
          opacityTo: 0.5,
          stops: [0, 80, 100]
        }
      },
      series: [
        {
          name: 'Orders',
          data: [10, 15, 8, 15, 7, 12, 8]
        }
      ],
      tooltip: {
        x: { show: false }
      }
    };

    // Average Session Chart
    this.avgsessionChartoptions = {
      chart: {
        type: 'bar',
        height: 200,
        sparkline: { enabled: true },
        toolbar: { show: false }
      },
      colors: [
        this.$label_color,
        this.$label_color,
        this.$primary,
        this.$label_color,
        this.$label_color,
        this.$label_color
      ],
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      },
      plotOptions: {
        bar: {
          columnWidth: '45%',
          distributed: true,
          endingShape: 'rounded'
        }
      },
      tooltip: {
        x: { show: false }
      }
    };

    // Support Tracker Chart
    this.supportChartoptions = {
      chart: {
        height: 290,
        type: 'radialBar',
        sparkline: {
          enabled: false
        }
      },
      plotOptions: {
        radialBar: {
          offsetY: 20,
          startAngle: -150,
          endAngle: 150,
          hollow: {
            size: '65%'
          },
          track: {
            background: this.$white,
            strokeWidth: '100%'
          },
          dataLabels: {
            name: {
              offsetY: -5,
              color: this.$textHeadingColor,
              fontSize: '1rem'
            },
            value: {
              offsetY: 15,
              color: this.$textHeadingColor,
              fontSize: '1.714rem'
            }
          }
        }
      },
      colors: [colors.solid.danger],
      fill: {
        type: 'gradient',
        gradient: {
          shade: 'dark',
          type: 'horizontal',
          shadeIntensity: 0.5,
          gradientToColors: [colors.solid.primary],
          inverseColors: true,
          opacityFrom: 1,
          opacityTo: 1,
          stops: [0, 100]
        }
      },
      stroke: {
        dashArray: 8
      },
      labels: ['Completed Tickets']
    };

    // Sales Chart
    this.salesChartoptions = {
      chart: {
        height: 330,
        type: 'radar',
        dropShadow: {
          enabled: true,
          blur: 8,
          left: 1,
          top: 1,
          opacity: 0.2
        },
        toolbar: {
          show: false
        }
      },
      stroke: {
        width: 0
      },
      colors: [this.$primary, this.$info],
      plotOptions: {
        radar: {
          polygons: {
            connectorColors: 'transparent'
          }
        }
      },
      fill: {
        type: 'gradient',
        gradient: {
          shade: 'dark',
          gradientToColors: ['#9f8ed7', this.$info_light],
          shadeIntensity: 1,
          type: 'horizontal',
          opacityFrom: 1,
          opacityTo: 1,
          stops: [0, 100, 100, 100]
        }
      },
      markers: {
        size: 0
      },
      legend: {
        show: false
      },
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
      dataLabels: {
        style: {
          colors: [
            this.$strok_color,
            this.$strok_color,
            this.$strok_color,
            this.$strok_color,
            this.$strok_color,
            this.$strok_color
          ]
        }
      },
      yaxis: {
        show: false
      }
    };
  }

  changeTimespan(value) {

    var otherselement = document.getElementsByClassName('aikapainike');
    for (let index = 0; index < otherselement.length; index++) {
      const elem = otherselement[index] as HTMLElement;
      elem.classList.remove('bg-primary');
      elem.classList.remove('text-white');
    }

    var element = document.getElementById(value);
    const e = element as HTMLElement;
    e.classList.add('bg-primary');
    e.classList.add('text-white');

    if (value == 'today') {
      this.tendercount = this.dashboardData.tendercount_today;
      this.responsecount = this.dashboardData.responsecount_today
      this.eventcount = this.dashboardData.eventcount_today
    } else if (value == 'week') {
      this.tendercount = this.dashboardData.tendercount_thisweek;
      this.responsecount = this.dashboardData.responsecount_thisweek;
      this.eventcount = this.dashboardData.eventcount_thisweek;
    } else if (value == 'month') {
      this.tendercount = this.dashboardData.tendercount_thismonth;
      this.responsecount = this.dashboardData.responsecount_thismonth;
      this.eventcount = this.dashboardData.eventcount_thismonth;
    } else if (value == 'year') {
      this.tendercount = this.dashboardData.tendercount_thisyear;
      this.responsecount = this.dashboardData.responsecount_thisyear;
      this.eventcount = this.dashboardData.eventcount_thisyear;
    }
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    // get the currentUser details from localStorage
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    // console.log(this.currentUser);

    /**
     * Get the secure api service (based on user role) (Admin Only secure API)
     * For example purpose
     */
    // this.loading = true;
    // this._userService
    //   .getAll()
    //   // .subscribe(t => { console.log(t) }, e => console.log(e));
    //   .pipe(first())
    //   .subscribe(users => {
    //     this.loading = false;
    //     this.users = users;
    //   });

    // Get the dashboard service data
    // this._dashboardService.onApiDataChanged.subscribe(response => {
    //   this.data = response;
    // });
    // this._dashboardService.oonApiDataChanged.subscribe(response => {
    //   this.dashboardData = response;
    // });

    this.loadingdata = true;
    this.getDashboardData();

  }

  sum(...args) {
    var total = 0;
    args.forEach(arg => {
      total += arg;
    });
    // console.log(total);
  }

  getDashboardData() {
    this._httpClient.get(`${environment.apiUrl}/dashboard`).subscribe((response: any) => {
      this.dashboardData = response;
      if (this.dashboardData.usr.role == 'Admin' || this.dashboardData.usr.role == 'Moderator' || this.dashboardData.usr.role == 'Yrittäjä') {
        this.isallowed = true;
      } else {
        this.router.navigate(['/dashboard/tenders/all']);
      }
  
      if (this.dashboardData.usr.role == 'Admin') {
        for (let index = 0; index < this.dashboardData.top3Opened.length; index++) {
          const tndr = this.dashboardData.top3Opened[index];
          this.dashboardData.top3Opened[index].wherefrom = JSON.parse(tndr.where_from);
          this.dashboardData.top3Opened[index].whereto = JSON.parse(tndr.where_to);
          // console.log(this.dashboardData.top3Opened[index])
        }
        if (this.dashboardData.failedPayments.length > 0) {
          this.dashboardData.failedPayments.sort(function (aa, bb) { return bb.id - aa.id });
        }
        // console.log(this.dashboardData.settlements);
        // console.log(typeof this.dashboardData.settlements);
        // console.log(this.dashboardData.settlements.length);
        Object.keys(this.dashboardData.settlements).forEach(key => {
          // console.log(key, this.dashboardData.settlements[key]);
          let total = 0;
          for (let index = 0; index < this.dashboardData.settlements[key].length; index++) {
            const timespan = this.dashboardData.settlements[key][index];
            // console.log(timespan);
            total += parseFloat(timespan.total);
            // this.sum(total, timespan.total);
          }
          this.dashboardData.settlements[key].total = total;
          this.barchatDatasetsData.push(total);
          this.barchatLabels.push(key);
        });
        // console.log(this.dashboardData.settlements);
        // this.barChart.datasets['data'] = this.barchatDatasetsData;
        this.barChart.datasets = [
          {
            label: '€',
            data: this.barchatDatasetsData,
            backgroundColor: this.successColorShade,
            borderColor: 'transparent',
            hoverBackgroundColor: this.successColorShade,
            hoverBorderColor: this.successColorShade
          }
        ];
        this.barChart.labels = this.barchatLabels;
        // console.log(this.barChart);
      }
  
      for (let index = 0; index < this.dashboardData.latestTenders.length; index++) {
        const tndr = this.dashboardData.latestTenders[index];
        this.dashboardData.latestTenders[index].wherefrom = JSON.parse(tndr.where_from);
        this.dashboardData.latestTenders[index].whereto = JSON.parse(tndr.where_to);
        this.dashboardData.latestTenders[index].created_d = moment(this.dashboardData.latestTenders[index].created_at).format('DD.MM.YYYY');
        this.dashboardData.latestTenders[index].created_t = moment(this.dashboardData.latestTenders[index].created_at).format('HH:mm');
      }
      countdown.setLabels(
        ' millisecond| second| min|t| päivä| week| month| year| decade| century| millennium',
        ' milliseconds| seconds| min|t| päivää| weeks| months| years| decades| centuries| millennia',
        ' ',
        ' ',
        ' ',
      );
      for (let index = 0; index < this.dashboardData.closingTenders.length; index++) {
        const tndr = this.dashboardData.closingTenders[index];
        this.dashboardData.closingTenders[index].wherefrom = JSON.parse(tndr.where_from);
        this.dashboardData.closingTenders[index].whereto = JSON.parse(tndr.where_to);
        this.dashboardData.closingTenders[index].ending_d = moment(this.dashboardData.closingTenders[index].epoch).format('DD.MM.YYYY');
        this.dashboardData.closingTenders[index].ending_t = moment(this.dashboardData.closingTenders[index].epoch).format('HH:mm');
        this.dashboardData.closingTenders[index].timeleft = countdown(
          null,
          new Date(this.dashboardData.closingTenders[index].epoch),
          countdown.DAYS | countdown.HOURS | countdown.MINUTES
        ).toString();
      }
  
      if (this.dashboardData.usr.role == 'Yrittäjä') {
        for (let index = 0; index < this.dashboardData.respondedTenders.length; index++) {
          const tndr = this.dashboardData.respondedTenders[index];
          this.dashboardData.respondedTenders[index].wherefrom = JSON.parse(tndr.where_from);
          this.dashboardData.respondedTenders[index].whereto = JSON.parse(tndr.where_to);
          this.dashboardData.respondedTenders[index].ending_d = moment(this.dashboardData.respondedTenders[index].epoch).format('DD.MM.YYYY');
          this.dashboardData.respondedTenders[index].ending_t = moment(this.dashboardData.respondedTenders[index].epoch).format('HH:mm');
        }
        for (let index = 0; index < this.dashboardData.acceptedTenders.length; index++) {
          const tndr = this.dashboardData.acceptedTenders[index];
          this.dashboardData.acceptedTenders[index].wherefrom = JSON.parse(tndr.where_from);
          this.dashboardData.acceptedTenders[index].whereto = JSON.parse(tndr.where_to);
          this.dashboardData.acceptedTenders[index].ending_d = moment(this.dashboardData.acceptedTenders[index].epoch).format('DD.MM.YYYY');
          this.dashboardData.acceptedTenders[index].ending_t = moment(this.dashboardData.acceptedTenders[index].epoch).format('HH:mm');
        }
      }
  
      if (this.dashboardData.usr.role == 'Admin') {
        setTimeout(() => {
          this.changeTimespan('today');
        }, 500);
      }

      this.loadingdata = false;
    });
  }
}


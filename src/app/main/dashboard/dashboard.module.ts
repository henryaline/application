import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgApexchartsModule } from 'ng-apexcharts';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { AuthGuard } from 'app/auth/helpers';
import { Role } from 'app/auth/models';

import { CoreCommonModule } from '@core/common.module';

import { InvoiceModule } from 'app/main/apps/invoice/invoice.module';
import { InvoiceListService } from 'app/main/apps/invoice/invoice-list/invoice-list.service';

import { TenderModule } from 'app/main/apps/tender/tender.module';
import { TenderListService } from 'app/main/apps/tender/tender-list/tender-list.service';

import { DashboardService } from 'app/main/dashboard/dashboard.service';

import { AnalyticsComponent } from 'app/main/dashboard/analytics/analytics.component';
import { HomeComponent } from 'app/main/dashboard/home/home.component';
import { OffersComponent } from 'app/main/dashboard/offers/offers.component';
import { TendersComponent } from 'app/main/dashboard/tenders/tenders.component';
import { EcommerceComponent } from 'app/main/dashboard/ecommerce/ecommerce.component';

import { ChartsModule } from 'ng2-charts';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';



const routes = [
  {
    path: 'analytics',
    component: AnalyticsComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin] },
    resolve: {
      css: DashboardService,
      inv: InvoiceListService
    }
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard],
    // data: { roles: [Role.Admin] },
    resolve: {
      css: DashboardService,
      inv: InvoiceListService
    }
  },
  {
    path: 'offers',
    component: OffersComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin] },
    resolve: {
      css: DashboardService,
      inv: InvoiceListService
    }
  },
  {
    // path: 'tenders',
    path: 'tenders/all',
    component: TendersComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin, Role.Yrittäjä] },
    resolve: {
      css: DashboardService,
      inv: TenderListService
    }
  },
  {
    path: 'tenders/company-listed',
    component: TendersComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin, Role.Yrittäjä, Role.Työntekijä] },
    resolve: {
      css: DashboardService,
      inv: TenderListService
    }
  },
  {
    path: 'tenders/responsed',
    component: TendersComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin, Role.Yrittäjä] },
    resolve: {
      css: DashboardService,
      inv: TenderListService
    }
  },
  {
    path: 'ecommerce',
    component: EcommerceComponent,
    canActivate: [AuthGuard],
    resolve: {
      css: DashboardService
    }
  }
];

@NgModule({
  declarations: [AnalyticsComponent, OffersComponent, TendersComponent, HomeComponent, EcommerceComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    NgbModule,
    PerfectScrollbarModule,
    CoreCommonModule,
    NgApexchartsModule,
    InvoiceModule,
    TenderModule,
    ChartsModule,
    NgxDatatableModule
  ],
  providers: [DashboardService, InvoiceListService, TenderListService],
  exports: [EcommerceComponent]
})
export class DashboardModule {}

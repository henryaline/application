export const environment = {
  production: true,
  hmr: false,
  // apiUrl: 'http://localhost:4000'
  // apiUrl: 'http://trafipointbackend:8888/api'
  // apiUrl: 'https://trafipoint-server.dev.aline.fi/api',
  // serverUrl: 'https://trafipoint-server.dev.aline.fi'
  // apiUrl: 'https://api.trafipoint.alinedev.fi/api',
  // serverUrl: 'https://api.trafipoint.alinedev.fi',
  apiUrl: 'https://api.trafipoint.fi/api',
  serverUrl: 'https://api.trafipoint.fi'
};
